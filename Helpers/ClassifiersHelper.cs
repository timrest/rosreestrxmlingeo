﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace Helpers
{
    public static class ClassifiersHelper
    {
        #region Properties
        public static List<ClassifiersItem> LandCatClassifiers = new List<ClassifiersItem>();
        public static List<ClassifiersItem> UsingClassifiers = new List<ClassifiersItem>();
        public static List<ClassifiersItem> BuildTypeClassifiers = new List<ClassifiersItem>();
        public static List<ClassifiersItem> BuildSignClassifiers = new List<ClassifiersItem>();
        public static List<ClassifiersItem> RegionClassifiers = new List<ClassifiersItem>();

        public struct ClassifiersItem
        {
            public string Code { get; set; }
            public string Value { get; set; }
        }

        #endregion

        #region Methods

        
        public static void FillClassifiers(Dictionary<string, string> settings)
        {
            foreach (KeyValuePair<string, string> cs in settings)
            {
                string ns = "http://www.w3.org/2001/XMLSchema";
                XmlDocument doc = new XmlDocument();
                doc.Load(cs.Value);
                XmlNodeList enumerations = doc.GetElementsByTagName("enumeration", ns);
                switch (cs.Key)
                {
                    case "LandCatClassifiers":
                        {
                            foreach (XmlElement enumeration in enumerations)
                            {
                                
                                ClassifiersItem item = new ClassifiersItem();
                                item.Code = enumeration.Attributes["value"].Value;
                                item.Value = enumeration["annotation", ns]["documentation", ns].InnerText;
                                LandCatClassifiers.Add(item);
                            }
                            break;
                        }
                    case "UsingClassifiers":
                        {
                            foreach (XmlElement enumeration in enumerations)
                            {

                                ClassifiersItem item = new ClassifiersItem();
                                item.Code = enumeration.Attributes["value"].Value;
                                item.Value = enumeration["annotation", ns]["documentation", ns].InnerText;
                                UsingClassifiers.Add(item);
                            }
                            break;
                        }
                    case "BuildTypeClassifiers":
                        {
                            foreach (XmlElement enumeration in enumerations)
                            {

                                ClassifiersItem item = new ClassifiersItem();
                                item.Code = enumeration.Attributes["value"].Value;
                                item.Value = enumeration["annotation", ns]["documentation", ns].InnerText;
                                BuildTypeClassifiers.Add(item);
                            }
                            break;
                        }
                    case "BuildSignClassifiers":
                        {
                            foreach (XmlElement enumeration in enumerations)
                            {

                                ClassifiersItem item = new ClassifiersItem();
                                item.Code = enumeration.Attributes["value"].Value;
                                item.Value = enumeration["annotation", ns]["documentation", ns].InnerText;
                                BuildSignClassifiers.Add(item);
                            }
                            break;
                        }
                    case "RegionClassifiers":
                        {
                            foreach (XmlElement enumeration in enumerations)
                            {

                                ClassifiersItem item = new ClassifiersItem();
                                item.Code = enumeration.Attributes["value"].Value;
                                item.Value = enumeration["annotation", ns]["documentation", ns].InnerText;
                                RegionClassifiers.Add(item);
                            }
                            break;
                        }
                }
                
            }
        }

        
        #endregion

        
    }

}
