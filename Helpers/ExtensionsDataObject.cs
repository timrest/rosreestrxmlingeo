﻿using Integro.InMeta.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    public static class ExtensionsDataObject
    {
        public static void DeleteAllAgregates(this DataObject parent, DataObject child)
        {
            foreach (MetadataChildRef refCl in parent.Class.Childs)
                if (refCl.ChildClass.Name.Equals(child.Class.Name))
                {
                    parent.GetChilds(child.Class.Name).DeleteAll();
                    break;
                }
        }
    }
}
