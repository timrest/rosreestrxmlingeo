﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Helpers
{
    public partial class IngeoHelper
    {
        public static void RestartIngeoServer()
        {
            var procs = System.Diagnostics.Process.GetProcessesByName("InGeo");
            if (procs.Length > 0)
            {
                procs[0].Kill();
                MessageBox.Show("ИнГео был выгружен из памяти для инициализации" +
                    " настроек программы. Пожалуйста запустите ИнГео.");
            }
        }

        #region Properties
        // Состояние подключения
        public bool isConnected = false;

        // Интерфейс для работы с ИнГео
        private IngeoApplication application;
        public IngeoApplication Application
        {
            set { this.application = value; }
            get { return this.application; }
        }

        
        #endregion

        #region Constructors
        public IngeoHelper()
        {
            this.application = GetActiveIngeo();
            if (this.application == null)
                throw new Exception("Не найдено запущенного экземпляра ИнГео");
            else
                this.isConnected = true;
        }
        #endregion
        
        #region Methods

        [DllImport("oleaut32.dll")]
        private static extern long GetActiveObject(ref Guid rclsid, IntPtr pvResreved, out IntPtr ppunk);

        public static IngeoApplication GetActiveIngeo()
        {
            IntPtr ppunk;
            Guid rclsid = new Guid("{04088492-0485-11D4-9719-000021C6D845}");
            GetActiveObject(ref rclsid, IntPtr.Zero, out ppunk);
            if (ppunk.ToInt32() != 0)
                return Marshal.GetObjectForIUnknown(ppunk) as IngeoApplication;
            else
                return null;
        }

        /// <summary>
        /// Функция возвращает пространственный объект по значению из семантической таблицы
        /// </summary>
        /// <param name="aLayerID">ID слоя</param>
        /// <param name="aSemTableName">Семантическая таблица</param>
        /// <param name="aSemFieldName">Семантическое поле</param>
        /// <param name="aSemFieldValue">Значение поля</param>
        /// <returns>Возвращается пространственный объект</returns>
        public static IIngeoMapObject GetOneMapObject(string aLayerID, string aSemTableName, string aSemFieldName, string aSemFieldValue)
        {
            IIngeoApplication aIngeoApp = GetActiveIngeo();
            IIngeoMapObject mapObject = null;
            IIngeoLayer layer = aIngeoApp.ActiveDb.LayerFromID(aLayerID);
            IIngeoSemDbTable semDbTable = null;
            foreach (IIngeoSemTable semTable in layer.SemTables)
            {
                if (semTable.Name == aSemTableName)
                {
                    semDbTable = semTable.SemDbTable;
                    break;
                }
            }
            IIngeoMapObjects mapObjects = aIngeoApp.ActiveDb.MapObjects;
            //IIngeoSemDbTable semDbTable = aIngeoApp.ActiveDb.SemDbTables[semDbTableName];
            if (semDbTable != null)
            {
                IIngeoSemDbDataSet objects = semDbTable.SelectData("ID", aSemFieldName + "=?", new object[] { aSemFieldValue });
                while (!objects.EOF)
                {
                    string objectId = objects.Fields["ID"].Value.ToString();
                    mapObject = mapObjects.GetObject(objectId);
                    break;
                }
                objects.Close();
            }

            return mapObject;
        }
        public void DeleteObject(string cadastralNumber, string layerID, string TableName)
        {
            IIngeoMapObjects objects = application.ActiveDb.MapObjects;
            IIngeoMapObjectsQuery Query = objects.QueryByLayers(layerID);
            while (!Query.EOF)
            {
                IIngeoMapObject obj = objects.GetObject(Query.ObjectID);
                if ((string)obj.SemData.GetValue(TableName, "CadastralNumber", 0) == cadastralNumber)
                {
                    objects.DeleteObject(obj.ID);
                    objects.UpdateChanges();
                    break;
                }
                Query.MoveNext();                
            }
        }

        /// <summary>
        /// Функция создания подписи к объекту
        /// </summary>
        /// <param name="mapObject">Пространственный объект</param>
        /// <param name="captionStyleId">Стиль подписи пространственного объекта</param>
        public static void CreateMapObjectCaption(IIngeoMapObject mapObject, string captionStyleId)
        {
            if (mapObject != null)
            {
                IIngeoShape shape = mapObject.Shapes.Insert(-1, captionStyleId);
                IIngeoContourPart contour = shape.Contour.Insert(-1);
                contour.InsertVertex(-1, (mapObject.X1 + mapObject.X2) / 2, (mapObject.Y1 + mapObject.Y2)/2, 0);
                contour.InsertVertex(-1, (mapObject.X1 + mapObject.X2) / 2, 1 + (mapObject.Y1 + mapObject.Y2) / 2, 0);
                mapObject.MapObjects.UpdateChanges();
            }
        }

        #endregion




        #region NEED REWORK

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="localId"></param>
        /// <returns></returns>
        public string GetGlobalId(string localId)
        {
            object o;
            object n;
            this.application.ActiveDb.LocalIDToGlobalID(localId, out o, out n);
            return o.ToString() + n.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static string GetLayerPath(IIngeoLayer layer)
        {
            return string.Format("[{0}].[{1}].[{2}]", layer.Map.Area.Name, layer.Map.Name, layer.Name);
        }

        public struct FieldDesc
        {
            public string name;
            public string caption;
            public TIngeoFieldDataType type;
            public int size;
        }

        public static void PrepareSemTable(IIngeoSemTable semTable, List<FieldDesc> fields)
        {
            foreach (FieldDesc field in fields)
            {
                try
                {
                    semTable.SemDbTable.Fields.Add(field.name, field.type, field.size, 0, 0);
                    semTable.FieldInfos.Add(field.name, field.caption, "", "", "");                    
                }
                catch { }
            }
            semTable.SemDbTable.Update();
            semTable.Update();
        }
        #endregion
    }
}
