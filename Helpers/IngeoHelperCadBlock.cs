﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Helpers
{
    public partial class IngeoHelper
    {
        

        #region Properties


        public List<IIngeoMapView> CadBlockMaps = new List<IIngeoMapView>();
        public List<IIngeoLayerView> CadBlockLayers = new List<IIngeoLayerView>();
        public List<IIngeoStyleView> CadBlockStyles = new List<IIngeoStyleView>();
        public List<IIngeoStyleView> CadBlockCaptionStyles = new List<IIngeoStyleView>();

        public List<IIngeoSemTable> CadBlockTablesCadNo = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> CadBlockFieldsCadNo = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> CadBlockTablesArea = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> CadBlockFieldsArea = new List<IIngeoSemFieldInfo>();


        
        #endregion

        
        #region Methods

        /// <summary>
        /// Загрузка и отображение списка карт выбранного проекта
        /// </summary>
        /// <param name="project">Проект</param>
        /// <param name="cb">ComboBox</param>
        public void GetCadBlockMaps(IIngeoProjectView project, ComboBox cb, string lastMapId)
        {
            CadBlockMaps.Clear();
            cb.Items.Clear();
            for (int i = 0; i < project.MapViews.Count; i++)
            {
                IIngeoMapView map = project.MapViews[i];
                CadBlockMaps.Add(map);
                cb.Items.Add(map.Map.Name);
                if (map.Map.ID == lastMapId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        

        /// <summary>
        /// Загрузка и отображение списка слоёв выбранной карты
        /// </summary>
        /// <param name="map">Карта</param>
        /// <param name="cb">ComboBox</param>
        public void GetCadBlockLayers(IIngeoMapView map, ComboBox cb, string lastLayerId)
        {
            CadBlockLayers.Clear();
            cb.Items.Clear();
            for (int i = 0; i < map.LayerViews.Count; i++)
            {
                IIngeoLayerView layer = map.LayerViews[i];
                CadBlockLayers.Add(layer);
                cb.Items.Add(layer.Layer.Name);
                if (layer.Layer.ID == lastLayerId)
                {
                    //selectedLayer = layer;
                    cb.SelectedIndex = i;
                }
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetCadBlockStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            CadBlockStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                CadBlockStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetCadBlockCaptionStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            CadBlockCaptionStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                CadBlockCaptionStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetCadBlockTablesCadNo(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            CadBlockTablesCadNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                CadBlockTablesCadNo.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetCadBlockFieldsCadNo(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            CadBlockFieldsCadNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                CadBlockFieldsCadNo.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetCadBlockTablesArea(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            CadBlockTablesArea.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                CadBlockTablesArea.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetCadBlockFieldsArea(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            CadBlockFieldsArea.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                CadBlockFieldsArea.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        
        #endregion
    }
}
