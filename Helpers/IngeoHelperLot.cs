﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Helpers
{
    public partial class IngeoHelper
    {
        

        #region Properties


        #region //----------------- Настройки для земельного участка -----------------------
        // Выбранная пользователем карта для земельного участка
        private IIngeoMapView selectedLotMap;
        public IIngeoMapView SelectedLotMap
        {
            set { this.selectedLotMap = value; }
            get { return this.selectedLotMap; }
        }

        // Выбранный пользователем слой для земельного участка
        private IIngeoLayerView selectedLotLayer;
        public IIngeoLayerView SelectedLotLayer
        {
            set { this.selectedLotLayer = value; }
            get { return this.selectedLotLayer; }
        }

        // Выбранный пользователем стиль для земельного участка
        private IIngeoStyleView selectedLotStyle;
        public IIngeoStyleView SelectedLotStyle
        {
            set { this.selectedLotStyle = value; }
            get { return this.selectedLotStyle; }
        }

        // Выбранный пользователем стиль для подписи земельного участка
        private IIngeoStyleView selectedLotCaptionStyle;
        public IIngeoStyleView SelectedLotCaptionStyle
        {
            set { this.selectedLotCaptionStyle = value; }
            get { return this.selectedLotCaptionStyle; }
        }

        // ----------- семантика земельного участка
        // Выбранная пользователем таблица семантики для кадастрового номера
        private IIngeoSemTable selectedLotTableCadNo;
        public IIngeoSemTable SelectedLotTableCadNo
        {
            set { this.selectedLotTableCadNo = value; }
            get { return this.selectedLotTableCadNo; }
        }
        // Выбранное пользователем поле кадастрового номера
        private IIngeoSemFieldInfo selectedLotFieldCadNo;
        public IIngeoSemFieldInfo SelectedLotFieldCadNo
        {
            set { this.selectedLotFieldCadNo = value; }
            get { return this.selectedLotFieldCadNo; }
        }

        // Выбранная пользователем таблица семантики для адреса
        private IIngeoSemTable selectedLotTableAddress;
        public IIngeoSemTable SelectedLotTableAddress
        {
            set { this.selectedLotTableAddress = value; }
            get { return this.selectedLotTableAddress; }
        }
        // Выбранное пользователем поле адреса
        private IIngeoSemFieldInfo selectedLotFieldAddress;
        public IIngeoSemFieldInfo SelectedLotFieldAddress
        {
            set { this.selectedLotFieldAddress = value; }
            get { return this.selectedLotFieldAddress; }
        }

        // Выбранная пользователем таблица семантики для площади
        private IIngeoSemTable selectedLotTableArea;
        public IIngeoSemTable SelectedLotTableArea
        {
            set { this.selectedLotTableArea = value; }
            get { return this.selectedLotTableArea; }
        }
        // Выбранная пользователем поле площади
        private IIngeoSemFieldInfo selectedLotFieldArea;
        public IIngeoSemFieldInfo SelectedLotFieldArea
        {
            set { this.selectedLotFieldArea = value; }
            get { return this.selectedLotFieldArea; }
        }

        // Выбранная пользователем таблица семантики для категории земель
        private IIngeoSemTable selectedLotTableLandCat;
        public IIngeoSemTable SelectedLotTableLandCat
        {
            set { this.selectedLotTableLandCat = value; }
            get { return this.selectedLotTableLandCat; }
        }
        // Выбранная пользователем поле для категории земель
        private IIngeoSemFieldInfo selectedLotFieldLandCat;
        public IIngeoSemFieldInfo SelectedLotFieldLandCat
        {
            set { this.selectedLotFieldLandCat = value; }
            get { return this.selectedLotFieldLandCat; }
        }

        // Выбранная пользователем таблица семантики для разрешенного использования участка
        private IIngeoSemTable selectedLotTableUsing;
        public IIngeoSemTable SelectedLotTableUsing
        {
            set { this.selectedLotTableUsing = value; }
            get { return this.selectedLotTableUsing; }
        }
        // Выбранная пользователем поле разрешенного использования участка
        private IIngeoSemFieldInfo selectedLotFieldUsing;
        public IIngeoSemFieldInfo SelectedLotFieldUsing
        {
            set { this.selectedLotFieldUsing = value; }
            get { return this.selectedLotFieldUsing; }
        }

        // Выбранная пользователем таблица семантики для разрешенного использования участка по документу
        private IIngeoSemTable selectedLotTableUsingByDoc;
        public IIngeoSemTable SelectedLotTableUsingByDoc
        {
            set { this.selectedLotTableUsingByDoc = value; }
            get { return this.selectedLotTableUsingByDoc; }
        }
        // Выбранная пользователем поле разрешенного использования участка по документу
        private IIngeoSemFieldInfo selectedLotFieldUsingByDoc;
        public IIngeoSemFieldInfo SelectedLotFieldUsingByDoc
        {
            set { this.selectedLotFieldUsingByDoc = value; }
            get { return this.selectedLotFieldUsingByDoc; }
        }
        #endregion

        

        public List<IIngeoMapView> LotMaps = new List<IIngeoMapView>();
        public List<IIngeoLayerView> LotLayers = new List<IIngeoLayerView>();
        public List<IIngeoStyleView> LotStyles = new List<IIngeoStyleView>();
        public List<IIngeoStyleView> LotCaptionStyles = new List<IIngeoStyleView>();
        public List<IIngeoSemTable> LotTablesCadNo = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> LotFieldsCadNo = new List<IIngeoSemFieldInfo>();
         public List<IIngeoSemTable> LotTablesAddress = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> LotFieldsAddress = new List<IIngeoSemFieldInfo>();
         public List<IIngeoSemTable> LotTablesArea = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> LotFieldsArea = new List<IIngeoSemFieldInfo>();
         public List<IIngeoSemTable> LotTablesLandCat = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> LotFieldsLandCat = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> LotTablesUsing = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> LotFieldsUsing = new List<IIngeoSemFieldInfo>();
         public List<IIngeoSemTable> LotTablesUsingByDoc = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> LotFieldsUsingByDoc = new List<IIngeoSemFieldInfo>();

        
        #endregion

        
        #region Methods

        /// <summary>
        /// Загрузка и отображение списка карт выбранного проекта
        /// </summary>
        /// <param name="project">Проект</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotMaps(IIngeoProjectView project, ComboBox cb, string lastMapId)
        {
            LotMaps.Clear();
            cb.Items.Clear();
            for (int i = 0; i < project.MapViews.Count; i++)
            {
                IIngeoMapView map = project.MapViews[i];
                LotMaps.Add(map);
                cb.Items.Add(map.Map.Name);
                if (map.Map.ID == lastMapId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        

        /// <summary>
        /// Загрузка и отображение списка слоёв выбранной карты
        /// </summary>
        /// <param name="map">Карта</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotLayers(IIngeoMapView map, ComboBox cb, string lastLayerId)
        {
            LotLayers.Clear();
            cb.Items.Clear();
            for (int i = 0; i < map.LayerViews.Count; i++)
            {
                IIngeoLayerView layer = map.LayerViews[i];
                LotLayers.Add(layer);
                cb.Items.Add(layer.Layer.Name);
                if (layer.Layer.ID == lastLayerId)
                {
                    //selectedLayer = layer;
                    cb.SelectedIndex = i;
                }
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            LotStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                LotStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotCaptionStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            LotCaptionStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                LotCaptionStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotTablesCadNo(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            LotTablesCadNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                LotTablesCadNo.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetLotFieldsCadNo(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            LotFieldsCadNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                LotFieldsCadNo.Add(field);
                //if (field.FieldName == "ID")
                //    continue;

                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotTablesAddress(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            LotTablesAddress.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                LotTablesAddress.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetLotFieldsAddress(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            LotFieldsAddress.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                LotFieldsAddress.Add(field);

                //if (field.FieldName == "ID")
                //    continue;
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotTablesArea(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            LotTablesArea.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                LotTablesArea.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetLotFieldsArea(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            LotFieldsArea.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                LotFieldsArea.Add(field);
                //if (field.FieldName == "ID")
                //    continue;
                
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotTablesLandCat(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            LotTablesLandCat.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                LotTablesLandCat.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetLotFieldsLandCat(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            LotFieldsLandCat.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                LotFieldsLandCat.Add(field);
                //if (field.FieldName == "ID")
                //    continue;
                
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotTablesUsing(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            LotTablesUsing.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                LotTablesUsing.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetLotFieldsUsing(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            LotFieldsUsing.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                LotFieldsUsing.Add(field);

                //if (field.FieldName == "ID")
                //    continue;
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetLotTablesUsingByDoc(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            LotTablesUsingByDoc.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                LotTablesUsingByDoc.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetLotFieldsUsingByDoc(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            LotFieldsUsingByDoc.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                LotFieldsUsingByDoc.Add(field);

                //if (field.FieldName == "ID")
                //    continue;
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        #endregion
    }
}
