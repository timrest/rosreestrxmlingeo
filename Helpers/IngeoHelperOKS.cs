﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Helpers
{
    public partial class IngeoHelper
    {
        

        #region Properties


        #region //----------------- Настройки для земельного участка -----------------------
        // Выбранная пользователем карта для земельного участка
        //private IIngeoMapView selectedLotMap;
        //public IIngeoMapView SelectedLotMap
        //{
        //    set { this.selectedLotMap = value; }
        //    get { return this.selectedLotMap; }
        //}

        //// Выбранный пользователем слой для земельного участка
        //private IIngeoLayerView selectedLotLayer;
        //public IIngeoLayerView SelectedLotLayer
        //{
        //    set { this.selectedLotLayer = value; }
        //    get { return this.selectedLotLayer; }
        //}

        //// Выбранный пользователем стиль для земельного участка
        //private IIngeoStyleView selectedLotStyle;
        //public IIngeoStyleView SelectedLotStyle
        //{
        //    set { this.selectedLotStyle = value; }
        //    get { return this.selectedLotStyle; }
        //}

        //// Выбранный пользователем стиль для подписи земельного участка
        //private IIngeoStyleView selectedLotCaptionStyle;
        //public IIngeoStyleView SelectedLotCaptionStyle
        //{
        //    set { this.selectedLotCaptionStyle = value; }
        //    get { return this.selectedLotCaptionStyle; }
        //}

        //// ----------- семантика земельного участка
        //// Выбранная пользователем таблица семантики для кадастрового номера
        //private IIngeoSemTable selectedLotTableCadNo;
        //public IIngeoSemTable SelectedLotTableCadNo
        //{
        //    set { this.selectedLotTableCadNo = value; }
        //    get { return this.selectedLotTableCadNo; }
        //}
        //// Выбранное пользователем поле кадастрового номера
        //private IIngeoSemFieldInfo selectedLotFieldCadNo;
        //public IIngeoSemFieldInfo SelectedLotFieldCadNo
        //{
        //    set { this.selectedLotFieldCadNo = value; }
        //    get { return this.selectedLotFieldCadNo; }
        //}

        //// Выбранная пользователем таблица семантики для адреса
        //private IIngeoSemTable selectedLotTableAddress;
        //public IIngeoSemTable SelectedLotTableAddress
        //{
        //    set { this.selectedLotTableAddress = value; }
        //    get { return this.selectedLotTableAddress; }
        //}
        //// Выбранное пользователем поле адреса
        //private IIngeoSemFieldInfo selectedLotFieldAddress;
        //public IIngeoSemFieldInfo SelectedLotFieldAddress
        //{
        //    set { this.selectedLotFieldAddress = value; }
        //    get { return this.selectedLotFieldAddress; }
        //}

        //// Выбранная пользователем таблица семантики для площади
        //private IIngeoSemTable selectedLotTableArea;
        //public IIngeoSemTable SelectedLotTableArea
        //{
        //    set { this.selectedLotTableArea = value; }
        //    get { return this.selectedLotTableArea; }
        //}
        //// Выбранная пользователем поле площади
        //private IIngeoSemFieldInfo selectedLotFieldArea;
        //public IIngeoSemFieldInfo SelectedLotFieldArea
        //{
        //    set { this.selectedLotFieldArea = value; }
        //    get { return this.selectedLotFieldArea; }
        //}

        //// Выбранная пользователем таблица семантики для категории земель
        //private IIngeoSemTable selectedLotTableLandCat;
        //public IIngeoSemTable SelectedLotTableLandCat
        //{
        //    set { this.selectedLotTableLandCat = value; }
        //    get { return this.selectedLotTableLandCat; }
        //}
        //// Выбранная пользователем поле для категории земель
        //private IIngeoSemFieldInfo selectedLotFieldLandCat;
        //public IIngeoSemFieldInfo SelectedLotFieldLandCat
        //{
        //    set { this.selectedLotFieldLandCat = value; }
        //    get { return this.selectedLotFieldLandCat; }
        //}

        //// Выбранная пользователем таблица семантики для разрешенного использования участка
        //private IIngeoSemTable selectedLotTableUsing;
        //public IIngeoSemTable SelectedLotTableUsing
        //{
        //    set { this.selectedLotTableUsing = value; }
        //    get { return this.selectedLotTableUsing; }
        //}
        //// Выбранная пользователем поле разрешенного использования участка
        //private IIngeoSemFieldInfo selectedLotFieldUsing;
        //public IIngeoSemFieldInfo SelectedLotFieldUsing
        //{
        //    set { this.selectedLotFieldUsing = value; }
        //    get { return this.selectedLotFieldUsing; }
        //}

        //// Выбранная пользователем таблица семантики для разрешенного использования участка по документу
        //private IIngeoSemTable selectedLotTableUsingByDoc;
        //public IIngeoSemTable SelectedLotTableUsingByDoc
        //{
        //    set { this.selectedLotTableUsingByDoc = value; }
        //    get { return this.selectedLotTableUsingByDoc; }
        //}
        //// Выбранная пользователем поле разрешенного использования участка по документу
        //private IIngeoSemFieldInfo selectedLotFieldUsingByDoc;
        //public IIngeoSemFieldInfo SelectedLotFieldUsingByDoc
        //{
        //    set { this.selectedLotFieldUsingByDoc = value; }
        //    get { return this.selectedLotFieldUsingByDoc; }
        //}
        #endregion

        

        public List<IIngeoMapView> OksMaps = new List<IIngeoMapView>();
        public List<IIngeoLayerView> OksLayers = new List<IIngeoLayerView>();
        public List<IIngeoStyleView> OksBuildStyles = new List<IIngeoStyleView>();
        public List<IIngeoStyleView> OksConstrStyles = new List<IIngeoStyleView>();
        public List<IIngeoStyleView> OksUncompletedStyles = new List<IIngeoStyleView>();
        public List<IIngeoStyleView> OksCaptionStyles = new List<IIngeoStyleView>();
        public List<IIngeoSemTable> OksTablesCadNo = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> OksFieldsCadNo = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> OksTablesAddress = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> OksFieldsAddress = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> OksTablesArea = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> OksFieldsArea = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> OksTablesType = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> OksFieldsType = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> OksTablesAssign = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> OksFieldsAssign = new List<IIngeoSemFieldInfo>();

        
        #endregion

        
        #region Methods

        /// <summary>
        /// Загрузка и отображение списка карт выбранного проекта
        /// </summary>
        /// <param name="project">Проект</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksMaps(IIngeoProjectView project, ComboBox cb, string lastMapId)
        {
            OksMaps.Clear();
            cb.Items.Clear();
            for (int i = 0; i < project.MapViews.Count; i++)
            {
                IIngeoMapView map = project.MapViews[i];
                OksMaps.Add(map);
                cb.Items.Add(map.Map.Name);
                if (map.Map.ID == lastMapId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        

        /// <summary>
        /// Загрузка и отображение списка слоёв выбранной карты
        /// </summary>
        /// <param name="map">Карта</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksLayers(IIngeoMapView map, ComboBox cb, string lastLayerId)
        {
            OksLayers.Clear();
            cb.Items.Clear();
            for (int i = 0; i < map.LayerViews.Count; i++)
            {
                IIngeoLayerView layer = map.LayerViews[i];
                OksLayers.Add(layer);
                cb.Items.Add(layer.Layer.Name);
                if (layer.Layer.ID == lastLayerId)
                {
                    //selectedLayer = layer;
                    cb.SelectedIndex = i;
                }
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksBuildStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            OksBuildStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                OksBuildStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksConstrStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            OksConstrStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                OksConstrStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksUncompletedStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            OksUncompletedStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                OksUncompletedStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksCaptionStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            OksCaptionStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                OksCaptionStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksTablesCadNo(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            OksTablesCadNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                OksTablesCadNo.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetOksFieldsCadNo(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            OksFieldsCadNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                OksFieldsCadNo.Add(field);

                //if (field.FieldName == "ID")
                //    continue;
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksTablesAddress(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            OksTablesAddress.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                OksTablesAddress.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetOksFieldsAddress(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            OksFieldsAddress.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                OksFieldsAddress.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksTablesArea(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            OksTablesArea.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                OksTablesArea.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetOksFieldsArea(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            OksFieldsArea.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                OksFieldsArea.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksTablesType(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            OksTablesType.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                OksTablesType.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetOksFieldsType(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            OksFieldsType.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                OksFieldsType.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetOksLotTablesAssign(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            OksTablesAssign.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                OksTablesAssign.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetOksFieldsAssign(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            OksFieldsAssign.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                OksFieldsAssign.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        
        #endregion
    }
}
