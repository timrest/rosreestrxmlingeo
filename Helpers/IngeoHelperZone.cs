﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Helpers
{
    public partial class IngeoHelper
    {
        

        #region Properties


        #region //----------------- Настройки для земельного участка -----------------------
        // Выбранная пользователем карта для земельного участка
        //private IIngeoMapView selectedLotMap;
        //public IIngeoMapView SelectedLotMap
        //{
        //    set { this.selectedLotMap = value; }
        //    get { return this.selectedLotMap; }
        //}

        //// Выбранный пользователем слой для земельного участка
        //private IIngeoLayerView selectedLotLayer;
        //public IIngeoLayerView SelectedLotLayer
        //{
        //    set { this.selectedLotLayer = value; }
        //    get { return this.selectedLotLayer; }
        //}

        //// Выбранный пользователем стиль для земельного участка
        //private IIngeoStyleView selectedLotStyle;
        //public IIngeoStyleView SelectedLotStyle
        //{
        //    set { this.selectedLotStyle = value; }
        //    get { return this.selectedLotStyle; }
        //}

        //// Выбранный пользователем стиль для подписи земельного участка
        //private IIngeoStyleView selectedLotCaptionStyle;
        //public IIngeoStyleView SelectedLotCaptionStyle
        //{
        //    set { this.selectedLotCaptionStyle = value; }
        //    get { return this.selectedLotCaptionStyle; }
        //}

        //// ----------- семантика земельного участка
        //// Выбранная пользователем таблица семантики для кадастрового номера
        //private IIngeoSemTable selectedLotTableCadNo;
        //public IIngeoSemTable SelectedLotTableCadNo
        //{
        //    set { this.selectedLotTableCadNo = value; }
        //    get { return this.selectedLotTableCadNo; }
        //}
        //// Выбранное пользователем поле кадастрового номера
        //private IIngeoSemFieldInfo selectedLotFieldCadNo;
        //public IIngeoSemFieldInfo SelectedLotFieldCadNo
        //{
        //    set { this.selectedLotFieldCadNo = value; }
        //    get { return this.selectedLotFieldCadNo; }
        //}

        //// Выбранная пользователем таблица семантики для адреса
        //private IIngeoSemTable selectedLotTableAddress;
        //public IIngeoSemTable SelectedLotTableAddress
        //{
        //    set { this.selectedLotTableAddress = value; }
        //    get { return this.selectedLotTableAddress; }
        //}
        //// Выбранное пользователем поле адреса
        //private IIngeoSemFieldInfo selectedLotFieldAddress;
        //public IIngeoSemFieldInfo SelectedLotFieldAddress
        //{
        //    set { this.selectedLotFieldAddress = value; }
        //    get { return this.selectedLotFieldAddress; }
        //}

        //// Выбранная пользователем таблица семантики для площади
        //private IIngeoSemTable selectedLotTableArea;
        //public IIngeoSemTable SelectedLotTableArea
        //{
        //    set { this.selectedLotTableArea = value; }
        //    get { return this.selectedLotTableArea; }
        //}
        //// Выбранная пользователем поле площади
        //private IIngeoSemFieldInfo selectedLotFieldArea;
        //public IIngeoSemFieldInfo SelectedLotFieldArea
        //{
        //    set { this.selectedLotFieldArea = value; }
        //    get { return this.selectedLotFieldArea; }
        //}

        //// Выбранная пользователем таблица семантики для категории земель
        //private IIngeoSemTable selectedLotTableLandCat;
        //public IIngeoSemTable SelectedLotTableLandCat
        //{
        //    set { this.selectedLotTableLandCat = value; }
        //    get { return this.selectedLotTableLandCat; }
        //}
        //// Выбранная пользователем поле для категории земель
        //private IIngeoSemFieldInfo selectedLotFieldLandCat;
        //public IIngeoSemFieldInfo SelectedLotFieldLandCat
        //{
        //    set { this.selectedLotFieldLandCat = value; }
        //    get { return this.selectedLotFieldLandCat; }
        //}

        //// Выбранная пользователем таблица семантики для разрешенного использования участка
        //private IIngeoSemTable selectedLotTableUsing;
        //public IIngeoSemTable SelectedLotTableUsing
        //{
        //    set { this.selectedLotTableUsing = value; }
        //    get { return this.selectedLotTableUsing; }
        //}
        //// Выбранная пользователем поле разрешенного использования участка
        //private IIngeoSemFieldInfo selectedLotFieldUsing;
        //public IIngeoSemFieldInfo SelectedLotFieldUsing
        //{
        //    set { this.selectedLotFieldUsing = value; }
        //    get { return this.selectedLotFieldUsing; }
        //}

        //// Выбранная пользователем таблица семантики для разрешенного использования участка по документу
        //private IIngeoSemTable selectedLotTableUsingByDoc;
        //public IIngeoSemTable SelectedLotTableUsingByDoc
        //{
        //    set { this.selectedLotTableUsingByDoc = value; }
        //    get { return this.selectedLotTableUsingByDoc; }
        //}
        //// Выбранная пользователем поле разрешенного использования участка по документу
        //private IIngeoSemFieldInfo selectedLotFieldUsingByDoc;
        //public IIngeoSemFieldInfo SelectedLotFieldUsingByDoc
        //{
        //    set { this.selectedLotFieldUsingByDoc = value; }
        //    get { return this.selectedLotFieldUsingByDoc; }
        //}
        #endregion

        

        public List<IIngeoMapView> ZoneMaps = new List<IIngeoMapView>();
        public List<IIngeoLayerView> ZoneLayers = new List<IIngeoLayerView>();
        public List<IIngeoStyleView> ZoneTerrStyles = new List<IIngeoStyleView>();
        public List<IIngeoStyleView> ZoneSZZStyles = new List<IIngeoStyleView>();
        public List<IIngeoStyleView> ZoneCaptionStyles = new List<IIngeoStyleView>();
        public List<IIngeoSemTable> ZoneTablesAccountNo = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> ZoneFieldsAccountNo = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> ZoneTablesName = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> ZoneFieldsName = new List<IIngeoSemFieldInfo>();
        public List<IIngeoSemTable> ZoneTablesRestrict = new List<IIngeoSemTable>();
        public List<IIngeoSemFieldInfo> ZoneFieldsRestrict = new List<IIngeoSemFieldInfo>();
       

        
        #endregion

        
        #region Methods

        /// <summary>
        /// Загрузка и отображение списка карт выбранного проекта
        /// </summary>
        /// <param name="project">Проект</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneMaps(IIngeoProjectView project, ComboBox cb, string lastMapId)
        {
            ZoneMaps.Clear();
            cb.Items.Clear();
            for (int i = 0; i < project.MapViews.Count; i++)
            {
                IIngeoMapView map = project.MapViews[i];
                ZoneMaps.Add(map);
                cb.Items.Add(map.Map.Name);
                if (map.Map.ID == lastMapId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        

        /// <summary>
        /// Загрузка и отображение списка слоёв выбранной карты
        /// </summary>
        /// <param name="map">Карта</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneLayers(IIngeoMapView map, ComboBox cb, string lastLayerId)
        {
            ZoneLayers.Clear();
            cb.Items.Clear();
            for (int i = 0; i < map.LayerViews.Count; i++)
            {
                IIngeoLayerView layer = map.LayerViews[i];
                ZoneLayers.Add(layer);
                cb.Items.Add(layer.Layer.Name);
                if (layer.Layer.ID == lastLayerId)
                {
                    //selectedLayer = layer;
                    cb.SelectedIndex = i;
                }
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneTerrStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            ZoneTerrStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                ZoneTerrStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneSZZStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            ZoneSZZStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                ZoneSZZStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
       
        /// <summary>
        /// Загрузка и отображение списка стилей выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneCaptionStyles(IIngeoLayerView layer, ComboBox cb, string lastStyleId)
        {
            ZoneCaptionStyles.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.StyleViews.Count; i++)
            {
                IIngeoStyleView style = layer.StyleViews[i];
                ZoneCaptionStyles.Add(style);
                cb.Items.Add(style.Style.Name);
                if (style.Style.ID == lastStyleId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneTablesAccountNo(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            ZoneTablesAccountNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                ZoneTablesAccountNo.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetZoneFieldsAccountNo(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            ZoneFieldsAccountNo.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                ZoneFieldsAccountNo.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneTablesName(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            ZoneTablesName.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                ZoneTablesName.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetZoneFieldsName(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            ZoneFieldsName.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                ZoneFieldsName.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        /// <summary>
        /// Загрузка и отображение списка таблиц выбранного слоя
        /// </summary>
        /// <param name="layer">Слой</param>
        /// <param name="cb">ComboBox</param>
        public void GetZoneTablesRestrict(IIngeoLayerView layer, ComboBox cb, string lastTableId)
        {
            ZoneTablesRestrict.Clear();
            cb.Items.Clear();
            for (int i = 0; i < layer.Layer.SemTables.Count; i++)
            {
                IIngeoSemTable table = layer.Layer.SemTables[i];
                ZoneTablesRestrict.Add(table);
                cb.Items.Add(table.Name);
                if (table.ID == lastTableId)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }

        /// <summary>
        /// Загрузка и отображение полей выбранной таблицы
        /// </summary>
        /// <param name="table">Таблица</param>
        /// /// <param name="cb">ComboBox</param>
        public void GetZoneFieldsRestrict(IIngeoSemTable table, ComboBox cb, string lastFieldName)
        {
            ZoneFieldsRestrict.Clear();
            cb.Items.Clear();
            for (int i = 0; i < table.FieldInfos.Count; i++)
            {
                IIngeoSemFieldInfo field = table.FieldInfos[i];
                //if (field.FieldName == "ID")
                //    continue;
                ZoneFieldsRestrict.Add(field);
                cb.Items.Add(field.FieldName);
                if (field.FieldName == lastFieldName)
                    cb.SelectedIndex = i;
            }
            if (cb.SelectedIndex == -1 && cb.Items.Count > 0)
                cb.SelectedIndex = 0;
        }
        
        #endregion
    }
}
