﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Helpers
{
    public class ImportResult
    {
        public ImportResult()
        {
            Output = new List<string>();
        }

        public List<string> Output { get; set; }

        public void WriteLine(string message)
        {
            Output.Add(message);
        }
    }

    public class ProgressBarLogger
    {
        public static ImportResult Writer { get; set; }
        public static ProgressBar Progress;

        public static int MaxValue
        {
            get { return Progress.Maximum; }
            set { Progress.Maximum = value; }
        }

        public static int Value
        {
            get { return Progress.Value; }
            set { Progress.Value = value; }
        }

        public static void ProgressInc()
        {
            Progress.Invoke((MethodInvoker) (() =>
            {
                Progress.Value++;
            }));
        }
    }
}
