﻿using Ingeo;
using RosreestrXMLIngeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    public static class SettingsSave
    {
        const string lotTableName = "RosreestrSettingsLot";
        const string cadBlockTableName = "RosreestrSettingsCadBlock";
        const string oksTableName = "RosreestrSettingsOKS";
        const string coordSysTableName = "RosreestrSettingsCoordSys";
        const string zoneTableName = "RosreestrSettingsZone";
        const string templateTableName = "RosreestrSettingsTemplate";
        static IIngeoSemDbTable lotTable;
        static IIngeoSemDbTable cadBlockTable;
        static IIngeoSemDbTable oksTable;
        static IIngeoSemDbTable zoneTable;
        static IIngeoSemDbTable coordSysTable;
        static IIngeoSemDbTable templateTable;

        static IngeoHelper ingeoHelper;
        static IngeoApplication ingeo;
        static MainForm mainForm;
        static bool flag;

        public static void SetInGeoHelper(IngeoHelper _ingeoHelper)
        {
            ingeoHelper = _ingeoHelper;
            ingeo = _ingeoHelper.Application;
        }

        static void checkField(IIngeoSemDbTable table, string fieldName)
        {
            foreach (IIngeoSemDbField field in table.Fields)
                if (field.Name.Equals(fieldName))
                {
                    //string[] val1 = { "1" };
                    //table.InsertData("ID", "?", val1);
                    //table.Update();
                    return;
                }
            table.Fields.Add(fieldName, TIngeoFieldDataType.inftString, 128, 0, 0);
            table.Update();
            flag = false;
        }

        static IIngeoSemDbTable createOrOpenTable(string tableName)
        {
            IIngeoSemDbTable table = null;
            if (!ingeo.ActiveDb.SemDbTables.Exists(tableName))
            {
                flag = false;
                table = ingeo.ActiveDb.SemDbTables.Add(tableName);
                table.Fields.Add("ID", TIngeoFieldDataType.inftString, 12, TIngeoFieldFlags.inffRequired, 0);
                table.Update();
            }
            else
                table = ingeo.ActiveDb.SemDbTables[tableName];

            switch (tableName)
            {
                case lotTableName:
                    {
                        checkField(table, "Map");
                        checkField(table, "Layer");
                        checkField(table, "Style");
                        checkField(table, "CaptionStyle");
                        checkField(table, "TableCadNo");
                        checkField(table, "FieldCadNo");
                        checkField(table, "TableAddress");
                        checkField(table, "FieldAddress");
                        checkField(table, "TableArea");
                        checkField(table, "FieldArea");
                        checkField(table, "TableLandCat");
                        checkField(table, "FieldLandCat");
                        checkField(table, "TableUsing");
                        checkField(table, "FieldUsing");
                        checkField(table, "TableUsingByDoc");
                        checkField(table, "FieldUsingByDoc");
                        break;
                    }
                case cadBlockTableName:
                    {
                        checkField(table, "Map");
                        checkField(table, "Layer");
                        checkField(table, "Style");
                        checkField(table, "CaptionStyle");
                        checkField(table, "TableCadNo");
                        checkField(table, "FieldCadNo");
                        checkField(table, "TableArea");
                        checkField(table, "FieldArea");
                        break;
                    }
                case oksTableName:
                    {
                        checkField(table, "Map");
                        checkField(table, "Layer");
                        checkField(table, "StyleBuild");
                        checkField(table, "StyleConstr");
                        checkField(table, "StyleUncompleted");
                        checkField(table, "CaptionStyle");
                        checkField(table, "TableCadNo");
                        checkField(table, "FieldCadNo");
                        checkField(table, "TableAddress");
                        checkField(table, "FieldAddress");
                        checkField(table, "TableArea");
                        checkField(table, "FieldArea");
                        checkField(table, "TableType");
                        checkField(table, "FieldType");
                        checkField(table, "TableAssign");
                        checkField(table, "FieldAssign");
                        break;
                    }
                case zoneTableName:
                    {
                        checkField(table, "Map");
                        checkField(table, "Layer");
                        checkField(table, "StyleTerr");
                        checkField(table, "StyleSZZ");
                        checkField(table, "CaptionStyle");
                        checkField(table, "TableAccountNo");
                        checkField(table, "FieldAccountNo");
                        checkField(table, "TableName");
                        checkField(table, "FieldName");
                        checkField(table, "TableRestrict");
                        checkField(table, "FieldRestrict");
                        break;
                    }
                case coordSysTableName:
                    {
                        checkField(table, "TransformIn");
                        checkField(table, "TransformOut");
                        checkField(table, "TransformTypeIn");
                        checkField(table, "TransformTypeOut");
                        checkField(table, "TransformEnabled");
                        checkField(table, "XtoYChange");
                        break;
                    }
                case templateTableName:
                    {
                        checkField(table, "Path");
                        checkField(table, "PropName");
                        checkField(table, "TableName");
                        checkField(table, "TableNameDb");
                        checkField(table, "FieldName");
                        break;
                    }
            }

            return table;
        }

        /// <summary>
        /// true - old tables, false - new tables
        /// </summary>
        /// <returns></returns>
        static bool createOrOpenTables()
        {
            lotTable = createOrOpenTable(lotTableName);
            cadBlockTable = createOrOpenTable(cadBlockTableName);
            oksTable = createOrOpenTable(oksTableName);
            zoneTable = createOrOpenTable(zoneTableName);
            coordSysTable = createOrOpenTable(coordSysTableName);
            templateTable = createOrOpenTable(templateTableName);
            return flag;
        }

        public static void OpenSettings(IngeoHelper _ingeo, MainForm _mainForm)
        {
            if (_ingeo == null || _ingeo.Application == null)
                return;
            mainForm = _mainForm;
            ingeoHelper = _ingeo;
            ingeo = _ingeo.Application;
            try
            {
                flag = true;
                if (!createOrOpenTables())
                    IngeoHelper.RestartIngeoServer();
            }
            catch { return; }

            
            if (templateTable != null)
            {
                //try
                //{
                    Helpers.XmlTemplate.Template = new Helpers.XmlTemplate();
                    IIngeoSemDbTable table = GetTemplateTable();
                    
                    var res = table.SelectData("Path,PropName," +
                        "TableName,TableNameDb,FieldName");
                    while (!res.EOF)
                    {
                        Helpers.XmlTemplateItem item = new Helpers.XmlTemplateItem();
                        item.Path = res.Fields[0].Value.ToString();
                        item.PropName = res.Fields[1].Value.ToString();
                        item.TableName = res.Fields[2].Value.ToString();
                        item.TableNameDb = res.Fields[3].Value.ToString();
                        item.FieldName = res.Fields[4].Value.ToString();
                        //System.Windows.Forms.MessageBox.Show(item.Path);
                        Helpers.XmlTemplate.Template.Add(item);
                        res.MoveNext();
                    }
                //}
                //catch
                //{ }
            }

            if (lotTable != null)
            {
                string[] whereParam = { "1" };
                var res = lotTable.SelectData("Map,Layer,Style,CaptionStyle,TableCadNo,FieldCadNo,TableAddress,FieldAddress," +
                "TableArea,FieldArea,TableLandCat,FieldLandCat,TableUsing,FieldUsing,TableUsingByDoc,FieldUsingByDoc", "ID=?", whereParam);
                if (!res.EOF)
                {
                    res.MoveFirst();
                    Helpers.Settings.LotMap = res.Fields[0].Value.ToString();
                    Helpers.Settings.LotLayer = res.Fields[1].Value.ToString();
                    Helpers.Settings.LotStyle = res.Fields[2].Value.ToString();
                    Helpers.Settings.LotCaptionStyle = res.Fields[3].Value.ToString();
                    Helpers.Settings.LotTableCadNo = res.Fields[4].Value.ToString();
                    Helpers.Settings.LotFieldCadNo = res.Fields[5].Value.ToString();
                    Helpers.Settings.LotTableAddress = res.Fields[6].Value.ToString();
                    Helpers.Settings.LotFieldAddress = res.Fields[7].Value.ToString();
                    Helpers.Settings.LotTableArea = res.Fields[8].Value.ToString();
                    Helpers.Settings.LotFieldArea = res.Fields[9].Value.ToString();
                    Helpers.Settings.LotTableLandCat = res.Fields[10].Value.ToString();
                    Helpers.Settings.LotFieldLandCat = res.Fields[11].Value.ToString();
                    Helpers.Settings.LotTableUsing = res.Fields[12].Value.ToString();
                    Helpers.Settings.LotFieldUsing = res.Fields[13].Value.ToString();
                    Helpers.Settings.LotTableUsingByDoc = res.Fields[14].Value.ToString();
                    Helpers.Settings.LotFieldUsingByDoc = res.Fields[15].Value.ToString();
                }
            }
            if (cadBlockTable != null)
            {
                string[] whereParam = { "1" };
                var res = cadBlockTable.SelectData("Map,Layer,Style,CaptionStyle,TableCadNo,FieldCadNo," +
                "TableArea,FieldArea", "ID=?", whereParam);
                if (!res.EOF)
                {
                    res.MoveFirst();
                    Helpers.Settings.CadBlockMap = res.Fields[0].Value.ToString();
                    Helpers.Settings.CadBlockLayer = res.Fields[1].Value.ToString();
                    Helpers.Settings.CadBlockStyle = res.Fields[2].Value.ToString();
                    Helpers.Settings.CadBlockCaptionStyle = res.Fields[3].Value.ToString();
                    Helpers.Settings.CadBlockTableCadNo = res.Fields[4].Value.ToString();
                    Helpers.Settings.CadBlockFieldCadNo = res.Fields[5].Value.ToString();
                    Helpers.Settings.CadBlockTableArea = res.Fields[6].Value.ToString();
                    Helpers.Settings.CadBlockFieldArea = res.Fields[7].Value.ToString();
                }
            }
            if (oksTable != null)
            {
                string[] whereParam = { "1" };
                var res = oksTable.SelectData("Map,Layer,StyleBuild,StyleConstr,StyleUncompleted,CaptionStyle,TableCadNo,FieldCadNo,TableAddress,FieldAddress," +
                "TableArea,FieldArea,TableType,FieldType,TableAssign,FieldAssign", "ID=?", whereParam);
                if (!res.EOF)
                {
                    res.MoveFirst();
                    Helpers.Settings.OksMap = res.Fields[0].Value.ToString();
                    Helpers.Settings.OksLayer = res.Fields[1].Value.ToString();
                    Helpers.Settings.OksBuildStyle = res.Fields[2].Value.ToString();
                    Helpers.Settings.OksConstrStyle = res.Fields[3].Value.ToString();
                    Helpers.Settings.OksUncompletedStyle = res.Fields[4].Value.ToString();
                    Helpers.Settings.OksCaptionStyle = res.Fields[5].Value.ToString();
                    Helpers.Settings.OksTableCadNo = res.Fields[6].Value.ToString();
                    Helpers.Settings.OksFieldCadNo = res.Fields[7].Value.ToString();
                    Helpers.Settings.OksTableAddress = res.Fields[8].Value.ToString();
                    Helpers.Settings.OksFieldAddress = res.Fields[9].Value.ToString();
                    Helpers.Settings.OksTableArea = res.Fields[10].Value.ToString();
                    Helpers.Settings.OksFieldArea = res.Fields[11].Value.ToString();
                    Helpers.Settings.OksTableType = res.Fields[12].Value.ToString();
                    Helpers.Settings.OksFieldType = res.Fields[13].Value.ToString();
                    Helpers.Settings.OksTableAssign = res.Fields[14].Value.ToString();
                    Helpers.Settings.OksFieldAssign = res.Fields[15].Value.ToString();
                }
            }
            if (zoneTable != null)
            {
                string[] whereParam = { "1" };
                var res = zoneTable.SelectData("Map,Layer,StyleTerr,StyleSZZ,CaptionStyle,TableAccountNo,FieldAccountNo," +
                "TableName,FieldName,TableRestrict,FieldRestrict", "ID=?", whereParam);
                if (!res.EOF)
                {
                    res.MoveFirst();
                    Helpers.Settings.ZoneMap = res.Fields[0].Value.ToString();
                    Helpers.Settings.ZoneLayer = res.Fields[1].Value.ToString();
                    Helpers.Settings.ZoneTerrStyle = res.Fields[2].Value.ToString();
                    Helpers.Settings.ZoneSZZStyle = res.Fields[3].Value.ToString();
                    Helpers.Settings.ZoneCaptionStyle = res.Fields[4].Value.ToString();
                    Helpers.Settings.ZoneTableAccountNo = res.Fields[5].Value.ToString();
                    Helpers.Settings.ZoneFieldAccountNo = res.Fields[6].Value.ToString();
                    Helpers.Settings.ZoneTableName = res.Fields[7].Value.ToString();
                    Helpers.Settings.ZoneFieldName = res.Fields[8].Value.ToString();
                    Helpers.Settings.ZoneTableRestrict = res.Fields[9].Value.ToString();
                    Helpers.Settings.ZoneFieldRestrict = res.Fields[10].Value.ToString();
                    
                }
            }
            if (coordSysTable != null)
            {
                string[] whereParam = { "1" };
                var res = coordSysTable.SelectData("TransformIn,TransformOut,TransformTypeIn," +
                "TransformTypeOut,TransformEnabled,XtoYChange", "ID=?", whereParam);
                if (!res.EOF)
                {
                    res.MoveFirst();
                    Helpers.Settings.CoordSysTransformIn = res.Fields[0].Value.ToString();
                    Helpers.Settings.CoordSysTransformOut = res.Fields[1].Value.ToString();
                    Helpers.Settings.CoordSysTransformTypeIn = res.Fields[2].Value.ToString();
                    Helpers.Settings.CoordSysTransformTypeOut = res.Fields[3].Value.ToString();
                    Helpers.Settings.CoordSysTransformEnabled = res.Fields[4].Value.ToString();
                    Helpers.Settings.XtoYChange = res.Fields[5].Value.ToString();
                }
            }
        }

        public static void SaveSettings()
        {
            if (lotTable != null && mainForm.lotMapCB.SelectedIndex != -1 &&
                mainForm.lotLayerCB.SelectedIndex != -1 && mainForm.lotStyleCB.SelectedIndex != -1)
            {
                string[] delParam = { "1" };
                lotTable.DeleteData("ID=?", delParam);
                lotTable.Update();
                string[] param = { "1", 
                        ingeoHelper.LotMaps[mainForm.lotMapCB.SelectedIndex].Map.ID,
                        ingeoHelper.LotLayers[mainForm.lotLayerCB.SelectedIndex].Layer.ID,
                        ingeoHelper.LotStyles[mainForm.lotStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.LotCaptionStyles[mainForm.lotStyleSingCB.SelectedIndex].Style.ID,
                        ingeoHelper.LotTablesCadNo[mainForm.lotCadNoTableCB.SelectedIndex].Name,
                        ingeoHelper.LotFieldsCadNo[mainForm.lotCadNoFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.LotTablesAddress[mainForm.lotAddressTableCB.SelectedIndex].Name,
                        ingeoHelper.LotFieldsAddress[mainForm.lotAddressFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.LotTablesArea[mainForm.lotAreaTableCB.SelectedIndex].Name,
                        ingeoHelper.LotFieldsArea[mainForm.lotAreaFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.LotTablesLandCat[mainForm.lotLandCatTableCB.SelectedIndex].Name,
                        ingeoHelper.LotFieldsLandCat[mainForm.lotLandCatFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.LotTablesUsing[mainForm.lotUsingTableCB.SelectedIndex].Name,
                        ingeoHelper.LotFieldsUsing[mainForm.lotUsingFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.LotTablesUsingByDoc[mainForm.lotUsingByDocTableCB.SelectedIndex].Name,
                        ingeoHelper.LotFieldsUsingByDoc[mainForm.lotUsingByDocFieldCB.SelectedIndex].FieldName};

                lotTable.InsertData("ID, Map, Layer, Style, CaptionStyle, TableCadNo,FieldCadNo,TableAddress,FieldAddress," +
                "TableArea,FieldArea,TableLandCat,FieldLandCat,TableUsing,FieldUsing,TableUsingByDoc,FieldUsingByDoc", "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?", param);
                lotTable.Update();
            }
            if (cadBlockTable != null && mainForm.cadBlockMapCB.SelectedIndex != -1 &&
                mainForm.cadBlockLayerCB.SelectedIndex != -1 && mainForm.cadBlockStyleCB.SelectedIndex != -1)
            {
                string[] delParam = { "1" };
                cadBlockTable.DeleteData("ID=?", delParam);
                cadBlockTable.Update();
                string[] param = { "1", 
                        ingeoHelper.CadBlockMaps[mainForm.cadBlockMapCB.SelectedIndex].Map.ID,
                        ingeoHelper.CadBlockLayers[mainForm.cadBlockLayerCB.SelectedIndex].Layer.ID,
                        ingeoHelper.CadBlockStyles[mainForm.cadBlockStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.CadBlockCaptionStyles[mainForm.cadBlockCaptionStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.CadBlockTablesCadNo[mainForm.cadBlockCadNoTableCB.SelectedIndex].Name,
                        ingeoHelper.CadBlockFieldsCadNo[mainForm.cadBlockCadNoFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.CadBlockTablesArea[mainForm.cadBlockAreaTableCB.SelectedIndex].Name,
                        ingeoHelper.CadBlockFieldsArea[mainForm.cadBlockAreaFieldCB.SelectedIndex].FieldName};

                cadBlockTable.InsertData("ID, Map, Layer, Style, CaptionStyle, TableCadNo,FieldCadNo," +
                "TableArea,FieldArea", "?,?,?,?,?,?,?,?,?", param);
                cadBlockTable.Update();
            }
            if (oksTable != null && mainForm.oksMapCB.SelectedIndex != -1 &&
                mainForm.oksLayerCB.SelectedIndex != -1 && mainForm.oksBuildStyleCB.SelectedIndex != -1)
            {
                string[] delParam = { "1" };
                oksTable.DeleteData("ID=?", delParam);
                oksTable.Update();
                string[] param = { "1", 
                        ingeoHelper.OksMaps[mainForm.oksMapCB.SelectedIndex].Map.ID,
                        ingeoHelper.OksLayers[mainForm.oksLayerCB.SelectedIndex].Layer.ID,
                        ingeoHelper.OksBuildStyles[mainForm.oksBuildStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.OksConstrStyles[mainForm.oksConstrStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.OksUncompletedStyles[mainForm.oksUncompletedStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.OksCaptionStyles[mainForm.oksCaptionStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.OksTablesCadNo[mainForm.oksCadNoTableCB.SelectedIndex].Name,
                        ingeoHelper.OksFieldsCadNo[mainForm.oksCadNoFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.OksTablesAddress[mainForm.oksAddressTableCB.SelectedIndex].Name,
                        ingeoHelper.OksFieldsAddress[mainForm.oksAddressFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.OksTablesArea[mainForm.oksAreaTableCB.SelectedIndex].Name,
                        ingeoHelper.OksFieldsArea[mainForm.oksAreaFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.OksTablesType[mainForm.oksTypeTableCB.SelectedIndex].Name,
                        ingeoHelper.OksFieldsType[mainForm.oksTypeFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.OksTablesAssign[mainForm.oksAssignTableCB.SelectedIndex].Name,
                        ingeoHelper.OksFieldsAssign[mainForm.oksAssignFieldCB.SelectedIndex].FieldName};


                oksTable.InsertData("ID, Map, Layer, StyleBuild, StyleConstr, StyleUncompleted, CaptionStyle, TableCadNo,FieldCadNo,TableAddress,FieldAddress," +
                "TableArea,FieldArea,TableType,FieldType,TableAssign,FieldAssign", "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?", param);
                oksTable.Update();
            }
            if (zoneTable != null && mainForm.zoneMapCB.SelectedIndex != -1 &&
                mainForm.zoneLayerCB.SelectedIndex != -1 && mainForm.zoneTerrStyleCB.SelectedIndex != -1
                && mainForm.zoneSZZStyleCB.SelectedIndex != -1)
            {
                string[] delParam = { "1" };
                zoneTable.DeleteData("ID=?", delParam);
                zoneTable.Update();
                string[] param = { "1", 
                        ingeoHelper.ZoneMaps[mainForm.zoneMapCB.SelectedIndex].Map.ID,
                        ingeoHelper.ZoneLayers[mainForm.zoneLayerCB.SelectedIndex].Layer.ID,
                        ingeoHelper.ZoneTerrStyles[mainForm.zoneTerrStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.ZoneSZZStyles[mainForm.zoneSZZStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.ZoneCaptionStyles[mainForm.zoneCaptionStyleCB.SelectedIndex].Style.ID,
                        ingeoHelper.ZoneTablesAccountNo[mainForm.zoneAccountNoTableCB.SelectedIndex].Name,
                        ingeoHelper.ZoneFieldsAccountNo[mainForm.zoneAccountNoFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.ZoneTablesName[mainForm.zoneNameTableCB.SelectedIndex].Name,
                        ingeoHelper.ZoneFieldsName[mainForm.zoneNameFieldCB.SelectedIndex].FieldName,
                        ingeoHelper.ZoneTablesRestrict[mainForm.zoneRestrictTableCB.SelectedIndex].Name,
                        ingeoHelper.ZoneFieldsRestrict[mainForm.zoneRestrictFieldCB.SelectedIndex].FieldName};

                zoneTable.InsertData("ID, Map, Layer, StyleTerr, StyleSZZ, CaptionStyle, TableAccountNo,FieldAccountNo," +
                "TableName,FieldName,TableRestrict,FieldRestrict", "?,?,?,?,?,?,?,?,?,?,?,?", param);
                zoneTable.Update();
            }
            if (coordSysTable != null)
            {
                string[] delParam = { "1" };
                coordSysTable.DeleteData("ID=?", delParam);
                coordSysTable.Update();
                string[] param = { "1", Transforms.FromName, Transforms.OutName, Transforms.Type.ToString(),
                                     Transforms.TypeOut.ToString(), Transforms.IsEnabled.ToString(), Transforms.XtoYChange.ToString() };
                coordSysTable.InsertData("ID,TransformIn,TransformOut,TransformTypeIn,TransformTypeOut," +
                    "TransformEnabled,XtoYChange", "?,?,?,?,?,?,?", param);
                coordSysTable.Update();
            }
        }

        public static bool TemplateTableOpened()
        {
            if (templateTable == null)
                return false;
            else
                return true;
        }

        public static void ClearTemplateTable()
        {
            templateTable.DeleteData();
            templateTable.Update();
        }

        public static void InsertIntoTemplateTable(int id, string Path, string PropName,
            string TableName, string TableNameDb, string FieldName)
        {
            string[] param = { id.ToString(), Path, PropName, TableName, TableNameDb, FieldName };
            templateTable.InsertData("ID,Path,PropName,TableName,TableNameDb,FieldName",
                "?,?,?,?,?,?", param);
            templateTable.Update();
        }
		
		public static IIngeoSemDbTable GetTemplateTable()
        {
            return templateTable;
        }
    }
}
