﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    public class Rect
    {
        public Rect(IIngeoMapObject obj)
        {
            var ing = IngeoHelper.GetActiveIngeo();
            ing.Selection.SelectAlone(obj.ID, 0);
            X1 = double.MaxValue; X2 = double.MinValue;
            Y1 = double.MaxValue; Y2 = double.MinValue;
            X1 = obj.X1; X2 = obj.X2;
            Y1 = obj.Y1; Y2 = obj.Y2;
        }

        public Rect(IIngeoMapObject obj, Rect rect)
        {
            var ing = IngeoHelper.GetActiveIngeo();
            ing.Selection.Select(obj.ID, 0);
            X1 = double.MaxValue; X2 = double.MinValue;
            Y1 = double.MaxValue; Y2 = double.MinValue;
            if (rect == null)
            {
                X1 = obj.X1; X2 = obj.X2;
                Y1 = obj.Y1; Y2 = obj.Y2;
            }
            else
            {
                X1 = obj.X1 < rect.X1 ? obj.X1 : rect.X1;
                X2 = obj.X2 > rect.X2 ? obj.X2 : rect.X2;
                Y1 = obj.Y1 < rect.Y1 ? obj.Y1 : rect.Y1;
                Y2 = obj.Y2 > rect.Y2 ? obj.Y2 : rect.Y2;
            }
        }

        public double X1, X2, Y1, Y2;
    }

    public static class Settings
    {
        public static bool IsZoom { get; set; }
        public static bool IsAllObjectsZoom { get; set; }
        public static Rect ZoomRect { get; set; }

        public static string SettingsDataBaseName { get; set; }

        // блок ЗУ
        public static string LotMap { get; set; }
        public static string LotLayer { get; set; }
        public static string LotStyle { get; set; }
        public static string LotCaptionStyle { get; set; }
        public static string LotTableCadNo { get; set; }
        public static string LotFieldCadNo { get; set; }
        public static string LotTableAddress { get; set; }
        public static string LotFieldAddress { get; set; }
        public static string LotTableArea { get; set; }
        public static string LotFieldArea { get; set; }
        public static string LotTableLandCat { get; set; }
        public static string LotFieldLandCat { get; set; }
        public static string LotTableUsing { get; set; }
        public static string LotFieldUsing { get; set; }
        public static string LotTableUsingByDoc { get; set; }
        public static string LotFieldUsingByDoc { get; set; }

        // блок кадастрового квартала
        public static string CadBlockMap { get; set; }
        public static string CadBlockLayer { get; set; }
        public static string CadBlockStyle { get; set; }
        public static string CadBlockCaptionStyle { get; set; }
        public static string CadBlockTableCadNo { get; set; }
        public static string CadBlockFieldCadNo { get; set; }
        public static string CadBlockTableArea { get; set; }
        public static string CadBlockFieldArea { get; set; }

        // блок ОКС
        public static string OksMap { get; set; }
        public static string OksLayer { get; set; }
        public static string OksBuildStyle { get; set; }
        public static string OksConstrStyle { get; set; }
        public static string OksUncompletedStyle { get; set; }
        public static string OksCaptionStyle { get; set; }
        public static string OksTableCadNo { get; set; }
        public static string OksFieldCadNo { get; set; }
        public static string OksTableAddress { get; set; }
        public static string OksFieldAddress { get; set; }
        public static string OksTableArea { get; set; }
        public static string OksFieldArea { get; set; }
        public static string OksTableType { get; set; }
        public static string OksFieldType { get; set; }
        public static string OksTableAssign { get; set; }
        public static string OksFieldAssign { get; set; }

        // блок зон
        public static string ZoneMap { get; set; }
        public static string ZoneLayer { get; set; }
        public static string ZoneTerrStyle { get; set; }
        public static string ZoneSZZStyle { get; set; }
        public static string ZoneCaptionStyle { get; set; }
        public static string ZoneTableAccountNo { get; set; }
        public static string ZoneFieldAccountNo { get; set; }
        public static string ZoneTableName { get; set; }
        public static string ZoneFieldName { get; set; }
        public static string ZoneTableRestrict { get; set; }
        public static string ZoneFieldRestrict { get; set; }
       

        static string _coordSysTransformIn;
        public static string CoordSysTransformIn
        {
            get { return _coordSysTransformIn; }
            set
            {
                _coordSysTransformIn = value;
                Helpers.Transforms.FromName = value;
            }
        }

        static string _coordSysTransformOut;
        public static string CoordSysTransformOut {
            get { return _coordSysTransformOut; }
            set
            {
                _coordSysTransformOut = value;
                Helpers.Transforms.OutName = value;
            }
        }

        static string _coordSysTransformTypeIn;
        public static string CoordSysTransformTypeIn {
            get { return _coordSysTransformTypeIn; }
            set
            {
                _coordSysTransformTypeIn = value;
                bool temp;
                bool.TryParse(value, out temp);
                Helpers.Transforms.Type = temp;
            }
        }

        static string _coordSysTransformTypeOut;
        public static string CoordSysTransformTypeOut {
            get { return _coordSysTransformTypeOut; }
            set
            {
                _coordSysTransformTypeOut = value;
                bool temp;
                bool.TryParse(value, out temp);
                Helpers.Transforms.TypeOut = temp;
            }
        }

        static string _coordSysTransformEnabled;
        public static string CoordSysTransformEnabled
        {
            get { return _coordSysTransformEnabled; }
            set
            {
                _coordSysTransformEnabled = value;
                bool temp;
                bool.TryParse(value, out temp);
                Helpers.Transforms.IsEnabled = temp;
            }
        }
        static string _XtoYChange;
        public static string XtoYChange
        {
            get { return _XtoYChange; }
            set
            {
                _XtoYChange = value;
                bool temp;
                bool.TryParse(value, out temp);
                Helpers.Transforms.XtoYChange = temp;
            }
        }
        public static string DbLogin { get; set; }
        public static string DbPassword { get; set; }
        public static string DbServer { get; set; }
        public static string DbName { get; set; }
    }
}
