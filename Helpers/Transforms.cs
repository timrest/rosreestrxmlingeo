﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Helpers
{
    public static class Transforms
    {
        public static string FromName { get; set; }
        public static string OutName { get; set; }
        /// <summary>
        /// true - математическая, false - геодезическая, null - без пересчета
        /// </summary>
        public static bool Type { get; set; }
        public static bool TypeOut { get; set; }
        public static bool IsEnabled { get; set; }
        public static bool XtoYChange { get; set; }

        public static void TransformCoordinates(double x, double y, out double X, out double Y)
        {
            GeoTransformer.GeoTransformClass gt = new GeoTransformer.GeoTransformClass();
            gt.InitByXmlFile(Application.StartupPath + "/GeoTransform/geotransform.xml");
            gt.SourceSys = FromName;
            gt.DestSys = OutName;
            double outX, outY, outZ = 0.0;
            if (Type)
                gt.TransformOnePoint(x, y, 0, out outX, out outY, out outZ);
            else
                gt.TransformOnePoint(y, x, 0, out outX, out outY, out outZ);
            if (TypeOut)
            {
                X = outX;
                Y = outY;
            }
            else
            {
                X = outY;
                Y = outX;
            }
        }

        public static void Start(IIngeoContourPart contour, double spelX, double spelY)
        {
            double x, y;
            try
            {
                if (IsEnabled)
                    Transforms.TransformCoordinates(spelY, spelX, out x, out y);
                else
                {
                    if (!XtoYChange)
                    {
                        x = spelX;
                        y = spelY;
                    }
                    else
                    {
                        x = spelY;
                        y = spelX;
                    }
                }
                contour.InsertVertex(-1, x, y, 0);
            }
            catch
            {
                contour.InsertVertex(-1, spelY, spelX, 0);
            }
        }

        public static void StartNew(IIngeoContourPart contour, decimal spX, decimal spY)
        {
            double x, y;
            double spelX = Convert.ToDouble(spX);
            double spelY = Convert.ToDouble(spY);
            try
            {
                if (IsEnabled)
                    Transforms.TransformCoordinates(spelY, spelX, out x, out y);
                else
                {
                    x = spelX;
                    y = spelY;
                }
                contour.InsertVertex(-1, x, y, 0);
            }
            catch
            {
                contour.InsertVertex(-1, spelY, spelX, 0);
            }
        }

    }
}
