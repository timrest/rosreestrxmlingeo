﻿using System.Windows.Forms;
using System.Xml;

namespace Helpers
{
    public class XmlHelper
    {
        public string GetVersion(string fileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(fileName);
            }
            catch { return string.Empty; }
            string type = "0";
            string version = "0";

            if (doc["Region_Cadastr_Vidimus_KV"] != null)
            {
                type = "114";
                if (doc["Region_Cadastr_Vidimus_KV"]["eDocument"] != null)
                    version = doc["Region_Cadastr_Vidimus_KV"]["eDocument"].Attributes["Version"].Value;
                else
                    version = doc["Region_Cadastr_Vidimus_KV"].Attributes["Version"].Value;
            }
            if (doc["Region_Cadastr"] != null)
            {
                type = "101";
                if (doc["Region_Cadastr"]["eDocument"] != null)
                    version = doc["Region_Cadastr"]["eDocument"].Attributes["Version"].Value;
                else
                    version = doc["Region_Cadastr"].Attributes["Version"].Value;
            }
            if (doc["Region_Cadastr_Vidimus_KP"] != null)
            {
                type = "114";
                if (doc["Region_Cadastr_Vidimus_KP"]["eDocument"] != null)
                    version = doc["Region_Cadastr_Vidimus_KP"]["eDocument"].Attributes["Version"].Value;
                else
                    version = doc["Region_Cadastr_Vidimus_KP"].Attributes["Version"].Value;
            }
            if (doc["KVZU"] != null)
            {
                type = "114";
                if (doc["KVZU"].NamespaceURI == "urn://x-artefacts-rosreestr-ru/outgoing/kvzu/6.0.9")
                    version = "06";
                else
                    if (doc["KVZU"].NamespaceURI == "urn://x-artefacts-rosreestr-ru/outgoing/kvzu/7.0.1")
                        version = "07";
            }
            if (doc["KPT"] != null)
            {
                type = "101";
                if (doc["KPT"].NamespaceURI == "urn://x-artefacts-rosreestr-ru/outgoing/kpt/9.0.3")
                    version = "09";
                else
                    if (doc["KPT"].NamespaceURI == "urn://x-artefacts-rosreestr-ru/outgoing/kpt/10.0.1")
                        version = "10";
            }
            if (doc["KPZU"] != null)
            {
                type = "114";
                if (doc["KPZU"].NamespaceURI == "urn://x-artefacts-rosreestr-ru/outgoing/kpzu/5.0.8")
                    version = "05";
                else
                    if (doc["KPZU"].NamespaceURI == "urn://x-artefacts-rosreestr-ru/outgoing/kpzu/6.0.1")
                        version = "06";
            }
            //MessageBox.Show(type + ":" + version);
            return type + ":" + version;
        }
    }
}
