﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Helpers
{
    public class XmlTemplateItem
    {
        public string Path { get; set; }
        public string PropName { get; set; }
        public string TableName { get; set; }
        public string TableNameDb { get; set; }
        public string FieldName { get; set; }
    }

    public class XmlTemplate
    {
        public static XmlTemplate Template { get; set; }

        public XmlTemplate()
        {
            templates = new List<XmlTemplateItem>();
        }

        List<XmlTemplateItem> templates;

        public int Count
        {
            get { return templates.Count; }
        }

        public XmlTemplateItem this[string path, string propName]
        {
            get
            {
                return templates.SingleOrDefault(mc =>
                    mc.Path.Substring(mc.Path.LastIndexOf("Parcel")).Equals(path) &&
                    mc.PropName.Equals(propName));
            }
        }

        public XmlTemplateItem this[int index]
        {
            get
            {
                return templates[index];
            }
        }

        public XmlTemplateItem this[string propName]
        {
            get
            {
                try
                {
                    return templates.SingleOrDefault(mc => mc.PropName.Equals(propName));
                }
                catch
                {
                    return null;
                }
            }
        }

        public void Remove(string path, string propName)
        {
            templates.RemoveAll(mc => mc.Path == path && mc.PropName == propName);
        }

        public void Add(XmlTemplateItem item)
        {
            if (Template[item.Path, item.PropName] == null)
                templates.Add(item);
        }

        public void CreateTemplate(string templateStr)
        {
            foreach (string s in templateStr.Split('\n'))
                if (!s.Equals(string.Empty))
                {
                    XmlTemplateItem item = new XmlTemplateItem();
                    foreach (string parms in s.Split('&'))
                    {
                        string[] param = parms.Trim().Split('=');
                        switch (param[0])
                        {
                            case "Path": item.Path = param[1]; break;
                            case "PropName": item.PropName = param[1]; break;
                            case "TableName": item.TableName = param[1]; break;
                            case "TableNameDb": item.TableNameDb = param[1]; break;
                            case "FieldName": item.FieldName = param[1]; break;
                        }
                    }
                    if (item.Path != null && item.PropName != null && item.TableName != null &&
                            item.TableNameDb != null && item.FieldName != null)
                        XmlTemplate.Template.Add(item);
                }
        }

        public static string GetPath(XElement el)
        {
            List<string> path = new List<string>();

            string pathStr = string.Empty;
            while (el != null)
            {
                
                    path.Add(el.Name.ToString());
                    el = el.Parent;
            }
            path.Reverse();
            foreach (string s in path)
                pathStr += s + "/";

            if (pathStr == "{urn://x-artefacts-rosreestr-ru/outgoing/kvzu/6.0.9}Parcel/")
                pathStr = "Parcel";
            return pathStr.TrimEnd('/');
        }
    }
}
