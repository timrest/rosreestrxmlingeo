﻿using Helpers;
using Ingeo;
using KPT08_spatial;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KPT08
{
    public class Importer
    {
        private Region_Cadastr KPT;
        private IngeoApplication ingeo;

        #region
        public string LotLayer;
        //public string LotStyle;
        public List<IIngeoStyleView> LotStyles;
        public string LotCaptionStyle;
        public string LotTableCadNo;
        public string LotFieldCadNo;
        public string LotTableAddress;
        public string LotFieldAddress;
        public string LotTableArea;
        public string LotFieldArea;
        public string LotTableLandCat;
        public string LotFieldLandCat;
        public string LotTableUsing;
        public string LotFieldUsing;
        public string LotTableUsingByDoc;
        public string LotFieldUsingByDoc;

        public string CadBlockLayer;
        public string CadBlockStyle;
        public string CadBlockCaptionStyle;
        public string CadBlockTableCadNo;
        public string CadBlockFieldCadNo;
        public string CadBlockTableArea;
        public string CadBlockFieldArea;

        public string OksLayer;
        public string OksBuildStyle;
        public string OksConstrStyle;
        public string OksUncompletedStyle;
        public string OksCaptionStyle;
        public string OksTableCadNo;
        public string OksFieldCadNo;
        public string OksTableAddress;
        public string OksFieldAddress;
        public string OksTableArea;
        public string OksFieldArea;
        public string OksTableType;
        public string OksFieldType;
        public string OksTableAssign;
        public string OksFieldAssign;

        public string ZoneLayer;
        public string ZoneTerrStyle;
        public string ZoneSZZStyle;
        public string ZoneCaptionStyle;
        public string ZoneTableAccountNo;
        public string ZoneFieldAccountNo;
        public string ZoneTableName;
        public string ZoneFieldName;
        public string ZoneTableRestrict;
        public string ZoneFieldRestrict;
        #endregion

        public string FileType { get; set; }


        bool isModify = false;

        public Importer(IngeoHelper Ingeo)
        {
            ingeo = Ingeo.Application;
        }

        public void Import(Region_Cadastr kpt, tKPT08_spatial kpt_spatial)
        {
            KPT = kpt;
            tCadastralBlockSpatial cadBlock_spatial = null;
            tParcelSpatial parcelSpatial = null;
            //tObjectsRealtyObjectRealtySpatial objectRealtySpatial = null;
            tZoneSpatial zoneSpatial = null;

            DateTime start = DateTime.Now;
            ProgressBarLogger.Writer.WriteLine("Начало импорта: " + start.ToString());
            // Импортируем описание документа
            foreach (tCadastral_Block block in kpt.Package.Cadastral_Blocks)
            {
                isModify = false;
                // по кадастровому номеру находим нужный нам кадастровый квартал(координатное описание)
                foreach (tCadastralBlockSpatial cadB in kpt_spatial.CadastralBlocks)
                {
                    if (cadB.CadastralNumber == block.CadastralNumber)
                    {
                        cadBlock_spatial = cadB;
                        break;
                    }
                }
                
                // Импортируем пространственные данные
                IIngeoMapObject mapObjectCadBlock = null;
                if (block.SpatialData != null)
                    if (block.SpatialData.Entity_Spatial!= null)
                        mapObjectCadBlock = ImportGeometryBlock(cadBlock_spatial, block);
                //else if (parcel.Contours != null && parcel.Contours.Count > 0)
                //    mapObject = ImportGeometry(parcel.Contours, parcel.CadastralNumber);
                string temp = "";
                if (mapObjectCadBlock == null)
                    temp = " - координаты кадастрового квартала не найдены. Пространственный объект квартала не создан.";
                else
                {
                    if(isModify)
                        temp = " - пространственный объект кадастрового квартала обновлен.";
                    else
                        temp = " - создан пространственный объект кадастрового квартала.";
                }
                ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + block.CadastralNumber + temp);
                int maxValue = 0;
                if (block.Parcels != null)
                {
                    maxValue = block.Parcels.Length;
                }
                //if (block.ObjectsRealty != null)
                //{
                //    maxValue += block.ObjectsRealty.Length;
                //}
                if (block.Zones != null)
                {
                    maxValue += block.Zones.Length;
                }
                ProgressBarLogger.MaxValue = maxValue;
                #region //импортируем участки
                if (block.Parcels != null)
                {
                    //ProgressBarLogger.MaxValue = block.Parcels.Length;
                    foreach (tParcel parcel in block.Parcels)
                    {
                        isModify = false;
                        ProgressBarLogger.ProgressInc();
                        foreach (tParcelSpatial pSp in cadBlock_spatial.Parcels)
                        {
                            if (pSp.CadastralNumber == parcel.CadastralNumber)
                            {
                                parcelSpatial = pSp;
                                break;
                            }
                        }
                        // Импортируем пространственные данные участков

                        IIngeoMapObject mapObjectLot = null;
                        if (parcelSpatial != null)
                        {
                            //if (parcelSpatial.EntitySpatial != null)
                            //{
                            
                                mapObjectLot = ImportGeometryLot(parcelSpatial, parcel);
                            
                            if (mapObjectLot != null)
                            {
                                try
                                {
                                    if (isModify)
                                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " обновлен");
                                    else
                                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " добавлен");
                                }
                                catch { }
                                //if (Settings.IsZoom)
                                //{
                                //    if (Settings.IsAllObjectsZoom)
                                //        Settings.ZoomRect = new Rect(mapObjectLot, Settings.ZoomRect);
                                //    else
                                //        Settings.ZoomRect = new Rect(mapObjectLot);
                                //}
                            }
                            else
                                ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber +
                            " - информации о координатах земельного участка не найдено! Импорт данного участка не произведен!");
                        }
                        else
                            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber +
                            " - информации о координатах земельного участка не найдено! Импорт данного участка не произведен!");
                    }
                }
                #endregion

                #region  //импортируем объекты недвижимости
                //if (block.ObjectsRealty != null)
                //{
                //    //ProgressBarLogger.MaxValue += block.ObjectsRealty.Length;
                //    foreach (tObjectsRealtyObjectRealty objectRealty in block.ObjectsRealty)
                //    {
                //        isModify = false;
                //        string cadNum = "";
                //        ProgressBarLogger.ProgressInc();
                //        if (objectRealty.Item.ToString() == "KPT10.tBuilding")
                //        {
                //            tBuilding building = (tBuilding)objectRealty.Item;
                //            foreach (tObjectsRealtyObjectRealtySpatial orSp in cadBlock_spatial.ObjectsRealty)
                //            {
                //                if (orSp.BuildingSpatial != null)
                //                {
                //                    if (orSp.BuildingSpatial.CadastralNumber == building.CadastralNumber)
                //                    {
                //                        objectRealtySpatial = orSp;
                //                        cadNum = building.CadastralNumber;
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //        if (objectRealty.Item.ToString() == "KPT10.tConstruction")
                //        {
                //            tConstruction oks = (tConstruction)objectRealty.Item;
                //            foreach (tObjectsRealtyObjectRealtySpatial orSp in cadBlock_spatial.ObjectsRealty)
                //            {
                //                if (orSp.ConstructionSpatial != null)
                //                {
                //                    if (orSp.ConstructionSpatial.CadastralNumber == oks.CadastralNumber)
                //                    {
                //                        objectRealtySpatial = orSp;
                //                        cadNum = oks.CadastralNumber;
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //        if (objectRealty.Item.ToString() == "KPT10.tUncompleted")
                //        {
                //            tUncompleted oks = (tUncompleted)objectRealty.Item;
                //            foreach (tObjectsRealtyObjectRealtySpatial orSp in cadBlock_spatial.ObjectsRealty)
                //            {
                //                if (orSp.UncompletedSpatial != null)
                //                {
                //                    if (orSp.UncompletedSpatial.CadastralNumber == oks.CadastralNumber)
                //                    {
                //                        objectRealtySpatial = orSp;
                //                        cadNum = oks.CadastralNumber;
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //        // Импортируем пространственные данные

                //        IIngeoMapObject mapObjectObjectRealty = null;
                //        if (objectRealtySpatial != null)
                //        {
                //            mapObjectObjectRealty = ImportGeometryOblectRealty(objectRealtySpatial, objectRealty);

                //            if (mapObjectObjectRealty != null)
                //            {
                //                try
                //                {
                //                    if (isModify)
                //                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum + " обновлен");
                //                    else
                //                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum + " добавлен");
                //                }
                //                catch { }
                //            }
                //            else
                //                ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum +
                //            " - информации о координатах объекта строительства не найдено! Импорт данного объекта не произведен!");
                //        }
                //        else
                //            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum +
                //            " - информации о координатах объекта строительства не найдено! Импорт данного объекта не произведен!");
                //    }
                //}
                #endregion

                #region  //импортируем зоны
                if (block.Zones != null)
                {
                    //ProgressBarLogger.MaxValue += block.Zones.Length;
                    foreach (tCadastral_BlockZone zone in block.Zones)
                    {
                        isModify = false;
                        string cadNum = "";
                        ProgressBarLogger.ProgressInc();

                        foreach (tZoneSpatial zSp in cadBlock_spatial.Zones)
                            {
                                if (zSp.AccountNumber == zone.AccountNumber)
                                {
                                    zoneSpatial = zSp;
                                    cadNum = zone.AccountNumber;
                                    break;
                                }
                            }
                        // Импортируем пространственные данные

                        IIngeoMapObject mapObjectZone = null;
                        if (zoneSpatial != null)
                        {
                            //if (parcelSpatial.EntitySpatial != null)
                            //{

                            mapObjectZone = ImportGeometryZone(zoneSpatial, zone);

                            if (mapObjectZone != null)
                            {
                                try
                                {
                                    if (isModify)
                                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum + " обновлен");
                                    else
                                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum + " добавлен");
                                }
                                catch { }
                                //if (Settings.IsZoom)
                                //{
                                //    if (Settings.IsAllObjectsZoom)
                                //        Settings.ZoomRect = new Rect(mapObjectZone, Settings.ZoomRect);
                                //    else
                                //        Settings.ZoomRect = new Rect(mapObjectZone);
                                //}
                            }
                            else
                                ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum +
                            " - информации о координатах зоны не найдено! Импорт данного объекта не произведен!");
                        }
                        else
                            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + cadNum +
                            " - информации о координатах зоны не найдено! Импорт данного объекта не произведен!");
                    }
                }
                #endregion

                if (mapObjectCadBlock != null)
                {
                    if (Settings.IsZoom)
                    {
                        if (Settings.IsAllObjectsZoom)
                            Settings.ZoomRect = new Rect(mapObjectCadBlock, Settings.ZoomRect);
                        else
                            Settings.ZoomRect = new Rect(mapObjectCadBlock);
                    }
                }
                DateTime finish = DateTime.Now;

                ProgressBarLogger.Writer.WriteLine("Окончание импорта: " + finish.ToString());
                ProgressBarLogger.Writer.WriteLine("Время импорта: " + (finish-start).ToString());
            }
        }

        private string itemEnumCheck(string item)
        {
            string newItem = "";
            if (item.Contains("Item"))
            {
                newItem = item.Replace("Item", "");
            }
            else
                newItem = item;
            return newItem;
        }



        private IIngeoMapObject ImportGeometryBlock(KPT08_spatial.tCadastralBlockSpatial cadBlock_spatial, tCadastral_Block cadBlock)
        {
            IIngeoMapObject mapObject = null;
            List<IIngeoShape> shapesToDel = new List<IIngeoShape>();
            
            if (cadBlock_spatial.SpatialData != null)
                if (cadBlock_spatial.SpatialData.EntitySpatial != null)
                    if (cadBlock_spatial.SpatialData.EntitySpatial.SpatialElements != null)
                    {
                        // ищем пространственный объект квартала
                        mapObject = IngeoHelper.GetOneMapObject(CadBlockLayer, CadBlockTableCadNo, CadBlockFieldCadNo, cadBlock.CadastralNumber);
                        if (mapObject != null)
                        {
                            foreach (IIngeoShape sh in mapObject.Shapes)
                                shapesToDel.Add(sh);
                            isModify = true;
                        }
                        else
                            mapObject = ingeo.ActiveDb.MapObjects.AddObject(CadBlockLayer);
                        foreach (IIngeoShape s in shapesToDel)
                        {
                            s.Delete();
                        }
                        int i = 0;
                        IIngeoShape mainshape = null;
                        List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                        // Считаем, что первый shape (tSpatialElement) - основной контур, остальные вырезаются из него - 'это не верно
                        // перебираем все контуры, смотрим, какой самый большой - из него вычетаем
                        
                        foreach (tSpatialElement spatialElement in cadBlock_spatial.SpatialData.EntitySpatial.SpatialElements)
                        {
                            IIngeoShape shape = mapObject.Shapes.Insert(-1, CadBlockStyle);
                            IIngeoContourPart contour = shape.Contour.Insert(-1);
                            foreach (tSpelementUnit spelementUnit in spatialElement.SpelementUnits)
                            {
                                Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                                
                            }
                            contour.Closed = true;
                            if (i == 0)
                                mainshape = shape;
                            else
                            {
                                if (mainshape.Contour.Square > shape.Contour.Square)
                                {
                                    holeshapes.Add(shape);
                                }
                                else
                                {
                                    holeshapes.Add(mainshape);
                                    mainshape = shape;
                                }
                            }
                            mapObject.MapObjects.UpdateChanges();
                            i++;
                        }
                        
                        shapesToDel = new List<IIngeoShape>();
                        foreach (IIngeoShape shape in holeshapes)
                        {

                            if (shape != mainshape)
                            {
                                mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                                shapesToDel.Add(shape);
                            }
                        }
                        i = 0;
                        foreach (IIngeoShape s in shapesToDel)
                        {
                            s.Delete();
                        }
                        mapObject.MapObjects.UpdateChanges();


                        // заполняем семантику
                        try
                        {
                            mapObject.SemData.SetValue(CadBlockTableCadNo, CadBlockFieldCadNo, cadBlock.CadastralNumber, 0);
                        }
                        catch { }
                        try
                        {
                            if (cadBlock.Area.Total != null)
                                mapObject.SemData.SetValue(CadBlockTableArea, CadBlockFieldArea, cadBlock.Area.Total, 0);
                        }
                        catch { }

                        mapObject.MapObjects.UpdateChanges();
                        // создаем подпись
                        try
                        {
                            IngeoHelper.CreateMapObjectCaption(mapObject, CadBlockCaptionStyle);
                        }
                        catch { }
                        mapObject.MapObjects.UpdateChanges();
                        IIngeoMapObjects mobjects = mapObject.MapObjects;
                        mobjects.UpdateChanges();
                    }
            return mapObject;
        }

        /// <summary>
        /// Импортируем геометрию земельного участка
        /// </summary>
        /// <param name="parcelSpatial">описание координатной части участка</param>
        /// <param name="cadNo">кадастровый номер участка</param>
        /// <param name="address">адрес участка</param>
        /// <returns>Пространственный объект земельного участка</returns>
        private IIngeoMapObject ImportGeometryLot(tParcelSpatial parcelSpatial, tParcel parcel)
        {
            IIngeoMapObject mapObject = null;
            List<IIngeoShape> shapesToDel = new List<IIngeoShape>();
            
            // находим стиль по коду в названии по коду категории
            string LotStyle = "";
            string categoryCode = "";
            try
            {
                categoryCode = itemEnumCheck(parcel.Category.Category.ToString());
                LotStyle = LotStyles.Find(x => x.Style.Name.Contains(categoryCode)).Style.ID;
            }
            catch { }
            if (LotStyle != "")
            {
                #region if (parcelSpatial.EntitySpatial != null)
                if (parcelSpatial.EntitySpatial != null)
                {
                    // ищем пространственный объект участка
                    mapObject = IngeoHelper.GetOneMapObject(LotLayer, LotTableCadNo, LotFieldCadNo, parcel.CadastralNumber);
                    if (mapObject != null)
                    {
                        foreach (IIngeoShape sh in mapObject.Shapes)
                            shapesToDel.Add(sh);
                        isModify = true;
                    }
                    else
                        mapObject = ingeo.ActiveDb.MapObjects.AddObject(LotLayer);
                    foreach (IIngeoShape s in shapesToDel)
                    {
                        s.Delete();
                    }
                    List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                    // Считаем, что первый shape (tSpatialElement) - основной контур, остальные вырезаются из него - 'это не верно
                    // перебираем все контуры, смотрим, какой самый большой - из него вычетаем
                    int i = 0;
                    IIngeoShape mainshape = null;
                    foreach (tSpatialElement spelem in parcelSpatial.EntitySpatial.SpatialElements)
                    {
                        IIngeoShape shape = mapObject.Shapes.Insert(-1, LotStyle);
                        IIngeoContourPart contour = shape.Contour.Insert(-1);
                        foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
                        {
                            Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                        }
                        contour.Closed = true;
                        if (i == 0)
                            mainshape = shape;
                        else
                        {
                            if (mainshape.Contour.Square > shape.Contour.Square)
                            {
                                holeshapes.Add(shape);
                            }
                            else
                            {
                                holeshapes.Add(mainshape);
                                mainshape = shape;
                            }
                        }
                        mapObject.MapObjects.UpdateChanges();
                        i++;
                    }
                    shapesToDel = new List<IIngeoShape>();
                    foreach (IIngeoShape shape in mapObject.Shapes)
                    {

                        if (shape != mainshape)
                        {
                            mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                            shapesToDel.Add(shape);
                        }
                    }

                    foreach (IIngeoShape s in shapesToDel)
                    {
                        s.Delete();
                    }
                    mapObject.MapObjects.UpdateChanges();

                }
                #endregion
                #region else if (parcelSpatial.Contours != null)
                else
                    if (parcelSpatial.Contours != null)
                    {
                        mapObject = IngeoHelper.GetOneMapObject(LotLayer, LotTableCadNo, LotFieldCadNo, parcel.CadastralNumber);
                        if (mapObject != null)
                        {
                            foreach (IIngeoShape sh in mapObject.Shapes)
                                shapesToDel.Add(sh);
                            isModify = true;
                        }
                        else
                            mapObject = ingeo.ActiveDb.MapObjects.AddObject(LotLayer);
                        foreach (IIngeoShape s in shapesToDel)
                        {
                            s.Delete();
                        }
                        foreach (KPT08_spatial.tContour contour in parcelSpatial.Contours)
                        {
                            if (contour.EntitySpatial != null)
                            {
                                // Считаем, что первый shape - основной контур, остальные вырезаются из него
                                int i = 0;
                                IIngeoShape mainshape = null;
                                List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                                foreach (tSpatialElement spelem in contour.EntitySpatial.SpatialElements)
                                {
                                    IIngeoShape shape = mapObject.Shapes.Insert(-1, LotStyle);
                                    IIngeoContourPart cont = shape.Contour.Insert(-1);
                                    foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
                                    {
                                        Transforms.Start(cont, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                                    }
                                    cont.Closed = true;
                                    if (i == 0)
                                        mainshape = shape;
                                    else
                                    {
                                        if (mainshape.Contour.Square > shape.Contour.Square)
                                        {
                                            holeshapes.Add(shape);
                                        }
                                        else
                                        {
                                            holeshapes.Add(mainshape);
                                            mainshape = shape;
                                        }
                                    }
                                    mapObject.MapObjects.UpdateChanges();
                                    i++;
                                }
                                shapesToDel = new List<IIngeoShape>();
                                foreach (IIngeoShape shape in holeshapes)
                                {

                                    if (shape != mainshape)
                                    {
                                        mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                                        shapesToDel.Add(shape);
                                    }
                                }

                                foreach (IIngeoShape s in shapesToDel)
                                {
                                    s.Delete();
                                }
                            }
                        }
                    }
                #endregion
            }
            if (mapObject != null)
            {
                try
                {
                    mapObject.SemData.SetValue(LotTableCadNo, LotFieldCadNo, parcel.CadastralNumber, 0);
                }
                catch { }
                #region Адрес
                try
                {
                    
                    if (parcel.Location.Address != null)
                    {
                        if (parcel.Location.Address.Note != null)
                            mapObject.SemData.SetValue(LotTableAddress, LotFieldAddress, parcel.Location.Address.Note, 0);
                        else
                        {
                            string addr = "";
                            try
                            {
                                string item = "";
                                if (parcel.Location.Address.Region != null)
                                {
                                    item = itemEnumCheck(parcel.Location.Address.Region.ToString());
                                    foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.RegionClassifiers)
                                    {
                                        if (cl.Code == item)
                                        {
                                            addr = cl.Value + ", ";
                                            break;
                                        }
                                    }
                                }
                            }
                            catch { }
                            try
                            {
                                if (parcel.Location.Address.District != null)
                                {
                                    addr += String.Format("{0} {1}, ", parcel.Location.Address.District.Type, parcel.Location.Address.District.Name);
                                }
                            }
                            catch { }
                            try
                            {
                                if (parcel.Location.Address.Locality != null)
                                {
                                    addr += String.Format("{0} {1}, ", parcel.Location.Address.Locality.Type, parcel.Location.Address.Locality.Name);
                                }
                            }
                            catch { }
                            try
                            {
                                if (parcel.Location.Address.Street != null)
                                {
                                    addr += String.Format("{0} {1}, ", parcel.Location.Address.Street.Type, parcel.Location.Address.Street.Name);
                                }
                            }
                            catch { }
                            try
                            {
                                if (parcel.Location.Address.Level1 != null)
                                {
                                    addr += String.Format("{0} {1} ", parcel.Location.Address.Level1.Type, parcel.Location.Address.Level1.Value);
                                }
                            }
                            catch { }
                            try
                            {
                                if (parcel.Location.Address.Level2 != null)
                                {
                                    addr += String.Format("{0} {1} ", parcel.Location.Address.Level2.Type, parcel.Location.Address.Level2.Value);
                                }
                            }
                            catch { }
                            try
                            {
                                if (parcel.Location.Address.Level3 != null)
                                {
                                    addr += String.Format("{0} {1} ", parcel.Location.Address.Level3.Type, parcel.Location.Address.Level3.Value);
                                }
                            }
                            catch { }
                            mapObject.SemData.SetValue(LotTableAddress, LotFieldAddress, addr.Trim(), 0);
                        }
                    }
                }
                catch { }
                #endregion
                try
                {
                    mapObject.SemData.SetValue(LotTableArea, LotFieldArea, parcel.Area.Area, 0);
                }
                catch { }

                #region Категория земель
                try
                {
                    string item = "";
                    if (parcel.Category != null)
                    {
                        item = itemEnumCheck(parcel.Category.Category.ToString());
                        foreach (ClassifiersHelper.ClassifiersItem i in ClassifiersHelper.LandCatClassifiers)
                        {
                            if (i.Code == item)
                            {
                                item = i.Value;
                                break;
                            }
                        }
                    }
                    mapObject.SemData.SetValue(LotTableLandCat, LotFieldLandCat, item, 0);
                }
                catch { }
                #endregion

                #region Использование
                try
                {
                    string item = "";
                    if (parcel.Utilization != null)
                    {
                        item = itemEnumCheck(parcel.Utilization.Kind.ToString());
                        foreach (ClassifiersHelper.ClassifiersItem i in ClassifiersHelper.UsingClassifiers)
                        {
                            if (i.Code == item)
                            {
                                item = i.Value;
                                break;
                            }
                        }
                    }
                    mapObject.SemData.SetValue(LotTableUsing, LotFieldUsing, item, 0);
                }
                catch { }
                #endregion
                try
                {
                    mapObject.SemData.SetValue(LotTableUsingByDoc, LotFieldUsingByDoc, parcel.Utilization.ByDoc, 0);
                }
                catch { }

                // создаем подпись
                try
                {
                    IngeoHelper.CreateMapObjectCaption(mapObject, LotCaptionStyle);
                }
                catch { }

                mapObject.MapObjects.UpdateChanges();
            }
            return mapObject;
        }

        /// <summary>
        /// Импортируем геометрию объекта капитального строительства
        /// </summary>
        /// <param name="parcelSpatial">описание координатной части участка</param>
        /// <param name="cadNo">кадастровый номер участка</param>
        /// <param name="address">адрес участка</param>
        /// <returns>Пространственный объект земельного участка</returns>
        //private IIngeoMapObject ImportGeometryOblectRealty(tObjectsRealtyObjectRealtySpatial objectRealtySpatial, tObjectsRealtyObjectRealty objectRealty)
        //{
        //    IIngeoMapObject mapObject = null;
        //    List<IIngeoShape> shapesToDel = new List<IIngeoShape>();

        //    //mapObject.MapObjects.UpdateChanges();

        //    #region if (objectRealtySpatial.BuildingSpatial != null)
        //    if (objectRealtySpatial.BuildingSpatial != null)
        //    {
        //        if (objectRealtySpatial.BuildingSpatial.EntitySpatial != null)
        //        {
        //            tBuilding building = (tBuilding)objectRealty.Item;
        //            #region создание пространственного объекта
        //            // ищем пространственный объект
        //            mapObject = IngeoHelper.GetOneMapObject(OksLayer, OksTableCadNo, OksFieldCadNo, building.CadastralNumber);
        //            if (mapObject != null)
        //            {
        //                foreach (IIngeoShape sh in mapObject.Shapes)
        //                    shapesToDel.Add(sh);
        //                isModify = true;
        //            }
        //            else
        //                mapObject = ingeo.ActiveDb.MapObjects.AddObject(OksLayer);
        //            foreach (IIngeoShape s in shapesToDel)
        //            {
        //                s.Delete();
        //            }
        //            // 
        //            int i = 0;
        //            IIngeoShape mainshape = null;
        //            List<IIngeoShape> holeshapes = new List<IIngeoShape>();
        //            foreach (tSpatialElement spelem in objectRealtySpatial.BuildingSpatial.EntitySpatial.SpatialElements)
        //            {
        //                IIngeoShape shape = mapObject.Shapes.Insert(-1, OksBuildStyle);
        //                IIngeoContourPart contour = shape.Contour.Insert(-1);
        //                foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
        //                {
        //                    Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
        //                }
        //                // объект может быть линейным
        //                if (spelem.SpelementUnits[0].Ordinate.X == spelem.SpelementUnits[spelem.SpelementUnits.Count - 1].Ordinate.X &&
        //                    spelem.SpelementUnits[0].Ordinate.Y == spelem.SpelementUnits[spelem.SpelementUnits.Count - 1].Ordinate.Y)
        //                    contour.Closed = true;
        //                else
        //                    contour.Closed = false;
        //                if (contour.Closed)
        //                {
        //                    if (i == 0)
        //                        mainshape = shape;
        //                    else
        //                    {
        //                        if (mainshape.Contour.Square > shape.Contour.Square)
        //                            holeshapes.Add(shape);
        //                        else
        //                        {
        //                            holeshapes.Add(mainshape);
        //                            mainshape = shape;
        //                        }
        //                    }
        //                }
        //                mapObject.MapObjects.UpdateChanges();
        //                i++;
        //            }
        //            shapesToDel = new List<IIngeoShape>();
        //            foreach (IIngeoShape shape in holeshapes)
        //            {

        //                if (shape != mainshape)
        //                {
        //                    mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
        //                    shapesToDel.Add(shape);
        //                }
        //            }

        //            foreach (IIngeoShape s in shapesToDel)
        //            {
        //                s.Delete();
        //            }
        //            mapObject.MapObjects.UpdateChanges();
        //            #endregion

        //            #region импорт семантики
        //            //try
        //            //{
        //            mapObject.SemData.SetValue(OksTableCadNo, OksFieldCadNo, building.CadastralNumber, 0);
        //            //}
        //            //catch { }
        //            #region Адрес
        //            try
        //            {
        //                if (building.Address != null)
        //                {
        //                    if (building.Address.Note != null)
        //                        mapObject.SemData.SetValue(OksTableAddress, OksFieldAddress, building.Address.Note, 0);
        //                    else
        //                    {
        //                        string addr = "";
        //                        try
        //                        {
        //                            string item = "";
        //                            if (building.Address.Region != null)
        //                            {
        //                                item = itemEnumCheck(building.Address.Region.ToString());
        //                                foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.RegionClassifiers)
        //                                {
        //                                    if (cl.Code == item)
        //                                    {
        //                                        addr = cl.Value + ", ";
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (building.Address.District != null)
        //                            {
        //                                addr += String.Format("{0} {1}, ", building.Address.District.Type, building.Address.District.Name);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (building.Address.Locality != null)
        //                            {
        //                                addr += String.Format("{0} {1}, ", building.Address.Locality.Type, building.Address.Locality.Name);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (building.Address.Street != null)
        //                            {
        //                                addr += String.Format("{0} {1}, ", building.Address.Street.Type, building.Address.Street.Name);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (building.Address.Level1 != null)
        //                            {
        //                                addr += String.Format("{0} {1} ", building.Address.Level1.Type, building.Address.Level1.Value);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (building.Address.Level2 != null)
        //                            {
        //                                addr += String.Format("{0} {1} ", building.Address.Level2.Type, building.Address.Level2.Value);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (building.Address.Level3 != null)
        //                            {
        //                                addr += String.Format("{0} {1} ", building.Address.Level3.Type, building.Address.Level3.Value);
        //                            }
        //                        }
        //                        catch { }
        //                        mapObject.SemData.SetValue(OksTableAddress, OksFieldAddress, addr.Trim(), 0);
        //                    }
        //                }
        //            }
        //            catch { }
        //            #endregion
        //            try
        //            {
        //                mapObject.SemData.SetValue(OksTableArea, OksFieldArea, building.Area + " кв.м", 0);
        //            }
        //            catch { }
        //            #region Тип
        //            try
        //            {
        //                string item = "";
        //                if (building.ObjectType != null)
        //                {
        //                    item = itemEnumCheck(building.ObjectType.ToString());
        //                    foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.BuildTypeClassifiers)
        //                    {
        //                        if (cl.Code == item)
        //                        {
        //                            item = cl.Value;
        //                            break;
        //                        }
        //                    }
        //                }
        //                mapObject.SemData.SetValue(OksTableType, OksFieldType, item, 0);
        //            }
        //            catch { }
        //            #endregion

        //            #region Назначение
        //            try
        //            {
        //                string item = "";
        //                if (building.AssignationBuilding != null)
        //                {
        //                    item = itemEnumCheck(building.AssignationBuilding.ToString());
        //                    foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.BuildSignClassifiers)
        //                    {
        //                        if (cl.Code == item)
        //                        {
        //                            item = cl.Value;
        //                            break;
        //                        }
        //                    }
        //                }
        //                mapObject.SemData.SetValue(OksTableAssign, OksFieldAssign, item, 0);
        //            }
        //            catch { }
        //            #endregion

        //            mapObject.MapObjects.UpdateChanges();
        //            #endregion
        //        }
        //    }
        //    #endregion
        //    #region else if (objectRealtySpatial.ConstructionSpatial != null)
        //    else
        //        if (objectRealtySpatial.ConstructionSpatial != null)
        //        {
        //            if (objectRealtySpatial.ConstructionSpatial.EntitySpatial != null)
        //            {
        //                tConstruction oks = (tConstruction)objectRealty.Item;
        //                #region создание пространственного объекта
        //                // ищем пространственный объект
        //                mapObject = IngeoHelper.GetOneMapObject(OksLayer, OksTableCadNo, OksFieldCadNo, oks.CadastralNumber);
        //                if (mapObject != null)
        //                {
        //                    foreach (IIngeoShape sh in mapObject.Shapes)
        //                        shapesToDel.Add(sh);
        //                    isModify = true;
        //                }
        //                else
        //                    mapObject = ingeo.ActiveDb.MapObjects.AddObject(OksLayer);
        //                foreach (IIngeoShape s in shapesToDel)
        //                {
        //                    s.Delete();
        //                }
        //                // 
        //                int i = 0;
        //                IIngeoShape mainshape = null;
        //                List<IIngeoShape> holeshapes = new List<IIngeoShape>();
        //                foreach (tSpatialElement spelem in objectRealtySpatial.ConstructionSpatial.EntitySpatial.SpatialElements)
        //                {
        //                    IIngeoShape shape = mapObject.Shapes.Insert(-1, OksConstrStyle);
        //                    IIngeoContourPart contour = shape.Contour.Insert(-1);
        //                    foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
        //                    {
        //                        Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
        //                    }
        //                    // объект может быть линейным
        //                    if (spelem.SpelementUnits[0].Ordinate.X == spelem.SpelementUnits[spelem.SpelementUnits.Count-1].Ordinate.X &&
        //                        spelem.SpelementUnits[0].Ordinate.Y == spelem.SpelementUnits[spelem.SpelementUnits.Count - 1].Ordinate.Y)
        //                        contour.Closed = true;
        //                    else
        //                        contour.Closed = false;
        //                    if (contour.Closed)
        //                    {
        //                        if (i == 0)
        //                            mainshape = shape;
        //                        else
        //                        {
        //                            if (mainshape.Contour.Square > shape.Contour.Square)
        //                                holeshapes.Add(shape);
        //                            else
        //                            {
        //                                holeshapes.Add(mainshape);
        //                                mainshape = shape;
        //                            }
        //                        }
        //                    }
        //                    mapObject.MapObjects.UpdateChanges();
        //                    i++;
        //                }
        //                shapesToDel = new List<IIngeoShape>();
        //                foreach (IIngeoShape shape in holeshapes)
        //                {

        //                    if (shape != mainshape)
        //                    {
        //                        mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
        //                        shapesToDel.Add(shape);
        //                    }
        //                }

        //                foreach (IIngeoShape s in shapesToDel)
        //                {
        //                    s.Delete();
        //                }
        //                mapObject.MapObjects.UpdateChanges();
        //                #endregion

        //                #region импорт семантики
        //                try
        //                {
        //                    mapObject.SemData.SetValue(OksTableCadNo, OksFieldCadNo, oks.CadastralNumber, 0);
        //                }
        //                catch { }
        //                #region Адрес
        //                try
        //                {
        //                    if (oks.Address != null)
        //                    {
        //                        if (oks.Address.Note != null)
        //                            mapObject.SemData.SetValue(OksTableAddress, OksFieldAddress, oks.Address.Note, 0);
        //                        else
        //                        {
        //                            string addr = "";
        //                            try
        //                            {
        //                                string item = "";
        //                                if (oks.Address.Region != null)
        //                                {
        //                                    item = itemEnumCheck(oks.Address.Region.ToString());
        //                                    foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.RegionClassifiers)
        //                                    {
        //                                        if (cl.Code == item)
        //                                        {
        //                                            addr = cl.Value + ", ";
        //                                            break;
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            catch { }
        //                            try
        //                            {
        //                                if (oks.Address.District != null)
        //                                {
        //                                    addr += String.Format("{0} {1}, ", oks.Address.District.Type, oks.Address.District.Name);
        //                                }
        //                            }
        //                            catch { }
        //                            try
        //                            {
        //                                if (oks.Address.Locality != null)
        //                                {
        //                                    addr += String.Format("{0} {1}, ", oks.Address.Locality.Type, oks.Address.Locality.Name);
        //                                }
        //                            }
        //                            catch { }
        //                            try
        //                            {
        //                                if (oks.Address.Street != null)
        //                                {
        //                                    addr += String.Format("{0} {1}, ", oks.Address.Street.Type, oks.Address.Street.Name);
        //                                }
        //                            }
        //                            catch { }
        //                            try
        //                            {
        //                                if (oks.Address.Level1 != null)
        //                                {
        //                                    addr += String.Format("{0} {1} ", oks.Address.Level1.Type, oks.Address.Level1.Value);
        //                                }
        //                            }
        //                            catch { }
        //                            try
        //                            {
        //                                if (oks.Address.Level2 != null)
        //                                {
        //                                    addr += String.Format("{0} {1} ", oks.Address.Level2.Type, oks.Address.Level2.Value);
        //                                }
        //                            }
        //                            catch { }
        //                            try
        //                            {
        //                                if (oks.Address.Level3 != null)
        //                                {
        //                                    addr += String.Format("{0} {1} ", oks.Address.Level3.Type, oks.Address.Level3.Value);
        //                                }
        //                            }
        //                            catch { }
        //                            mapObject.SemData.SetValue(OksTableAddress, OksFieldAddress, addr.Trim(), 0);
        //                        }
        //                    }
        //                }
        //                catch { }
        //                #endregion
        //                try
        //                {
        //                    mapObject.SemData.SetValue(OksTableArea, OksFieldArea, mapObject.Square.ToString() + " кв.м", 0);
        //                }
        //                catch { }
        //                #region Тип
        //                try
        //                {
        //                    string item = "";
        //                    if (oks.ObjectType != null)
        //                    {
        //                        item = itemEnumCheck(oks.ObjectType.ToString());
        //                        foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.BuildTypeClassifiers)
        //                        {
        //                            if (cl.Code == item)
        //                            {
        //                                item = cl.Value;
        //                                break;
        //                            }
        //                        }
        //                    }
        //                    mapObject.SemData.SetValue(OksTableType, OksFieldType, item, 0);
        //                }
        //                catch { }
        //                #endregion

        //                #region Назначение
        //                try
        //                {
        //                    mapObject.SemData.SetValue(OksTableAssign, OksFieldAssign, oks.AssignationName, 0);
        //                }
        //                catch { }
        //                #endregion

        //                #endregion
        //                mapObject.MapObjects.UpdateChanges();
        //            }
        //        }
        //    #endregion
        //        #region else if (objectRealtySpatial.UncompletedSpatial != null)
        //        else
        //            if (objectRealtySpatial.UncompletedSpatial != null)
        //            {
        //                if (objectRealtySpatial.UncompletedSpatial.EntitySpatial != null)
        //                {
        //                    tUncompleted oks = (tUncompleted)objectRealty.Item;
        //                    #region создание пространственного объекта
        //                    // ищем пространственный объект
        //                    mapObject = IngeoHelper.GetOneMapObject(OksLayer, OksTableCadNo, OksFieldCadNo, oks.CadastralNumber);
        //                    if (mapObject != null)
        //                    {
        //                        foreach (IIngeoShape sh in mapObject.Shapes)
        //                            shapesToDel.Add(sh);
        //                        isModify = true;
        //                    }
        //                    else
        //                        mapObject = ingeo.ActiveDb.MapObjects.AddObject(OksLayer);
        //                    foreach (IIngeoShape s in shapesToDel)
        //                    {
        //                        s.Delete();
        //                    }
        //                    // 
        //                    int i = 0;
        //                    IIngeoShape mainshape = null;
        //                    List<IIngeoShape> holeshapes = new List<IIngeoShape>();
        //                    foreach (tSpatialElement spelem in objectRealtySpatial.UncompletedSpatial.EntitySpatial.SpatialElements)
        //                    {
        //                        IIngeoShape shape = mapObject.Shapes.Insert(-1, OksUncompletedStyle);
        //                        IIngeoContourPart contour = shape.Contour.Insert(-1);
        //                        foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
        //                        {
        //                            Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
        //                        }
        //                        // объект может быть линейным
        //                        if (spelem.SpelementUnits[0].Ordinate.X == spelem.SpelementUnits[spelem.SpelementUnits.Count - 1].Ordinate.X &&
        //                            spelem.SpelementUnits[0].Ordinate.Y == spelem.SpelementUnits[spelem.SpelementUnits.Count - 1].Ordinate.Y)
        //                            contour.Closed = true;
        //                        else
        //                            contour.Closed = false;
        //                        if (contour.Closed)
        //                        {
        //                            if (i == 0)
        //                                mainshape = shape;
        //                            else
        //                            {
        //                                if (mainshape.Contour.Square > shape.Contour.Square)
        //                                    holeshapes.Add(shape);
        //                                else
        //                                {
        //                                    holeshapes.Add(mainshape);
        //                                    mainshape = shape;
        //                                }
        //                            }
        //                        }
        //                        mapObject.MapObjects.UpdateChanges();
        //                        i++;
        //                    }
        //                    shapesToDel = new List<IIngeoShape>();
        //                    foreach (IIngeoShape shape in holeshapes)
        //                    {

        //                        if (shape != mainshape)
        //                        {
        //                            mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
        //                            shapesToDel.Add(shape);
        //                        }
        //                    }

        //                    foreach (IIngeoShape s in shapesToDel)
        //                    {
        //                        s.Delete();
        //                    }
        //                    mapObject.MapObjects.UpdateChanges();
        //                    #endregion

        //                    #region импорт семантики
        //                    try
        //                    {
        //                        mapObject.SemData.SetValue(OksTableCadNo, OksFieldCadNo, oks.CadastralNumber, 0);
        //                    }
        //                    catch { }
        //                    #region Адрес
        //            try
        //            {
        //                if (oks.Address != null)
        //                {
        //                    if (oks.Address.Note != null)
        //                        mapObject.SemData.SetValue(OksTableAddress, OksFieldAddress, oks.Address.Note, 0);
        //                    else
        //                    {
        //                        string addr = "";
        //                        try
        //                        {
        //                            string item = "";
        //                            if (oks.Address.Region != null)
        //                            {
        //                                item = itemEnumCheck(oks.Address.Region.ToString());
        //                                foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.RegionClassifiers)
        //                                {
        //                                    if (cl.Code == item)
        //                                    {
        //                                        addr = cl.Value + ", ";
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (oks.Address.District != null)
        //                            {
        //                                addr += String.Format("{0} {1}, ", oks.Address.District.Type, oks.Address.District.Name);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (oks.Address.Locality != null)
        //                            {
        //                                addr += String.Format("{0} {1}, ", oks.Address.Locality.Type, oks.Address.Locality.Name);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (oks.Address.Street != null)
        //                            {
        //                                addr += String.Format("{0} {1}, ", oks.Address.Street.Type, oks.Address.Street.Name);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (oks.Address.Level1 != null)
        //                            {
        //                                addr += String.Format("{0} {1} ", oks.Address.Level1.Type, oks.Address.Level1.Value);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (oks.Address.Level2 != null)
        //                            {
        //                                addr += String.Format("{0} {1} ", oks.Address.Level2.Type, oks.Address.Level2.Value);
        //                            }
        //                        }
        //                        catch { }
        //                        try
        //                        {
        //                            if (oks.Address.Level3 != null)
        //                            {
        //                                addr += String.Format("{0} {1} ", oks.Address.Level3.Type, oks.Address.Level3.Value);
        //                            }
        //                        }
        //                        catch { }
        //                        mapObject.SemData.SetValue(OksTableAddress, OksFieldAddress, addr.Trim(), 0);
        //                    }
        //                }
        //            }
        //            catch { }
        //            #endregion
        //                    try
        //                    {
        //                        mapObject.SemData.SetValue(OksTableArea, OksFieldArea, mapObject.Square.ToString() + " кв.м", 0);
        //                    }
        //                    catch { }
        //                    #region Тип
        //                    try
        //                    {
        //                        string item = "";
        //                        if (oks.ObjectType != null)
        //                        {
        //                            item = itemEnumCheck(oks.ObjectType.ToString());
        //                            foreach (ClassifiersHelper.ClassifiersItem cl in ClassifiersHelper.BuildTypeClassifiers)
        //                            {
        //                                if (cl.Code == item)
        //                                {
        //                                    item = cl.Value;
        //                                    break;
        //                                }
        //                            }
        //                        }
        //                        mapObject.SemData.SetValue(OksTableType, OksFieldType, item, 0);
        //                    }
        //                    catch { }
        //                    #endregion

        //                    #region Назначение
        //                    try
        //                    {
        //                        mapObject.SemData.SetValue(OksTableAssign, OksFieldAssign, oks.AssignationName, 0);
        //                    }
        //                    catch { }
        //                    #endregion

        //                    #endregion
        //                    mapObject.MapObjects.UpdateChanges();
        //                }
        //            }
        //        #endregion

        //    if (mapObject != null)
        //    {
        //        // создаем подпись
        //        try
        //        {
        //            IngeoHelper.CreateMapObjectCaption(mapObject, OksCaptionStyle);
        //        }
        //        catch { }
        //    }
        //    return mapObject;
        //}

        private IIngeoMapObject ImportGeometryZone(tZoneSpatial zoneSpatial, tCadastral_BlockZone zone)
        {
            IIngeoMapObject mapObject = null;
            List<IIngeoShape> shapesToDel = new List<IIngeoShape>();
            
            //mapObject.MapObjects.UpdateChanges();

            #region if (parcelSpatial.EntitySpatial != null)
            if (zoneSpatial.EntitySpatial != null)
            {
                // ищем пространственный объект
                mapObject = IngeoHelper.GetOneMapObject(ZoneLayer, ZoneTableAccountNo, ZoneFieldAccountNo, zone.AccountNumber);
                if (mapObject != null)
                {
                    foreach (IIngeoShape sh in mapObject.Shapes)
                        shapesToDel.Add(sh);
                    isModify = true;
                }
                else
                    mapObject = ingeo.ActiveDb.MapObjects.AddObject(ZoneLayer);
                foreach (IIngeoShape s in shapesToDel)
                {
                    s.Delete();
                }
                // Считаем, что первый shape - основной контур, остальные вырезаются из него
                int i = 0;
                IIngeoShape mainshape = null;
                string style = "";
                if (zone.Item.ToString() == "KPT08.tSpecialZones")
                {
                    style = ZoneSZZStyle;
                    //tSpecialZones building = (tSpecialZones)objectRealty.Item;
                }
                else if (zone.Item.ToString() == "KPT08.tTerritorialZone")
                {
                    style = ZoneTerrStyle;
                }
                List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                foreach (tSpatialElement spelem in zoneSpatial.EntitySpatial.SpatialElements)
                {
                    IIngeoShape shape = mapObject.Shapes.Insert(-1, style);
                    IIngeoContourPart contour = shape.Contour.Insert(-1);
                    foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
                    {
                        Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                    }
                    contour.Closed = true;
                    //if (i == 0)
                    //    mainshape = shape;
                    //else
                    //{
                    //    if (mainshape.Contour.Square > shape.Contour.Square)
                    //    {
                    //        if (mainshape.Contour.TestRelation(shape.Contour).ToString() == "5")
                    //        {

                    //            holeshapes.Add(shape);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (mainshape.Contour.TestRelation(shape.Contour).ToString() == "3")
                    //        {
                    //            holeshapes.Add(mainshape);
                    //            mainshape = shape;
                    //        }
                    //    }
                    //}
                    mapObject.MapObjects.UpdateChanges();
                    i++;
                }
                shapesToDel = new List<IIngeoShape>();
                foreach (IIngeoShape shape in holeshapes)
                {

                    if (shape != mainshape)
                    {
                        mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                        shapesToDel.Add(shape);
                    }
                }

                foreach (IIngeoShape s in shapesToDel)
                {
                    s.Delete();
                }
                mapObject.MapObjects.UpdateChanges();

            }
            #endregion

            #region импорт семантики
            if (mapObject != null)
            {
                try
                {
                    mapObject.SemData.SetValue(ZoneTableAccountNo, ZoneFieldAccountNo, zone.AccountNumber, 0);
                }
                catch { }
                //try
                //{
                //    mapObject.SemData.SetValue(ZoneTableName, ZoneFieldName, zone.Description, 0);
                //}
                //catch { }
                try
                {
                    if (zone.Item.ToString() == "KPT08.tSpecialZones")
                    {
                        tSpecialZones specialZone = (tSpecialZones)zone.Item;
                        mapObject.SemData.SetValue(ZoneTableRestrict, ZoneFieldRestrict, specialZone.ContentRestrictions, 0);

                    }
                }
                catch { }

                

                // создаем подпись
                try
                {
                    IngeoHelper.CreateMapObjectCaption(mapObject, ZoneCaptionStyle);
                }
                catch { }

                mapObject.MapObjects.UpdateChanges();
            }
            #endregion
            return mapObject;
        }
    }
}
