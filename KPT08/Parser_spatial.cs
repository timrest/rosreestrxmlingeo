﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace KPT08_spatial
{
    public class ParserSpatial
    {
        private XmlDocument doc = new XmlDocument();

        public ParserSpatial(string fileName)
        {
            doc.Load(fileName);
        }

        private void ParseElement(object obj, XmlElement element)
        {
            if (element == null)
                return;
            foreach (XmlAttribute attr in element.Attributes)
            {
                FieldInfo field = obj.GetType().GetField(attr.Name);
                if (field == null)
                    continue;
                switch (field.FieldType.Name)
                {
                    case "String":
                        field.SetValue(obj, attr.Value);
                        break;
                    case "Int32":
                        field.SetValue(obj, Int32.Parse(attr.Value));
                        break;
                    case "Double":
                        field.SetValue(obj, Double.Parse(attr.Value.Replace('.', ',')));
                        break;
                    case "Nullable`1":
                        if (field.FieldType.FullName.Contains("DateTime"))
                            field.SetValue(obj, DateTime.Parse(attr.Value));
                        break;
                }

            }
            foreach (XmlElement el in element.ChildNodes)
            {
                FieldInfo field = obj.GetType().GetField(el.Name);
                if (field == null)
                    continue;
                switch (field.FieldType.Name)
                {
                    case "String":
                        field.SetValue(obj, el.InnerText);
                        break;
                    case "Int32":
                        field.SetValue(obj, Int32.Parse(el.InnerText));
                        break;
                    case "Double":
                        field.SetValue(obj, Double.Parse(el.InnerText.Replace('.', ',')));
                        break;
                    case "Nullable`1":
                        if (field.FieldType.FullName.Contains("DateTime"))
                            field.SetValue(obj, DateTime.Parse(el.InnerText));
                        break;
                }
            }
        }

        public tKPT08_spatial ParseDocument()
        {
            tKPT08_spatial cv = new tKPT08_spatial();
            XmlElement root = doc["Region_Cadastr"]["Package"];
            ParseElement(cv, root);
            cv.CadastralBlocks = GetCadastralBlocks(root["Cadastral_Blocks"]);
            return cv;
        }


        #region CadastralBlocks
        private List<tCadastralBlockSpatial> GetCadastralBlocks(XmlElement xCadastralBlocks)
        {
            if (xCadastralBlocks == null)
                return null;
            List<tCadastralBlockSpatial> cadastralBlocks = new List<tCadastralBlockSpatial>();
            foreach (XmlElement xCadastralBlock in xCadastralBlocks.GetElementsByTagName("Cadastral_Block"))
            {
                tCadastralBlockSpatial cadastralBlock = new tCadastralBlockSpatial();
                ParseElement(cadastralBlock, xCadastralBlock);
                if (xCadastralBlock["Parcels"] != null)
                {
                    cadastralBlock.Parcels = GetParcels(xCadastralBlock["Parcels"]);
                }
                //if (xCadastralBlock["ObjectsRealty"] != null)
                //{
                //    cadastralBlock.ObjectsRealty = GetObjectsRealtySpatial(xCadastralBlock["ObjectsRealty"]);
                //}
                if (xCadastralBlock["Zones"] != null)
                {
                    cadastralBlock.Zones = GetZones(xCadastralBlock["Zones"]);
                }
                cadastralBlock.SpatialData = GetSpatialData(xCadastralBlock["SpatialData"]);
                cadastralBlocks.Add(cadastralBlock);
            }
            return cadastralBlocks;
        }


        #region Parcels
        private List<tParcelSpatial> GetParcels(XmlElement xParcels)
        {
            List<tParcelSpatial> parcels = new List<tParcelSpatial>();
            foreach (XmlElement xParcel in xParcels.GetElementsByTagName("Parcel"))
            {
                tParcelSpatial parcel = new tParcelSpatial();
                parcel.XElement = System.Xml.Linq.XElement.Load(xParcel.CreateNavigator().ReadSubtree());
                ParseElement(parcel, xParcel);
                parcel.Contours = GetContours(xParcel["Contours"]);
                parcel.EntitySpatial = GetEntitySpatial(xParcel["Entity_Spatial"]);
                parcels.Add(parcel);

            }
            return parcels;
        }

        
        

        private List<tContour> GetContours(XmlElement xContours)
        {
            if (xContours == null)
                return null;
            List<tContour> contours = new List<tContour>();
            foreach (XmlElement xContour in xContours.GetElementsByTagName("Contour"))
            {
                tContour contour = new tContour();
                ParseElement(contour, xContour);
                if (xContour["Entity_Spatial"] != null)
                    contour.EntitySpatial = GetEntitySpatial(xContour["Entity_Spatial"]);
                contours.Add(contour);
            }
            return contours;
        }
        #endregion



        #region Entity_Spatial
        private tEntitySpatial GetEntitySpatial(XmlElement xEntitySpatial)
        {
            if (xEntitySpatial == null)
                return null;
            tEntitySpatial entitySpatial = new tEntitySpatial();
            ParseElement(entitySpatial, xEntitySpatial);
            entitySpatial.SpatialElements = GetSpatialElements(xEntitySpatial.GetElementsByTagName("Spatial_Element"));
            return entitySpatial;
        }

        private List<tSpatialElement> GetSpatialElements(XmlNodeList xSpatialElements)
        {
            List<tSpatialElement> spatialElements = new List<tSpatialElement>();
            foreach (XmlElement xSpatialElement in xSpatialElements)
            {
                tSpatialElement spatialElement = new tSpatialElement();
                ParseElement(spatialElement, xSpatialElement);
                spatialElement.SpelementUnits = GetSpelementUnits(xSpatialElement.GetElementsByTagName("Spelement_Unit"));
                spatialElements.Add(spatialElement);
            }
            return spatialElements;
        }

        private List<tSpelementUnit> GetSpelementUnits(XmlNodeList xSpelementUnits)
        {
            List<tSpelementUnit> spelementUnits = new List<tSpelementUnit>();
            foreach (XmlElement xSpelementUnit in xSpelementUnits)
            {
                tSpelementUnit spelementUnit = new tSpelementUnit();
                ParseElement(spelementUnit, xSpelementUnit);
                spelementUnit.Ordinate = GetOrdinate(xSpelementUnit["Ordinate"]);
                spelementUnits.Add(spelementUnit);
            }
            return spelementUnits;
        }

        private tOrdinate GetOrdinate(XmlElement xOrdinate)
        {
            tOrdinate ordinate = new tOrdinate();
            ParseElement(ordinate, xOrdinate);
            return ordinate;
        }
        #endregion
       

        private tSpatialData GetSpatialData(XmlElement xSpatialData)
        {
            if (xSpatialData == null)
                return null;
            tSpatialData spatialData = new tSpatialData();
            ParseElement(spatialData, xSpatialData);
            spatialData.EntitySpatial = GetEntitySpatial(xSpatialData["Entity_Spatial"]);
            return spatialData;
        }

        //#region tObjectsRealtyObjectRealtySpatial
        //public List<tObjectsRealtyObjectRealtySpatial> GetObjectsRealtySpatial(XmlElement xObjectsRealty)
        //{
        //    List<tObjectsRealtyObjectRealtySpatial> objectsRealty = new List<tObjectsRealtyObjectRealtySpatial>();
        //    foreach (XmlElement xObjectRealty in xObjectsRealty.GetElementsByTagName("ObjectRealty"))
        //    {
        //        tObjectsRealtyObjectRealtySpatial objectRealty = new tObjectsRealtyObjectRealtySpatial();

        //        ParseElement(objectRealty, xObjectRealty);
        //        objectRealty.XElement = System.Xml.Linq.XElement.Load(xObjectRealty.CreateNavigator().ReadSubtree());
        //        //if (xObjectRealty.Attributes["CadastralNumber"] != null)
        //        //    objectRealty.CadastralNumber = GetCadastralNumber(xObjectRealty);
        //        objectRealty.BuildingSpatial = GetBuildingSpatial(xObjectRealty["Building"]);
        //        objectRealty.ConstructionSpatial = GetConstructionSpatial(xObjectRealty["Construction"]);
        //        objectRealty.UncompletedSpatial = GetUncompletedSpatial(xObjectRealty["Uncompleted"]);
        //        objectsRealty.Add(objectRealty);
        //    }
        //    return objectsRealty;
        //}
        //private tBuildingSpatial GetBuildingSpatial(XmlElement xBuilding)
        //{
        //    tBuildingSpatial building = new tBuildingSpatial();
        //    if (xBuilding == null)
        //        return null;
        //    ParseElement(building, xBuilding);
        //    //if (xBuilding.Attributes["CadastralNumber"] != null)
        //    //    building.CadastralNumber = GetCadastralNumber(xBuilding);
        //    if (xBuilding["EntitySpatial"] != null)
        //        building.EntitySpatial = GetEntitySpatial(xBuilding["EntitySpatial"]);
        //    return building;
        //}
        //private tConstructionSpatial GetConstructionSpatial(XmlElement xConstruction)
        //{
        //    tConstructionSpatial Construction = new tConstructionSpatial();
        //    if (xConstruction == null)
        //        return null;
        //    ParseElement(Construction, xConstruction);
        //    if (xConstruction["EntitySpatial"] != null)
        //        Construction.EntitySpatial = GetEntitySpatial(xConstruction["EntitySpatial"]);
        //    return Construction;
        //}
        //private tUncompletedSpatial GetUncompletedSpatial(XmlElement xUncompleted)
        //{
        //    tUncompletedSpatial Uncompleted = new tUncompletedSpatial();
        //    if (xUncompleted == null)
        //        return null;
        //    ParseElement(Uncompleted, xUncompleted);
        //    if (xUncompleted["EntitySpatial"] != null)
        //        Uncompleted.EntitySpatial = GetEntitySpatial(xUncompleted["EntitySpatial"]);
        //    return Uncompleted;
        //}
        //#endregion

        //#region Bounds
        //private List<tBound> GetBounds(XmlElement xBounds)
        //{
        //    if (xBounds == null)
        //        return null;
        //    List<tBound> bounds = new List<tBound>();
        //    foreach (XmlElement xBound in xBounds.GetElementsByTagName("Bound"))
        //    {
        //        tBound bound = new tBound();
        //        ParseElement(bound, xBound);
        //        bound.Boundaries = GetBoundaries(xBound["Boundaries"]);
        //        bound.Documents = GetPropsDocuments(xBound["Documents"]);
        //        if (xBound["MunicipalBoundary"] != null)
        //        {
        //            bound.Boundary = new tMunicipalBoundary();
        //            ParseElement(bound.Boundary, xBound["MunicipalBoundary"]);
        //        }
        //        else if (xBound["InhabitedLocalityBoundary"] != null)
        //        {
        //            bound.Boundary = new tInhabitedLocalityBoundary();
        //            ParseElement(bound.Boundary, xBound["InhabitedLocalityBoundary"]);
        //        }
        //        bounds.Add(bound);
        //    }
        //    return bounds;
        //}

        //private List<tBoundary> GetBoundaries(XmlElement xBoundaries)
        //{
        //    if (xBoundaries == null)
        //        return null;
        //    List<tBoundary> boundaries = new List<tBoundary>();
        //    foreach (XmlElement xBoundary in xBoundaries.GetElementsByTagName("Boundary"))
        //    {
        //        tBoundary boundary = new tBoundary();
        //        boundary.EntitySpatial = GetEntitySpatial(xBoundary["Entity_Spatial"]);
        //        boundaries.Add(boundary);
        //    }
        //    return boundaries;
        //}
        //#endregion

        #region Zones
        private List<tZoneSpatial> GetZones(XmlElement xZones)
        {
            if (xZones == null)
                return null;
            List<tZoneSpatial> zones = new List<tZoneSpatial>();
            foreach (XmlElement xZone in xZones.GetElementsByTagName("Zone"))
            {
                tZoneSpatial zone = new tZoneSpatial();
                ParseElement(zone, xZone);
                zone.EntitySpatial = GetEntitySpatial(xZone["Entity_Spatial"]);
                zones.Add(zone);
            }
            return zones;
        }

        //private tTerritorialZone GetTerritorialZone(XmlElement xTerZone)
        //{
        //    if (xTerZone == null)
        //        return null;
        //    tTerritorialZone terZone = new tTerritorialZone();
        //    terZone.PermittedUses = GetPermittedUses(xTerZone["PermittedUses"]);
        //    return terZone;
        //}

        //private List<tPermittedUse> GetPermittedUses(XmlElement xPermittedUses)
        //{
        //    if (xPermittedUses == null)
        //        return null;
        //    List<tPermittedUse> permittedUses = new List<tPermittedUse>();
        //    foreach (XmlElement xPermittedUse in xPermittedUses.GetElementsByTagName("PermittedUse"))
        //    {
        //        if (xPermittedUse["PermittedUse"] == null)
        //            continue;
        //        tPermittedUse permittedUse = new tPermittedUse();
        //        ParseElement(permittedUse, xPermittedUse);
        //        permittedUses.Add(permittedUse);
        //    }
        //    return permittedUses;
        //}
        #endregion


        #endregion


    }
}
