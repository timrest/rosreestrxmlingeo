﻿using Helpers;
using Ingeo;
using Integro.InMeta.Runtime;
using KPT09_spatial;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KPT09
{
    public class Importer
    {
        private KPT KPT;
        private IngeoApplication ingeo;
        private DataApplication inmeta;
        private DataSession session;
        public string TableName;
        public string LayerId;
        public string StyleId;
        public string CadBlockLayerId;
        public string CadBlockStyleId;
        public string CadBlockTableName;
        public string CadBlockFieldName;
        public string FileType { get; set; }
        bool isModify = false;

        public Importer(IngeoHelper Ingeo, InmetaHelper Inmeta)
        {
            ingeo = Ingeo.Application;
            inmeta = Inmeta.Application;
            session = Inmeta.Session;
        }

        public void Import(KPT kpt, tKPT09_spatial kpt_spatial)
        {
            KPT = kpt;
            tCadastralBlockSpatial cadBlock_spatial = null;
            tParcelSpatial parcelSpatial = null;
            DateTime start = DateTime.Now;
            ProgressBarLogger.Writer.WriteLine("Начало импорта: " + start.ToString());
            // Импортируем описание документа
            DataObject eDocument = ImportAdditionalRegistryData();
            foreach (tCadastralBlock block in kpt.CadastralBlocks)
            {
                isModify = false;
                // по кадастровому номеру находим нужный нам кадастровый квартал(координатное описание)
                foreach (tCadastralBlockSpatial cadB in kpt_spatial.CadastralBlocks)
                {
                    if (cadB.CadastralNumber == block.CadastralNumber)
                    {
                        cadBlock_spatial = cadB;
                        break;
                    }
                }
                // Импортируем данные текущего квартала
                DataObject regBlock = ImportBlockRegistryData(block);
                
                // Импортируем пространственные данные
                IIngeoMapObject mapObjectCadBlock = null;
                if (block.SpatialData != null)
                    if (block.SpatialData.EntitySpatial!= null)
                        mapObjectCadBlock = ImportGeometryBlock(cadBlock_spatial, block.CadastralNumber);
                //else if (parcel.Contours != null && parcel.Contours.Count > 0)
                //    mapObject = ImportGeometry(parcel.Contours, parcel.CadastralNumber);
                string temp = "";
                if (mapObjectCadBlock == null)
                    temp = " - координаты кадастрового квартала не найдены. Пространственный объект квартала не создан.";
                else
                {
                    if(isModify)
                        temp = " - пространственный объект кадастрового квартала обновлен.";
                    else
                        temp = " - создан пространственный объект кадастрового квартала.";
                    CreateLink(mapObjectCadBlock, regBlock, "Geocad/Kvartaly");
                }
                ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + block.CadastralNumber + temp);
                ImportBlockCoordSys(block);
                
                #region
                //импортируем участки
                if (block.Parcels != null)
                {
                    ProgressBarLogger.MaxValue = block.Parcels.Length;
                    foreach (tParcel parcel in block.Parcels)
                    {
                        isModify = false;
                        ProgressBarLogger.ProgressInc();
                        foreach (tParcelSpatial pSp in cadBlock_spatial.Parcels)
                        {
                            if (pSp.CadastralNumber == parcel.CadastralNumber)
                            {
                                parcelSpatial = pSp;
                                break;
                            }
                        }
                        // Импортируем пространственные данные

                        IIngeoMapObject mapObjectLot = null;
                        if (parcelSpatial != null)
                        {
                            //if (parcelSpatial.EntitySpatial != null)
                            //{
                            if (parcel.Location.Address.Note != null)
                                mapObjectLot = ImportGeometryLot(parcelSpatial, parcel.CadastralNumber,
                                    parcel.Location.Address.Note);
                            else
                                mapObjectLot = ImportGeometryLot(parcelSpatial, parcel.CadastralNumber, "");
                            //else if (parcel.Contours != null && parcel.Contours.Count > 0)
                            //    mapObject = ImportGeometry(parcel.Contours, parcel.CadastralNumber);

                            /*if (session["Land/Lot"].Query("", "CadNo=?", parcel.CadastralNumber).Count > 0)
                                continue;*/
                            if (mapObjectLot != null)
                            {
                                // Импортируем данные текущего участка
                                DataObject regParcel = ImportRegistryData(parcel, parcelSpatial);
                                // Связываем участок с документом
                                LinkLotAndDoc(regParcel, eDocument);

                                // Импортируем семантику
                                //ImportSemantics(parcel, mapObject);
                                // Связываем пространственные данные с реестровыми
                                CreateLink(mapObjectLot, regParcel, "Land/Lot");

                                try
                                {
                                    if (isModify)
                                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " обновлен");
                                    else
                                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " добавлен");
                                }
                                catch { }
                                if (Settings.IsZoom)
                                {
                                    if (Settings.IsAllObjectsZoom)
                                        Settings.ZoomRect = new Rect(mapObjectLot, Settings.ZoomRect);
                                    else
                                        Settings.ZoomRect = new Rect(mapObjectLot);
                                }
                            }
                            else
                                ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber +
                            " - информации о координатах земельного участка не найдено! Импорт данного участка не произведен!");
                        }
                        else
                            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber +
                            " - информации о координатах земельного участка не найдено! Импорт данного участка не произведен!");
                    }
                }
                #endregion
                DateTime finish = DateTime.Now;

                ProgressBarLogger.Writer.WriteLine("Окончание импорта: " + finish.ToString());
                ProgressBarLogger.Writer.WriteLine("Время импорта: " + (finish-start).ToString());
            }
        }

        /// <summary>
        /// Ищем в реестрах кадастровый квартал. Если находим - смотрим, есть ли пространственный, если есть - записываем 
        /// в семантику кадастровый номер, чтобы при импорте графики не создавать повторно контур
        /// Если в реестрах не находим квартал - создаем и связываем с пространственными данными
        /// </summary>
        /// <param name="block"></param>
        /// <param name="parcelSpatial"></param>
        /// <returns></returns>
        private DataObject ImportBlockRegistryData(tCadastralBlock block)
        {
            DataObject regObject = null;
            IIngeoMapObject mapObject = null;
            
            DataObjectList listL = session["Geocad/Kvartaly"].Query("", "CadNum=?", block.CadastralNumber);
            if (listL.Count == 0)
            {
                regObject = session["Geocad/Kvartaly"].AddNew();
                SetValue(regObject, "CadNum", block.CadastralNumber);
                SetValue(regObject, "ZipLastDate", DateTime.Now.Date);
                try
                {
                    SetValue(regObject, "ZipLastUser", session.CurrentUser.Caption);
                }
                catch { }
                //try
                //{
                    if (block.Area!= null)
                        SetValue(regObject, "Square", block.Area.Total);
                //}
                //catch { }
                
            }
            else
            {
                regObject = listL[0];
                SetValue(regObject, "ZipLastDate", DateTime.Now.Date);
                try
                {
                    SetValue(regObject, "ZipLastUser", session.CurrentUser.Caption);
                }
                catch { }
                //try
                //{
                if (block.Area != null)
                    SetValue(regObject, "Square", block.Area.Total);

                // ищем связанный пространственный объект квартала
                mapObject = IngeoHelper.GetOneMapObject("Link_KadastrovyeKvartaly", "RegisterNo", 
                    regObject.Id.ToString());
                if (mapObject!= null)
                {
                    mapObject.SemData.SetValue(CadBlockTableName, CadBlockFieldName, block.CadastralNumber, 0);
                    mapObject.MapObjects.UpdateChanges();
                }

            }
            
            DataObject coordDescr = null;
            return regObject;
        }
        private void ImportBlockCoordSys(tCadastralBlock block)
        {
            //if (block.CoordSystem != null && session["General/CoordSys"].
            //    Query("", "Code=?", block.CoordSystem.Cs_Id).Count <= 0)
            //{
            //    DataObject coordSys = session["General/CoordSys"].AddNew();
            //    coordSys.SetString("Code", block.CoordSystem.Cs_Id);
            //    coordSys.SetString("Name", block.CoordSystem.Name);
            //    session.Commit();
            //}
            #region Координатные системы
            if (block.CoordSystems != null)
                foreach (tCoordSystem cs in block.CoordSystems)
                {
                    if (session["General/CoordSys"].Query("", "Code=?", cs.CsId).Count > 0)
                        continue;
                    DataObject coordSys = session["General/CoordSys"].AddNew();
                    coordSys.SetString("Code", cs.CsId);
                    coordSys.SetString("Name", cs.Name);
                }
            #endregion
        }


        private DataObject ImportAdditionalRegistryData()
        {
            #region Описание документа
            DataObject eDoc = null;
            if (session["Rosreestr/eDocument"].Query("", "Number=?", KPT.CertificationDoc.Number).Count > 0)
                eDoc = session["Rosreestr/eDocument"].Query("", "Number=?", KPT.CertificationDoc.Number)[0];
            else
            {
                eDoc = session["Rosreestr/eDocument"].AddNew();
                SetValue(eDoc, "CodeType", 101);
                SetValue(eDoc, "Version", 09);
                SetValue(eDoc, "DateUpload", DateTime.Now);
                SetValue(eDoc, "Organization", KPT.CertificationDoc.Organization);
                if (KPT.CertificationDoc.Date != DateTime.MinValue)
                    SetValue(eDoc, "Date", KPT.CertificationDoc.Date);
                SetValue(eDoc, "Number", KPT.CertificationDoc.Number);
                SetValue(eDoc, "Appointment", KPT.CertificationDoc.Official.Appointment);
                SetValue(eDoc, "FamilyName", KPT.CertificationDoc.Official.FamilyName);
                SetValue(eDoc, "FirstName", KPT.CertificationDoc.Official.FirstName);
                SetValue(eDoc, "Patronymic", KPT.CertificationDoc.Official.Patronymic);
            }
            #endregion
            return eDoc;
        }

        private DataObject ImportRegistryData(tParcel parcel, tParcelSpatial parcelSpatial)
        {
            DataObject regObject = null;
            string item = "";
            bool isExist = false;
            DataObjectList listL = session["Land/Lot"].Query("", "CadNo=?", parcel.CadastralNumber);
            if (listL.Count == 0)
            {
                regObject = session["Land/Lot"].AddNew();
                SetValue(regObject, "ImportDate", DateTime.Now.Date);
            }
            else
            {
                regObject = listL[0];
                SetValue(regObject, "ImportChangeDate", DateTime.Now.Date);
            }
            DataObject coordDescr = null;

            if (parcelSpatial.EntitySpatial != null)
                if (parcelSpatial.EntitySpatial.SpatialElements != null)
                {
                    /// Удаление старых точек
                    DataObjectList refList = session["Land/CoordDescription"].Query(
                        "", "Lot=?", regObject.Id.ToString());
                    foreach (DataObject refObj in refList)
                    {
                        DataObjectList pointList = session["Land/Coord"].Query("",
                            "CoordDescription=?", refObj.Id.ToString());
                        foreach (DataObject pointObj in pointList)
                            pointObj.Delete();
                        refObj.Delete();
                    }
                    /// Добавление новых точек
                    coordDescr = session["Land/CoordDescription"].AddNew();
                    coordDescr.SetLink("Lot", regObject);
                    foreach (tSpatialElement spElem in parcelSpatial.EntitySpatial.SpatialElements)
                    {
                        foreach (tSpelementUnit spelementUnit in spElem.SpelementUnits)
                        {
                            DataObject coord = session["Land/Coord"].AddNew();
                            coord.SetDouble("CoordX", Convert.ToDouble(spelementUnit.Ordinate.X));
                            coord.SetDouble("CoordY", Convert.ToDouble(spelementUnit.Ordinate.Y));
                            try
                            {
                                coord.SetInteger("CoordsNo", Convert.ToInt16(spelementUnit.Su_Nmb));
                            }
                            catch { }
                            coord.SetLink("CoordDescription", coordDescr);
                        }
                    }
                }

            SetValue(regObject, "CadNo", parcel.CadastralNumber);
            if (parcel.DateCreated != DateTime.MinValue)
                SetValue(regObject, "CadDate", parcel.DateCreated);
            SetValue(regObject, "RegisterNo", parcel.CadastralNumber);

            if (parcel.State != null)
            {
                item = itemEnumCheck(parcel.State.ToString());
                regObject.SetLink("LotState", session["Land/LotState"].Query("", "Code=?", item)[0]);
                item = "";
            }
            if (parcel.Name != null)
            {
                item = itemEnumCheck(parcel.Name.ToString());
                regObject.SetLink("LotName", session["Land/LotName"].Query("", "Code=?", item)[0]);
                item = "";
            }

            #region Координатная система
            //if (parcel.EntitySpatial != null)
            //{
            //    if (session["General/CoordSys"].Query("", "Code=?", parcel.EntitySpatial.Ent_Sys).Count > 0)
            //    {
            //        regObject.SetLink("CoordSys", session["General/CoordSys"].
            //            Query("", "Code=?", parcel.EntitySpatial.Ent_Sys)[0]);
            //        coordDescr.SetLink("CoordSys", session["General/CoordSys"].
            //            Query("", "Code=?", parcel.EntitySpatial.Ent_Sys)[0]);
            //    }
            //    else if (parcel.CoordSystem != null)
            //    {
            //        DataObject coordSys = session["General/CoordSys"].AddNew();
            //        coordSys.SetString("Code", parcel.CoordSystem.Cs_Id);
            //        coordSys.SetString("Name", parcel.CoordSystem.Name);
            //        regObject.SetLink("CoordSys", coordSys);
            //        coordDescr.SetLink("CoordSys", coordSys);
            //    }
            //}
            #endregion

            #region Площадь участка
            if (parcel.Area != null)
            {
                DataObject lotSquare = session["Land/LotSquares"].AddNew();
                lotSquare.SetLink("AreaCode", session["General/AreaCode"].Query("", "Code=?", "002")[0]);
                if (parcel.Area.Unit != null)
                {
                    item = parcel.Area.Unit.ToString();
                    item = itemEnumCheck(item);
                    lotSquare.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", item)[0]);
                    item = "";
                }
                SetValue(lotSquare, "Area", parcel.Area.Area.ToString());
                SetValue(lotSquare, "Innccuracy", parcel.Area.Inaccuracy.ToString());
                regObject.DeleteAllAgregates(lotSquare);
                lotSquare.SetLink("Object", regObject);
            }
            #endregion

            #region Адрес участка
            if (parcel.Location != null)
            {
                if (parcel.Location.Address != null)
                {
                    DataObject address = session["General/LotPostAddr"].AddNew();
                    SetValue(address, "Codе_OKATO", parcel.Location.Address.OKATO);
                    SetValue(address, "Codе_KLADR", parcel.Location.Address.KLADR);
                    SetValue(address, "PostalCode", parcel.Location.Address.PostalCode);
                    if (parcel.Location.Address.Region != null)
                    {
                        item = parcel.Location.Address.Region.ToString();
                        item = itemEnumCheck(item);
                        address.SetLink("Region", session["General/Province"].Query("", "Code=?", item)[0]);
                        item = "";
                    }
                    try
                    {
                        SetValue(address, "District", parcel.Location.Address.District.Type + " " + parcel.Location.Address.District.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "City", parcel.Location.Address.City.Type + " " + parcel.Location.Address.City.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Urban_District", parcel.Location.Address.UrbanDistrict.Type + " " + parcel.Location.Address.UrbanDistrict.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Soviet_Village", parcel.Location.Address.SovietVillage.Type + " " + parcel.Location.Address.SovietVillage.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Locality", parcel.Location.Address.Locality.Type + " " + parcel.Location.Address.Locality.Name);
                    }
                    catch { }
                    try
                    {

                        SetValue(address, "Street", parcel.Location.Address.Street.Type + " " + parcel.Location.Address.Street.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "House", parcel.Location.Address.Level1.Type + " " + parcel.Location.Address.Level1.Value);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Corpus", parcel.Location.Address.Level2.Type + " " + parcel.Location.Address.Level2.Value);

                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Building", parcel.Location.Address.Level3.Type + " " + parcel.Location.Address.Level3.Value);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Flat", parcel.Location.Address.Apartment.Type + " " + parcel.Location.Address.Apartment.Value);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Other", parcel.Location.Address.Other);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Description", parcel.Location.Address.Note);
                        SetValue(regObject, "Adress", parcel.Location.Address.Note);
                    }
                    catch { }
                    regObject.DeleteAllAgregates(address);
                    address.SetLink("Lot", regObject);
                }
            }
            #endregion

            #region Уточненное местоположение
            if (parcel.Location.Elaboration != null)
            {
                DataObject elaboration = session["General/Location"].AddNew();
                SetValue(elaboration, "Orientir", parcel.Location.Elaboration.ReferenceMark);
                SetValue(elaboration, "Distance", parcel.Location.Elaboration.Distance);
                SetValue(elaboration, "Direction", parcel.Location.Elaboration.Direction);
                regObject.DeleteAllAgregates(elaboration);
                elaboration.SetLink("Object", regObject);
            }
            #endregion

            #region Категория земель
            if (parcel.Category != null)
            {
                DataObject category = session["Land/LotGroundCategory"].AddNew();
                if (parcel.Category != null)
                {
                    item = itemEnumCheck(parcel.Category.ToString());
                    category.SetLink("GroundCategory", session["Land/GroundCategory"].Query("", "Code=?", item)[0]);
                    item = "";
                }
                regObject.DeleteAllAgregates(category);
                category.SetLink("Object", regObject);
            }
            #endregion

            #region Использование участка
            if (parcel.Utilization != null)
            {
                DataObject util = session["Land/FactUsingWay"].AddNew();
                SetValue(util, "UsingWayDoc", parcel.Utilization.ByDoc);
                if (parcel.Utilization != null)
                {
                    item = itemEnumCheck(parcel.Utilization.Utilization.ToString());
                    util.SetLink("UsingWay", session["Land/UsingWay"].Query("", "Code=?", item)[0]);
                    item = "";
                }
                regObject.DeleteAllAgregates(util);
                util.SetLink("Object", regObject);
                SetValue(regObject, "UsingWayDoc", parcel.Utilization.ByDoc);
                if (parcel.Utilization != null)
                {
                    item = itemEnumCheck(parcel.Utilization.Utilization.ToString());
                    SetValue(regObject, "UsingWay",
                        session["Land/UsingWay"].Query("", "Code=?", item)[0]["Name"].ToString());
                    item = "";
                }
            }
            #endregion

            #region Контуры
            //if (parcel.Contours != null)
            //{
            //    foreach (tContour contour in parcel.Contours)
            //    {
            //        DataObject cont = session["Land/LandContour"].AddNew();
            //        SetValue(cont, "Number_PP", contour.Number_Record);
            //    }
            //}
            #endregion

            #region Стоимость земли
            if (parcel.CadastralCost != null)
            {
                DataObject payment = session["Land/GroundPayment"].AddNew();
                payment.SetLink("PaymentKind", session["General/PaymentKind"].Query("", "Code=?", "016010000000")[0]);
                try
                {
                    payment.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", parcel.CadastralCost.Unit)[0]);
                }
                catch { }
                SetValue(payment, "Value", parcel.CadastralCost.Value.ToString());
                regObject.DeleteAllAgregates(payment);
                payment.SetLink("Lot", regObject);
            }
            #endregion

            session.Commit();
            return regObject;
        }

        private string itemEnumCheck(string item)
        {
            string newItem = "";
            if (item.Contains("Item"))
            {
                newItem = item.Replace("Item", "");
            }
            else
                newItem = item;
            return newItem;
        }

        private void LinkLotAndDoc(DataObject lot, DataObject doc)
        {
            DataObject rosreestrLots = session["Rosreestr/Lots"].AddNew();
            rosreestrLots.SetLink("eDocument", doc);
            lot.DeleteAllAgregates(rosreestrLots);
            rosreestrLots.SetLink("Lot", lot);
            session.Commit();
        }

        private IIngeoMapObject ImportGeometryBlock(KPT09_spatial.tCadastralBlockSpatial cadBlock_spatial, string cadNo)
        {
            IIngeoMapObject mapObject = null;
            // ищем пространственный объект квартала
            mapObject = IngeoHelper.GetOneMapObject(CadBlockLayerId, CadBlockTableName, CadBlockFieldName, cadNo);
            if (mapObject!=null)
            {
                foreach (IIngeoShape sh in mapObject.Shapes)
                    sh.Delete();
                isModify = true;
            }
            else
                mapObject = ingeo.ActiveDb.MapObjects.AddObject(CadBlockLayerId);
            if (cadBlock_spatial.SpatialData != null)
                if (cadBlock_spatial.SpatialData.EntitySpatial != null)
                    if (cadBlock_spatial.SpatialData.EntitySpatial.SpatialElements != null)
                    {
                        foreach (tSpatialElement spatialElement in cadBlock_spatial.SpatialData.EntitySpatial.SpatialElements)
                        {
                            IIngeoShape shape = mapObject.Shapes.Insert(-1, CadBlockStyleId);
                            IIngeoContourPart contour = shape.Contour.Insert(-1);
                            foreach (tSpelementUnit spelementUnit in spatialElement.SpelementUnits)
                                Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);

                            contour.Closed = true;
                            mapObject.MapObjects.UpdateChanges();
                        }
                        if (CadBlockTableName != null && CadBlockFieldName != null)
                        {
                            mapObject.SemData.SetValue(CadBlockTableName, CadBlockFieldName, cadNo, 0);
                        }
                        mapObject.MapObjects.UpdateChanges();
                    }
            return mapObject;
        }

        /// <summary>
        /// Импортируем геометрию объекта
        /// </summary>
        /// <param name="parcelSpatial">описание координатной части участка</param>
        /// <param name="cadNo">кадастровый номер участка</param>
        /// <param name="address">адрес участка</param>
        /// <returns>Пространственный объект земельного участка</returns>
        private IIngeoMapObject ImportGeometryLot(tParcelSpatial parcelSpatial, string cadNo, string address)
        {
            IIngeoMapObject mapObject = null;
            List<IIngeoShape> shapesToDel = new List<IIngeoShape>();
            
            //mapObject.MapObjects.UpdateChanges();

            #region if (parcelSpatial.EntitySpatial != null)
            if (parcelSpatial.EntitySpatial != null)
            {
                string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
                if (!objId.Equals(string.Empty))
                {
                    mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                    foreach (IIngeoShape sh in mapObject.Shapes)
                        shapesToDel.Add(sh);
                    isModify = true;
                }
                else
                    mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
                foreach (IIngeoShape s in shapesToDel)
                {
                    s.Delete();
                }
                // Считаем, что первый shape - основной контур, остальные вырезаются из него
                int i = 0;
                IIngeoShape mainshape = null;
                foreach (tSpatialElement spelem in parcelSpatial.EntitySpatial.SpatialElements)
                {
                    IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                    IIngeoContourPart contour = shape.Contour.Insert(-1);
                    foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
                    {
                        Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                        contour.Closed = true;
                    }
                    if (i == 0)
                        mainshape = shape;
                    mapObject.MapObjects.UpdateChanges();
                    i++;
                }
                    shapesToDel = new List<IIngeoShape>();
                    foreach (IIngeoShape shape in mapObject.Shapes)
                    {
                        
                            if (shape != mainshape)
                            {
                                mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                                shapesToDel.Add(shape);
                            }
                    }
                
                foreach (IIngeoShape s in shapesToDel)
                {
                    s.Delete();
                }
                mapObject.MapObjects.UpdateChanges();

            }
            #endregion
            else
                if (parcelSpatial.Contours != null)
                {
                    string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
                    if (!objId.Equals(string.Empty))
                    {
                        mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                        foreach (IIngeoShape sh in mapObject.Shapes)
                            shapesToDel.Add(sh);
                        isModify = true;
                    }
                    else
                        mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
                    foreach (IIngeoShape s in shapesToDel)
                    {
                        s.Delete();
                    }
                    foreach (KPT09_spatial.tContour contour in parcelSpatial.Contours)
                    {
                        if (contour.EntitySpatial != null)
                        {
                            // Считаем, что первый shape - основной контур, остальные вырезаются из него
                            int i = 0;
                            IIngeoShape mainshape = null;
                            List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                            foreach (tSpatialElement spelem in contour.EntitySpatial.SpatialElements)
                            {
                                IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                                IIngeoContourPart cont = shape.Contour.Insert(-1);
                                foreach (tSpelementUnit spelementUnit in spelem.SpelementUnits)
                                {
                                    Transforms.Start(cont, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);

                                }
                                cont.Closed = true;
                                if (i == 0)
                                    mainshape = shape;
                                else
                                    holeshapes.Add(shape);
                                mapObject.MapObjects.UpdateChanges();
                                i++;
                            }
                            shapesToDel = new List<IIngeoShape>();
                            foreach (IIngeoShape shape in holeshapes)
                            {

                                if (shape != mainshape)
                                {
                                    mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                                    shapesToDel.Add(shape);
                                }
                            }
                            //foreach (IIngeoShape shape in mapObject.Shapes)
                            //{
                            //    foreach (IIngeoShape othershape in mapObject.Shapes)
                            //    {
                            //        if (shape.Index != othershape.Index)
                            //        {
                            //            //System.Windows.Forms.MessageBox.Show(shape.Contour.Square + " " +
                            //            //    shape.Contour.TestRelation(othershape.Contour).ToString());
                            //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "3")
                            //            {
                            //                othershape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                            //                shapesToDel.Add(shape);
                            //            }
                            //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "5")
                            //            {
                            //                shape.Contour.Combine(TIngeoContourCombineOperation.inccSub, othershape.Contour);
                            //                shapesToDel.Add(othershape);
                            //            }
                            //        }
                            //    }
                            //}
                            foreach (IIngeoShape s in shapesToDel)
                            {
                                s.Delete();
                            }
                        }
                    }
                }
            if (mapObject != null)
            {
                try
                {
                    mapObject.SemData.SetValue("LotInfo", "CadNo", cadNo, 0);
                }
                catch { }
                try
                {
                    mapObject.SemData.SetValue("LotInfo", "ADDR", address, 0);
                }
                catch { }
                mapObject.MapObjects.UpdateChanges();
            }
            return mapObject;
        }

        //private void ImportSemantics(tParcel parcel, IIngeoMapObject mapObject)
        //{
        //    import(parcel.XElement, mapObject);
        //    nextNode(parcel.XElement, mapObject);

        //    mapObject.MapObjects.UpdateChanges();
        //}


        void nextNode(XElement elem, IIngeoMapObject mapObject)
        {
            foreach (XElement el in elem.Elements())
            {
                import(el, mapObject);
                nextNode(el, mapObject);
            }
        }

        void import(XElement el, IIngeoMapObject mapObject)
        {
            foreach (XAttribute attr in el.Attributes())
            {
                XmlTemplateItem item = XmlTemplate.Template[XmlTemplate.GetPath(el), attr.Name.ToString()];
                if (item != null)
                    mapObject.SemData.SetValue(item.TableName, item.FieldName, attr.Value, 0);
            }
            if (!el.HasElements)
                if (el.Value.Length > 0)
                {
                    XmlTemplateItem item = XmlTemplate.Template[XmlTemplate.GetPath(el), "Значение узла"];
                    if (item != null)
                        mapObject.SemData.SetValue(item.TableName, item.FieldName, el.Value, 0);
                }
        }

        private void CreateLink(IIngeoMapObject mapObject, DataObject regObject, string classname)
        {
            mapObject.SemData.SetValue("InMetaLink", "RegisterNo", regObject.Id.ToString(), 0);
            mapObject.SemData.SetValue("InMetaLink", "Classifier", classname, 0);
            mapObject.MapObjects.UpdateChanges();
        }

        private void SetValue(DataObject obj, string property, object value)
        {
            if (value == null)
                return;
            switch (value.GetType().Name)
            {
                case "Int32": obj.SetInteger(property, (int)value); break;
                case "String": obj.SetString(property, (string)value); break;
                case "Double": obj.SetDouble(property, (double)value); break;
                case "Float": obj.SetDouble(property, (double)value); break;
                case "Boolean": obj.SetBoolean(property, (bool)value); break;
                case "DateTime": obj.SetDateTime(property, (DateTime)value); break;
            }
        }
    }
}
