﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KPT09_spatial
{
    public class tKPT09_spatial
    {
        public List<tCadastralBlockSpatial> CadastralBlocks;
    }

    public class tCadastralBlockSpatial
    {
        public string CadastralNumber;
        public List<tParcelSpatial> Parcels;
        public tSpatialData SpatialData;
        //public List<tBound> Bounds;
    }


    #region public class tParcel
    public class tParcelSpatial
    {
        public XElement XElement;
        public string CadastralNumber;
       
        public List<tSubParcel> SubParcels;
       
        public List<tContour> Contours;
        public tEntitySpatial EntitySpatial;
    }

    public class tSubParcel
    {
        public string Number_Record;
        public string State;

    }
    public class tContour
    {
        public string Number_Record;
        public tEntitySpatial EntitySpatial;
    }

    #region public class tEntitySpatial
    public class tEntitySpatial
    {
        public string Ent_Sys;
        public List<tSpatialElement> SpatialElements;
    }

    public class tSpatialElement
    {
        public List<tSpelementUnit> SpelementUnits;
    }

    public class tSpelementUnit
    {
        public string Type_Unit;
        public int Su_Nmb;
        public tOrdinate Ordinate;
    }

    public class tOrdinate
    {
        public double X;
        public double Y;
        public int Ord_Nmb;
        public int Num_Geopoint;
        public string Geopoint_Zacrep;
        public double Delta_Geopoint;
        public string Point_Pref;
    }
    #endregion
    #endregion


    public class tSpatialData
    {
        public tEntitySpatial EntitySpatial;
    }



    #region public class tBound
    public class tBound
    {
        public string AccountNumber;
        public List<tBoundary> Boundaries;
        public object Boundary;
    }

    public class tBoundary
    {
        public tEntitySpatial EntitySpatial;
    }

    public class tMunicipalBoundary
    {
        public string Name;
    }

    public class tInhabitedLocalityBoundary
    {
        public string Name;
    }
    #endregion

    #region public class tZone
    public class tZone
    {
        public string AccountNumber;
        public tEntitySpatial EntitySpatial;
        public object Zone;
    }
    #endregion


}