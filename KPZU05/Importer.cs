﻿using Helpers;
using Ingeo;
using InMeta;
using InMeta.Land;
using InMeta.Rosreestr;
using Integro.InMeta.Runtime;
using KPZU05_spatial;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KPZU05
{
    public class Importer
    {
        private KPZU CV;
        private IngeoApplication ingeo;
        private DataApplication inmeta;
        private DataSession session;
        //private InMeta.Session inmetaSession;
        public string TableName;
        public string LayerId;
        public string StyleId;
        public string CaptionStyleId;
        public string FileType { get; set; }
        bool isModify = false;

        public Importer(IngeoHelper Ingeo, InmetaHelper Inmeta)
        {
            ingeo = Ingeo.Application;
            inmeta = Inmeta.Application;
            session = Inmeta.Session;
            //inmetaSession = (Session)session;
        }

        public void Import(KPZU cv, tParcelSpatial parcelSpatial)
        {
            CV = cv;

            ProgressBarLogger.MaxValue = 1;

            ProgressBarLogger.ProgressInc();
            tParcel parcel = cv.Parcel;

            // Импортируем пространственные данные
            IIngeoMapObject mapObject = null;
            if (parcel.Item != null)
            {
                if (parcel.Location.Address.Note != null)
                    mapObject = ImportGeometry(parcelSpatial, parcel.CadastralNumber, parcel.Location.Address.Note);
                else
                    mapObject = ImportGeometry(parcelSpatial, parcel.CadastralNumber, "");

                // создаем подпись к ЗУ
                try
                {
                    IngeoHelper.CreateMapObjectCaption(mapObject, CaptionStyleId);
                }
                catch { }
                // Импортируем описание документа
                DataObject eDocument = ImportAdditionalRegistryData();
                // Импортируем данные текущего участка
                DataObject regParcel = ImportRegistryData(parcel, parcelSpatial);
                // Связываем участок с документом
                LinkLotAndDoc(regParcel, eDocument);

                if (mapObject == null)
                {
                    try
                    {
                        if (isModify)
                            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " обновлен (точки не найдены)");
                        else
                            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " добавлен (точки не найдены)");
                    }
                    catch { }
                    //continue;
                }

                // Импортируем семантику
                //---------------------------------------
                //ImportSemantics(parcel, mapObject);
                // Связываем пространственные данные с реестровыми
                CreateLink(mapObject, regParcel);
                try
                {
                    if (isModify)
                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " обновлен");
                    else
                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " добавлен");
                }
                catch { }
                if (Settings.IsZoom)
                {
                    if (Settings.IsAllObjectsZoom)
                        Settings.ZoomRect = new Rect(mapObject, Settings.ZoomRect);
                    else
                        Settings.ZoomRect = new Rect(mapObject);
                }
            }
            else
            {
                ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber +
                    " - информации о координатах земельного участка не найдено! Импорт данного участка не произведен!");

            }
        }

        /// <summary>
        /// Импортируем данные о документе (xml)
        /// </summary>
        /// <returns></returns>
        private DataObject ImportAdditionalRegistryData()
        {
            #region Описание документа
            DataObject eDoc = null;
            if (session["Rosreestr/eDocument"].Query("", "Number=?", CV.CertificationDoc.Number).Count > 0)
                eDoc = session["Rosreestr/eDocument"].Query("", "Number=?", CV.CertificationDoc.Number)[0];
            else
            {
                eDoc = session["Rosreestr/eDocument"].AddNew();
                SetValue(eDoc, "CodeType", 114);
                SetValue(eDoc, "Version", 05);
                SetValue(eDoc, "DateUpload", DateTime.Now);
                SetValue(eDoc, "Organization", CV.CertificationDoc.Organization);
                if (CV.CertificationDoc.Date != DateTime.MinValue)
                    SetValue(eDoc, "Date", CV.CertificationDoc.Date);
                SetValue(eDoc, "Number", CV.CertificationDoc.Number);
                SetValue(eDoc, "Appointment", CV.CertificationDoc.Official.Appointment);
                SetValue(eDoc, "FamilyName", CV.CertificationDoc.Official.FamilyName);
                SetValue(eDoc, "FirstName", CV.CertificationDoc.Official.FirstName);
                SetValue(eDoc, "Patronymic", CV.CertificationDoc.Official.Patronymic);
            }
            #endregion

            #region Координатные системы
            if (CV.CoordSystems != null)
                foreach (tCoordSystem cs in CV.CoordSystems)
                {
                    if (session["General/CoordSys"].Query("", "Code=?", cs.CsId).Count > 0)
                        continue;
                    DataObject coordSys = session["General/CoordSys"].AddNew();
                    coordSys.SetString("Code", cs.CsId);
                    coordSys.SetString("Name", cs.Name);
                }
            #endregion

            #region Реестр исполнителей работ
            if (CV.Contractors != null)
                foreach (KPZUContractor contractor in CV.Contractors)
                {
                    DataObject cadEngineer;
                    DataObject contr;
                    bool isExist = false;
                    if (session["Rosreestr/Cadastral_Engineer"].Query("", "NCertificate=?", contractor.NCertificate).Count == 0)
                    {
                        cadEngineer = session["Rosreestr/Cadastral_Engineer"].AddNew();
                        SetValue(cadEngineer, "NCertificate", contractor.NCertificate);
                        SetValue(cadEngineer, "LastName", contractor.FamilyName);
                        SetValue(cadEngineer, "FirstName", contractor.FirstName);
                        SetValue(cadEngineer, "Patronymic", contractor.Patronymic);
                        SetValue(cadEngineer, "OrganizationName", contractor.Organization);
                    }
                    else
                    {
                        cadEngineer = session["Rosreestr/Cadastral_Engineer"].Query("", "NCertificate=?", contractor.NCertificate)[0];
                    }

                    foreach (DataObject c in eDoc.GetChilds("Rosreestr/Contractor"))
                        if (contractor.Date != DateTime.MinValue)
                            if(c["Date"] != null)
                                if(c["Date"].ToString() == contractor.Date.ToString())
                                {
                                    isExist = true;
                                    break;
                                }
                        //if (c.GetLink("WorkContractor") != null)
                        //    if (c.GetLink("WorkContractor").Id == cadEngineer.Id)
                        //    {
                        //        isExist = true;
                        //        break;
                        //    }
                    if (!isExist)
                    {
                        contr = session["Rosreestr/Contractor"].AddNew();
                        contr.SetLink("eDocument", eDoc);
                        if (contractor.Date != DateTime.MinValue)
                            SetValue(contr, "Date", contractor.Date);
                        contr.SetLink("WorkContractor", cadEngineer);
                    }
                }
            #endregion

            session.Commit();
            return eDoc;
        }
        
        private void LinkLotAndDoc(DataObject lot, DataObject doc)
        {
            bool isExist = false;
            foreach (DataObject lots in lot.GetChilds("Rosreestr/Lots"))
            {
                if (lots.GetLink("eDocument") != null)
                    if (lots.GetLink("eDocument") == doc)
                    {
                        isExist = true;
                        break;
                    }
            }
            if (!isExist)
            {
                DataObject rosreestrLots = session["Rosreestr/Lots"].AddNew();
                rosreestrLots.SetLink("eDocument", doc);
                foreach (DataObject c in doc.GetChilds("Rosreestr/Contractor"))
                {
                    c.SetLink("Lot", lot);
                }
                rosreestrLots.SetLink("Lot", lot);
            }
            session.Commit();
        }

        private DataObject ImportRegistryData(tParcel parcel, tParcelSpatial parcelSpatial)
        {
            DataObject regObject = null;
            string item = "";
            bool isExist = false;
            DataObjectList listL = session["Land/Lot"].Query("", "CadNo=?", parcel.CadastralNumber);
            if (listL.Count == 0)
            {
                regObject = session["Land/Lot"].AddNew();
                SetValue(regObject, "ImportDate", DateTime.Now.Date);
            }
            else
            {
                regObject = listL[0];
                SetValue(regObject, "ImportChangeDate", DateTime.Now.Date);
            }

            DataObject coordDescr = null;

            if (parcelSpatial.EntitySpatial != null)
                if (parcelSpatial.EntitySpatial.SpatialElements != null)
                {
                    /// Удаление старых точек
                    DataObjectList refList = session["Land/CoordDescription"].Query(
                        "", "Lot=?", regObject.Id.ToString());
                    foreach (DataObject refObj in refList)
                    {
                        DataObjectList pointList = session["Land/Coord"].Query("",
                            "CoordDescription=?", refObj.Id.ToString());
                        foreach (DataObject pointObj in pointList)
                            pointObj.Delete();
                        refObj.Delete();
                    }
                    /// Добавление новых точек
                    coordDescr = session["Land/CoordDescription"].AddNew();
                    coordDescr.SetLink("Lot", regObject);
                    foreach (tSpatialElement spElem in parcelSpatial.EntitySpatial.SpatialElements)
                    {
                        foreach (KPZU05_spatial.tSpelementUnitZUOut spelementUnit in spElem.SpelementUnits)
                        {
                            DataObject coord = session["Land/Coord"].AddNew();
                            coord.SetDouble("CoordX", Convert.ToDouble(spelementUnit.Ordinate.X));
                            coord.SetDouble("CoordY", Convert.ToDouble(spelementUnit.Ordinate.Y));
                            try
                            {
                                coord.SetInteger("CoordsNo", Convert.ToInt16(spelementUnit.Su_Nmb));
                            }
                            catch { }
                            coord.SetLink("CoordDescription", coordDescr);
                        }
                    }
                }
                else
                    if (parcel.Item.ToString() == "KPZU05.tParcelContours")
                    {
                        #region Контуры
                        tParcelContours countours = (tParcelContours)parcel.Item;
                        foreach (tContour countour in countours.Contour)
                        {
                            DataObject cont = session["Land/LandContour"].AddNew();
                            SetValue(cont, "Number_PP", countour.NumberRecord);
                            if (countour.Area != null)
                            {
                                DataObject lotSquare = session["Land/LotSquares"].AddNew();
                                lotSquare.SetLink("AreaCode", session["General/AreaCode"].Query("", "Code=?", "002")[0]);
                                if (countour.Area.Unit != null)
                                {
                                    item = itemEnumCheck(countour.Area.Unit.ToString());
                                    lotSquare.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", item)[0]);
                                    item = "";
                                }
                                SetValue(lotSquare, "Area", countour.Area.Area.ToString());
                                cont.DeleteAllAgregates(lotSquare);
                                lotSquare.SetLink("Object", cont);
                            }
                            regObject.DeleteAllAgregates(cont);
                            cont.SetLink("Lot", regObject);
                        }
                        #endregion
                    }
                    else
                        if (parcel.Item.ToString() == "KPZU05.tParcelCompositionEZ")
                        {
                            #region Объект входящий в состав единого землепользования

                            tParcelCompositionEZ compositionEZ = (tParcelCompositionEZ)parcel.Item;
                            foreach (tEntryParcel entryParcel in compositionEZ.EntryParcel)
                            {
                                DataObject entry = session["Land/Object_Entry"].AddNew();
                                SetValue(entry, "CadastralNumber", entryParcel.CadastralNumber);
                                //if (entryParcel.DateRemoved != DateTime.MinValue)
                                //    SetValue(entry, "DateRemoved", entryParcel.DateRemoved);
                                //if (entryParcel.State != null)
                                //{
                                //    item = itemEnumCheck(entryParcel.State.ToString());
                                //    entry.SetLink("LotState", session["Lot/LotState"].Query("", "Code=?", item)[0]);
                                //    item = "";
                                //}
                                regObject.DeleteAllAgregates(entry);
                                entry.SetLink("Object", regObject);
                            }
                            #endregion
                        }

            SetValue(regObject, "CadNo", parcel.CadastralNumber);
            if (parcel.DateExpiry != DateTime.MinValue)
                SetValue(regObject, "DateExpiry", parcel.DateExpiry);
            if (parcel.DateCreated != DateTime.MinValue)
                SetValue(regObject, "CadDate", parcel.DateCreated);
            if (parcel.DateRemoved != DateTime.MinValue)
                SetValue(regObject, "DateRemoved", parcel.DateRemoved);
            if (parcel.DateCreatedDoc != DateTime.MinValue)
                SetValue(regObject, "DateCreatedDoc", parcel.DateCreatedDoc);
            SetValue(regObject, "RegisterNo", parcel.CadastralNumber);
            SetValue(regObject, "CadastralBlock", parcel.CadastralBlock);
            
            string oldNumbString = "";
            if (parcel.OldNumbers != null)
                foreach (tOldNumber oldNumber in parcel.OldNumbers)
                {
                    if(oldNumbString == "")
                        oldNumbString = oldNumber.Type + " " + oldNumber.Number;
                    else
                        oldNumbString = oldNumbString + ", " + oldNumber.Type + " " + oldNumber.Number;
                }
            SetValue(regObject, "ConditionalNo", oldNumbString);

            string prevCadastralNumString = "";
            if (parcel.PrevCadastralNumbers!= null)
                foreach (string prevCadastralNum in parcel.PrevCadastralNumbers)
                {
                    if (prevCadastralNumString == "")
                        prevCadastralNumString = prevCadastralNum;
                    else
                        prevCadastralNumString = prevCadastralNumString + ", " + prevCadastralNum;
                }
            SetValue(regObject, "PrevCadastralNumbers", prevCadastralNumString);

            string InnerCadastralNumString = "";
            if (parcel.InnerCadastralNumbers != null)
                foreach (string InnerCadastralNum in parcel.InnerCadastralNumbers)
                {
                    if (InnerCadastralNumString == "")
                        InnerCadastralNumString = InnerCadastralNum;
                    else
                        InnerCadastralNumString = InnerCadastralNumString + ", " + InnerCadastralNum;
                }
            SetValue(regObject, "InnerCadastralNumbers", InnerCadastralNumString);

            SetValue(regObject, "SpecialNote", parcel.SpecialNote);
            
            if (parcel.State != null)
            {
                item = parcel.State.ToString();
                item = itemEnumCheck(item);
                regObject.SetLink("LotState", session["Land/LotState"].Query("", "Code=?", item)[0]);
                item = "";
            }
            if (parcel.Name != null)
            {
                item = parcel.Name.ToString();
                item = itemEnumCheck(item);
                regObject.SetLink("LotName", session["Land/LotName"].Query("", "Code=?", item)[0]);
                item = "";
            }


            #region Координатная система
            if (parcel.Item != null)
                if (parcel.Item.ToString() == "KPZU05.tEntitySpatialBordersZUOut")
                {
                    tEntitySpatialBordersZUOut spatial = (tEntitySpatialBordersZUOut)parcel.Item;
                    //----------------------------------
                    if (session["General/CoordSys"].Query("", "Code=?", spatial.EntSys).Count > 0)
                    {
                        regObject.SetLink("CoordSys", session["General/CoordSys"].
                            Query("", "Code=?", spatial.EntSys)[0]);
                        if (coordDescr != null)
                            coordDescr.SetLink("CoordSys", session["General/CoordSys"].
                                Query("", "Code=?", spatial.EntSys)[0]);
                    }
                }
            #endregion

            #region Площади участка
            if (parcel.Area != null)
            {
                DataObject lotSquare = session["Land/LotSquares"].AddNew();
                lotSquare.SetLink("AreaCode", session["General/AreaCode"].Query("", "Code=?", "002")[0]);
                if (parcel.Area.Unit != null)
                {
                    item = parcel.Area.Unit.ToString();
                    item = itemEnumCheck(item);
                    lotSquare.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", item)[0]);
                    item = "";
                }
                SetValue(lotSquare, "Area", parcel.Area.Area.ToString());
                SetValue(lotSquare, "Innccuracy", parcel.Area.Inaccuracy.ToString());
                regObject.DeleteAllAgregates(lotSquare);
                lotSquare.SetLink("Object", regObject);
            }
            #endregion

            #region Адрес участка
            if (parcel.Location != null)
            {
                if (parcel.Location.Address != null)
                {
                    DataObject address = session["General/LotPostAddr"].AddNew();
                    SetValue(address, "Codе_OKATO", parcel.Location.Address.OKATO);
                    SetValue(address, "Codе_KLADR", parcel.Location.Address.KLADR);
                    SetValue(address, "PostalCode", parcel.Location.Address.PostalCode);
                    if (parcel.Location.Address.Region != null)
                    {
                        item = parcel.Location.Address.Region.ToString();
                        item = itemEnumCheck(item);
                        address.SetLink("Region", session["General/Province"].Query("", "Code=?", item)[0]);
                        item = "";
                    }
                    try
                    {
                        SetValue(address, "District", parcel.Location.Address.District.Type + " " + parcel.Location.Address.District.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "City", parcel.Location.Address.City.Type + " " + parcel.Location.Address.City.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Urban_District", parcel.Location.Address.UrbanDistrict.Type + " " + parcel.Location.Address.UrbanDistrict.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Soviet_Village", parcel.Location.Address.SovietVillage.Type + " " + parcel.Location.Address.SovietVillage.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Locality", parcel.Location.Address.Locality.Type + " " + parcel.Location.Address.Locality.Name);
                    }
                    catch { }
                    try
                    {

                        SetValue(address, "Street", parcel.Location.Address.Street.Type + " " + parcel.Location.Address.Street.Name);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "House", parcel.Location.Address.Level1.Type + " " + parcel.Location.Address.Level1.Value);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Corpus", parcel.Location.Address.Level2.Type + " " + parcel.Location.Address.Level2.Value);

                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Building", parcel.Location.Address.Level3.Type + " " + parcel.Location.Address.Level3.Value);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Flat", parcel.Location.Address.Apartment.Type + " " + parcel.Location.Address.Apartment.Value);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Other", parcel.Location.Address.Other);
                    }
                    catch { }
                    try
                    {
                        SetValue(address, "Description", parcel.Location.Address.Note);
                        SetValue(regObject, "Adress", parcel.Location.Address.Note);
                    }
                    catch { }
                    regObject.DeleteAllAgregates(address);
                    address.SetLink("Lot", regObject);
                }

                #region Уточненное местоположение
                if (parcel.Location.Elaboration != null)
                {
                    DataObject elaboration = session["General/Location"].AddNew();
                    SetValue(elaboration, "Orientir", parcel.Location.Elaboration.ReferenceMark);
                    SetValue(elaboration, "Distance", parcel.Location.Elaboration.Distance);
                    SetValue(elaboration, "Direction", parcel.Location.Elaboration.Direction);
                    regObject.DeleteAllAgregates(elaboration);
                    elaboration.SetLink("Object", regObject);
                }
            }
            #endregion
            #endregion

            #region Номера земельных участков, из которых образован данный участок
            //foreach (string cadastralNubmer in parcel.PrevCadastralNumbers)
            //{
            //    DataObject prevCadNo = session["Land/HistCadNo"].AddNew();
            //    SetValue(prevCadNo, "HistCadNo", cadastralNubmer);
            //    prevCadNo.SetLink("HistObject", regObject);
            //}
            #endregion

            #region Категория земель
            if (parcel.Category != null)
            {
                DataObject category = session["Land/LotGroundCategory"].AddNew();
                if (parcel.Category != null)
                {
                    item = itemEnumCheck(parcel.Category.ToString());
                    category.SetLink("GroundCategory", session["Land/GroundCategory"].Query("", "Code=?", item)[0]);
                    item = "";
                }
                regObject.DeleteAllAgregates(category);
                category.SetLink("Object", regObject);
            }
            #endregion

            #region Использование участка
            if (parcel.Utilization != null)
            {
                DataObject util = session["Land/FactUsingWay"].AddNew();
                SetValue(util, "UsingWayDoc", parcel.Utilization.ByDoc);
                if (parcel.Utilization != null)
                {
                    item = itemEnumCheck(parcel.Utilization.Utilization.ToString());
                    util.SetLink("UsingWay", session["Land/UsingWay"].Query("", "Code=?", item)[0]);
                    item = "";
                }
                regObject.DeleteAllAgregates(util);
                util.SetLink("Object", regObject);
                SetValue(regObject, "UsingWayDoc", parcel.Utilization.ByDoc);
                if (parcel.Utilization != null)
                {
                    item = itemEnumCheck(parcel.Utilization.Utilization.ToString());
                    SetValue(regObject, "UsingWay",
                        session["Land/UsingWay"].Query("", "Code=?", item)[0]["Name"].ToString());
                    item = "";
                }
            }
            #endregion

            #region Природные объекты
            if (parcel.NaturalObjects != null)
            {
                foreach (tNaturalObject obj in parcel.NaturalObjects)
                {
                    DataObject nObj = session["Land/NaturalObject"].AddNew();
                    SetValue(nObj, "TypeProtectiveForest", obj.ProtectiveForest);
                    SetValue(nObj, "TypeWaterObject", obj.WaterObject);
                    SetValue(nObj, "NameObject", obj.NameOther);
                    SetValue(nObj, "OtherChar", obj.CharOther);
                    if (obj.Kind != null)
                    {
                        item = itemEnumCheck(obj.Kind.ToString());
                        nObj.SetLink("Name", session["NaturalObjectKind"].
                            Query("", "Code=?", item)[0]);
                        item = "";
                    }
                    if (obj.ForestUse != null)
                    {
                        item = itemEnumCheck(obj.ForestUse.ToString());
                        nObj.SetLink("ForestUse", session["General/ForestUseKind"].
                            Query("", "Code=?", item)[0]);
                        item = "";
                    }
                    regObject.DeleteAllAgregates(nObj);
                    nObj.SetLink("Lot", regObject);
                }
            }
            #endregion

            #region Права
            if (parcel.Rights != null)
            {
                foreach (tRight right in parcel.Rights)
                {
                    DataObject lotRight = session["General/Law"].AddNew();
                    SetValue(lotRight, "Name", right.Name);
                    SetValue(lotRight, "Desc", right.Desc);
                    if (right.Type != null)
                    {
                        item = itemEnumCheck(right.Type.ToString());
                        lotRight.SetLink("LawKind", session["General/LawKind"].Query("", "Code=?", item)[0]);
                        item = "";
                    }
                    if (right.Item != null)
                    {
                        if (right.Item.ToString() == "ShareText")
                        {
                            string share = right.Item.ToString();
                            SetValue(lotRight, "ShareText", share);
                        }
                        if (right.Item.ToString() == "Share")
                        {
                            tShare share = (tShare)right.Item;
                            SetValue(lotRight, "Share", share.Numerator + "/" + share.Denominator);
                        }
                    }
                    if (right.Registration != null)
                    {
                        SetValue(lotRight, "RegistrationNo", right.Registration.RegNumber);
                        if ((DateTime)right.Registration.RegDate != DateTime.MinValue)
                            SetValue(lotRight, "RegistrationDate", (DateTime)right.Registration.RegDate);
                    }
                    #region Owners
                    if (right.Owners != null)
                    {
                        
                        foreach (tOwner rightOwner in right.Owners)
                        {
                            DataObject rightSubject = session["General/LawSubject"].AddNew();
                            //if (rightOwner.ContactOwner != null)
                            //{
                            //    SetValue(rightSubject, "Address", rightOwner.ContactOwner.Address);
                            //    SetValue(rightSubject, "Email", rightOwner.ContactOwner.Email);
                            //}
                            if (rightOwner.Item != null)
                            {
                                if (rightOwner.ItemElementName == ItemChoiceType.Person)
                                {
                                    tOwnerPerson own = (tOwnerPerson)rightOwner.Item;
                                    SetValue(rightSubject, "Surname", own.FamilyName);
                                    SetValue(rightSubject, "FirstName", own.FirstName);
                                    SetValue(rightSubject, "Patronymic", own.Patronymic);
                                }
                                if (rightOwner.ItemElementName == ItemChoiceType.Organization)
                                {
                                    tNameOwner own = (tNameOwner)rightOwner.Item;
                                    SetValue(rightSubject, "OrganizationName", own.Name);
                                }
                                if (rightOwner.ItemElementName == ItemChoiceType.Governance)
                                {
                                    tNameOwner own = (tNameOwner)rightOwner.Item;
                                    SetValue(rightSubject, "GovernanceName", own.Name);
                                }
                            }
                            rightSubject.SetLink("LawID", lotRight);
                        }
                    }
                    #endregion

                    #region Documents
                    if (right.Documents != null)
                    {
                        foreach (tDocumentWithoutAppliedFile doc in right.Documents)
                        {
                            DataObject document = session["Rosreestr/Document"].AddNew();
                            SetValue(document, "Cod_Document", doc.CodeDocument);
                            SetValue(document, "Name", doc.Name);
                            SetValue(document, "Series", doc.Series);
                            SetValue(document, "Number", doc.Number);
                            if (doc.Date != DateTime.MinValue)
                                SetValue(document, "Date", doc.Date);
                            SetValue(document, "IssueOrgan", doc.IssueOrgan);
                            SetValue(document, "Desc", doc.Desc);
                            document.SetLink("LawID", lotRight);
                        }
                    }
                    #endregion
                    //regObject.DeleteAllAgregates(lotRight);
                    lotRight.SetLink("Object", regObject);
                }
            }
            #endregion

            #region Части участка
            if (parcel.SubParcels != null)
            {
                foreach (tSubParcel subParcel in parcel.SubParcels)
                {
                    DataObject subParc = null;
                    KPZU05_spatial.tSubParcel subParcelSpatial = null;
                    foreach(KPZU05_spatial.tSubParcel sp in parcelSpatial.SubParcels)
                    {
                        if (sp.NumberRecord.ToString() == subParcel.NumberRecord)
                        {
                            subParcelSpatial = sp;
                            break;
                        }
                    }
                    foreach (DataObject lpart in regObject.GetChilds("Land/LotPart"))
                        if (lpart["Nom"] != null)
                            if (lpart["Nom"].ToString() == subParcel.NumberRecord)
                            {
                                isExist = true;
                                subParc = lpart;
                                break;
                            }

                    if (!isExist)
                        subParc = session["Land/LotPart"].AddNew();
                    if (subParc != null)
                    {
                        SetValue(subParc, "Nom", subParcel.NumberRecord);
                        string lotPartCadNo = parcel.CadastralNumber + "/" + subParcel.NumberRecord + " - Сервитут";
                        SetValue(subParc, "CadNo", lotPartCadNo);
                        try
                        {
                            SetValue(subParc, "Address", parcel.Location.Address.Note);
                        }
                        catch { }
                        
                        SetValue(subParc, "Full", subParcel.Full);
                        if (subParcel.State != null)
                        {
                            item = itemEnumCheck(subParcel.State.ToString());
                            subParc.SetLink("LotState", session["Land/LotState"].Query("", "Code=?", item)[0]);
                            item = "";
                        }
                        if (subParcel.DateExpiry != DateTime.MinValue)
                            SetValue(subParc, "DateExpiry", subParcel.DateExpiry);

                        #region Площадь
                        if (subParcel.Area != null)
                        {
                            DataObject lotSquare = session["Land/LotSquares"].AddNew();
                            lotSquare.SetLink("AreaCode", session["General/AreaCode"].Query("", "Code=?", "002")[0]);
                            if (subParcel.Area.Unit != null)
                            {
                                item = itemEnumCheck(subParcel.Area.Unit.ToString());
                                lotSquare.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", item)[0]);
                                item = "";
                            }
                            SetValue(lotSquare, "Area", subParcel.Area.Area.ToString());
                            //SetValue(lotSquare, "Innccuracy", subParcel.Area.Innccuracy.ToString());
                            subParc.DeleteAllAgregates(lotSquare);
                            lotSquare.SetLink("Object", subParc);
                        }
                        #endregion
                        #region Ограничения прав
                        if (subParcel.Encumbrance != null)
                        {
                            DataObject enc = session["Land/Encumbrance"].AddNew();
                            SetValue(enc, "Name", subParcel.Encumbrance.Name);
                            //_____________________________________________
                            if (subParcel.Encumbrance.Item != null)
                            {
                                if (subParcel.Encumbrance.ItemElementName == ItemChoiceType1.CadastralNumberRestriction)
                                    SetValue(enc, "CadastralNumberRestriction", subParcel.Encumbrance.Item.ToString());
                                if (subParcel.Encumbrance.ItemElementName == ItemChoiceType1.AccountNumber)
                                    SetValue(enc, "AccountNumber", subParcel.Encumbrance.Item.ToString());
                            }
                            if (subParcel.Encumbrance.Type != null)
                            {
                                item = itemEnumCheck(subParcel.Encumbrance.Type.ToString());
                                enc.SetLink("LawKind", session["Land/EncumbranceKind"].Query("", "Code=?", item)[0]);
                                item = "";
                            }
                            if (subParcel.Encumbrance.Duration != null)
                            {
                                if (subParcel.Encumbrance.Duration.Started != DateTime.MinValue)
                                    SetValue(enc, "Started", subParcel.Encumbrance.Duration.Started);
                                if (subParcel.Encumbrance.Duration.Item != null)
                                {
                                    if (subParcel.Encumbrance.Duration.Item == "Stopped")
                                    {
                                        //if (subParcel.Encumbrance.Duration.Stopped != null)
                                        SetValue(enc, "Stopped", subParcel.Encumbrance.Duration.Item);
                                    }
                                    if (subParcel.Encumbrance.Duration.Item == "Term")
                                    {
                                        SetValue(enc, "Term", subParcel.Encumbrance.Duration.Item);
                                    }
                                }
                            }
                            if (subParcel.Encumbrance.Registration != null)
                            {
                                if (subParcel.Encumbrance.Registration.RegDate != DateTime.MinValue)
                                    SetValue(enc, "RegDate", subParcel.Encumbrance.Registration.RegDate);
                                SetValue(enc, "RegNumber", subParcel.Encumbrance.Registration.RegNumber);
                            }
                            if (subParcel.Encumbrance.OwnersRestrictionInFavorem != null)
                            {
                                foreach (tOwner owner in subParcel.Encumbrance.OwnersRestrictionInFavorem)
                                {

                                    DataObject ownerR = session["General/Owner_Restriction_InFavorem"].AddNew();
                                    if (owner.Item != null)
                                    {
                                        if (owner.ItemElementName == ItemChoiceType.Person)
                                        {
                                            tOwnerPerson own = (tOwnerPerson)owner.Item;
                                            SetValue(ownerR, "Surname", own.FamilyName);
                                            SetValue(ownerR, "FirstName", own.FirstName);
                                            SetValue(ownerR, "Patronymic", own.Patronymic);
                                        }
                                        if (owner.ItemElementName == ItemChoiceType.Organization)
                                        {
                                            tNameOwner own = (tNameOwner)owner.Item;
                                            SetValue(ownerR, "OrganizationName", own.Name);
                                        }
                                        if (owner.ItemElementName == ItemChoiceType.Organization)
                                        {
                                            tNameOwner own = (tNameOwner)owner.Item;
                                            SetValue(ownerR, "GovernanceName", own.Name);
                                        }
                                    }
                                    ownerR.SetLink("Encumbrance", enc);
                                }
                            }
                            //---------------------------------------------
                            #region Document
                            if (subParcel.Encumbrance.Document != null)
                            {
                                DataObject document = session["Rosreestr/Document"].AddNew();
                                SetValue(document, "Cod_Document", subParcel.Encumbrance.Document.CodeDocument);
                                SetValue(document, "Name", subParcel.Encumbrance.Document.Name);
                                SetValue(document, "Series", subParcel.Encumbrance.Document.Series);
                                SetValue(document, "Number", subParcel.Encumbrance.Document.Number);
                                if (subParcel.Encumbrance.Document.Date != DateTime.MinValue)
                                    SetValue(document, "Date", subParcel.Encumbrance.Document.Date);
                                SetValue(document, "IssueOrgan", subParcel.Encumbrance.Document.IssueOrgan);
                                SetValue(document, "Desc", subParcel.Encumbrance.Document.Desc);
                                document.SetLink("EncumbranceID", enc);
                            }
                            #endregion
                            //subParc.DeleteAllAgregates(enc);
                            enc.SetLink("Object", subParc);
                        }
                        #endregion

                        //regObject.DeleteAllAgregates(subParc);
                        subParc.SetLink("Object", regObject);

                        DataObject LotLink = null;

                        DataObjectList listLotLink = session["Land/Lot"].Query("", "CadNo=?", lotPartCadNo);
                        if (listLotLink.Count == 0)
                        {
                            LotLink = session["Land/Lot"].AddNew();
                            SetValue(LotLink, "CadNo", lotPartCadNo);
                            SetValue(LotLink, "ImportDate", DateTime.Now.Date);
                            //try
                            //{
                            SetValue(LotLink, "Adress", parcel.Location.Address.Note);
                            //}
                            //catch { }
                            if (subParcel.State != null)
                            {
                                item = itemEnumCheck(subParcel.State.ToString());
                                LotLink.SetLink("LotState", session["Land/LotState"].Query("", "Code=?", item)[0]);
                                item = "";
                            }
                            if (subParcel.DateExpiry != DateTime.MinValue)
                                SetValue(LotLink, "Note", "Дата истечения временного характера сведений о части: " + subParcel.DateExpiry.ToShortDateString());
                        }
                        else
                        {
                            LotLink = listLotLink[0];
                            SetValue(LotLink, "ImportChangeDate", DateTime.Now.Date);
                        }

                        subParc.SetLink("LotLink", LotLink);

                        // Импортируем пространственные данные части участка
                        if (subParcelSpatial != null)
                            if (subParcelSpatial.EntitySpatial != null)
                            {
                                IIngeoMapObject mapObjectLotLink = null;
                                if (parcel.Location.Address.Note != null)
                                    mapObjectLotLink = ImportGeometryLotPart(subParcelSpatial, lotPartCadNo, parcel.Location.Address.Note);
                                else
                                    mapObjectLotLink = ImportGeometryLotPart(subParcelSpatial, lotPartCadNo, "");

                                // Связываем пространственные данные с реестровыми
                                CreateLink(mapObjectLotLink, LotLink);
                            }

                    }
                    isExist = false;
                }
            }
            #endregion

            #region Сведения об ограничениях прав
            if (parcel.Encumbrances != null)
            {
                foreach (tEncumbranceZU encumbrance in parcel.Encumbrances)
                {
                    DataObject enc = session["Land/Encumbrance"].AddNew();
                    SetValue(enc, "Name", encumbrance.Name);
                    if (encumbrance.Item != null)
                    {
                        if (encumbrance.ItemElementName == ItemChoiceType1.CadastralNumberRestriction)
                            SetValue(enc, "CadastralNumberRestriction", encumbrance.Item.ToString());
                        if (encumbrance.ItemElementName == ItemChoiceType1.AccountNumber)
                            SetValue(enc, "AccountNumber", encumbrance.Item.ToString());
                    }
                    if (encumbrance.Type != null)
                    {
                        item = itemEnumCheck(encumbrance.Type.ToString());
                        enc.SetLink("LawKind", session["Land/EncumbranceKind"].Query("", "Code=?", item)[0]);
                        item = "";
                    }
                    if (encumbrance.Duration != null)
                    {
                        if (encumbrance.Duration.Started != DateTime.MinValue)
                            SetValue(enc, "Started", encumbrance.Duration.Started);
                        if (encumbrance.Duration.Item != null)
                        {
                            if (encumbrance.Duration.Item == "Stopped")
                            {
                                //if (subParcel.Encumbrance.Duration.Stopped != null)
                                SetValue(enc, "Stopped", encumbrance.Duration.Item);
                            }
                            if (encumbrance.Duration.Item == "Term")
                            {
                                SetValue(enc, "Term", encumbrance.Duration.Item);
                            }
                        }
                    }
                    if (encumbrance.Registration != null)
                    {
                        if (encumbrance.Registration.RegDate != DateTime.MinValue)
                            SetValue(enc, "RegDate", encumbrance.Registration.RegDate);
                        SetValue(enc, "RegNumber", encumbrance.Registration.RegNumber);
                    }
                    if (encumbrance.OwnersRestrictionInFavorem != null)
                    {
                        foreach (tOwner owner in encumbrance.OwnersRestrictionInFavorem)
                        {

                            DataObject ownerR = session["General/Owner_Restriction_InFavorem"].AddNew();
                            if (owner.Item != null)
                            {
                                if (owner.ItemElementName == ItemChoiceType.Person)
                                {
                                    tOwnerPerson own = (tOwnerPerson)owner.Item;
                                    SetValue(ownerR, "Surname", own.FamilyName);
                                    SetValue(ownerR, "FirstName", own.FirstName);
                                    SetValue(ownerR, "Patronymic", own.Patronymic);
                                }
                                if (owner.ItemElementName == ItemChoiceType.Organization)
                                {
                                    tNameOwner own = (tNameOwner)owner.Item;
                                    SetValue(ownerR, "OrganizationName", own.Name);
                                }
                                if (owner.ItemElementName == ItemChoiceType.Organization)
                                {
                                    tNameOwner own = (tNameOwner)owner.Item;
                                    SetValue(ownerR, "GovernanceName", own.Name);
                                }
                            }
                            ownerR.SetLink("Encumbrance", enc);
                        }
                    }
                    #region Document
                    if (encumbrance.Document != null)
                    {
                        DataObject document = session["Rosreestr/Document"].AddNew();
                        SetValue(document, "Cod_Document", encumbrance.Document.CodeDocument);
                        SetValue(document, "Name", encumbrance.Document.Name);
                        SetValue(document, "Series", encumbrance.Document.Series);
                        SetValue(document, "Number", encumbrance.Document.Number);
                        if (encumbrance.Document.Date != DateTime.MinValue)
                            SetValue(document, "Date", encumbrance.Document.Date);
                        SetValue(document, "IssueOrgan", encumbrance.Document.IssueOrgan);
                        SetValue(document, "Desc", encumbrance.Document.Desc);
                        document.SetLink("EncumbranceID", enc);
                    }
                    #endregion
                    regObject.DeleteAllAgregates(enc);
                    enc.SetLink("Object", regObject);
                }
            }
            #endregion

            #region Платежи
            if (parcel.CadastralCost != null)
            {
                DataObject payment = session["Land/GroundPayment"].AddNew();
                payment.SetLink("PaymentKind", session["General/PaymentKind"].Query("", "Code=?", "016010000000")[0]);
                payment.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", parcel.CadastralCost.Unit)[0]);
                SetValue(payment, "Value", parcel.CadastralCost.Value.ToString());
                regObject.DeleteAllAgregates(payment);
                payment.SetLink("Lot", regObject);
            }
            #endregion

            #region Правообладатели смежных участков
            //if (parcel.ParcelNeighbours != null)
            //{
            //    foreach (tParcelParcelNeighbour ownerNeighbour in parcel.ParcelNeighbours)
            //    {
            //        foreach (DataObject ad in regObject.GetChilds("Land/Adjacency"))
            //        {
            //            if(ad["CadNo"]!= null)
            //                if(ownerNeighbour.CadastralNumber != null)
            //                    if (ad["CadNo"].ToString() == ownerNeighbour.CadastralNumber.ToString())
            //                    {
            //                        isExist = true;
            //                        break;
            //                    }
            //        }
            //        if (!isExist)
            //        {
            //            DataObject on = session["Land/Adjacency"].AddNew();
            //            SetValue(on, "CadNo", ownerNeighbour.CadastralNumber);
            //            string ownstring = "";
            //            foreach (tParcelParcelNeighbourOwnerNeighbour own in ownerNeighbour.OwnerNeighbour)
            //            {
            //                if (ownstring == "")
            //                    ownstring = own.ContactAddress + ", " + own.Email;
            //                else
            //                    ownstring = ownstring + "; " + own.ContactAddress + ", " + own.Email;
            //            }
            //            SetValue(on, "NeighbourAddr", ownstring);
            //            //regObject.DeleteAllAgregates(on);
            //            on.SetLink("Lot", regObject);
            //        }
            //        isExist = false;
            //    }
            //}
            #endregion


            session.Commit();
            return regObject;
        }

        private string itemEnumCheck(string item)
        {
            string newItem = "";
            if (item.Contains("Item"))
            {
                newItem = item.Replace("Item", "");
            }
            else
                newItem = item;
            return newItem;
        }

        /// <summary>
        /// Импортируем геометрию объекта
        /// </summary>
        /// <param name="parcelSpatial">описание координатной части участка</param>
        /// <param name="cadNo">кадастровый номер участка</param>
        /// <param name="address">адрес участка</param>
        /// <returns>Пространственный объект земельного участка</returns>
        private IIngeoMapObject ImportGeometry(tParcelSpatial parcelSpatial, string cadNo, string address)
        {
            IIngeoMapObject mapObject = null;
            List<IIngeoShape> shapesToDel = new List<IIngeoShape>();

            //mapObject.MapObjects.UpdateChanges();
            //System.Windows.Forms.MessageBox.Show(mapObject.Shapes.Count.ToString());

            #region if (parcelSpatial.EntitySpatial != null)
            if (parcelSpatial.EntitySpatial != null)
            {
                string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
                if (!objId.Equals(string.Empty))
                {
                    mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                    //System.Windows.Forms.MessageBox.Show(mapObject.Shapes.Count.ToString());
                    foreach (IIngeoShape sh in mapObject.Shapes)
                        shapesToDel.Add(sh);
                    isModify = true;
                }
                else
                    mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
                foreach (IIngeoShape s in shapesToDel)
                {
                    s.Delete();
                }
                // Считаем, что первый shape - основной контур, остальные вырезаются из него
                int i = 0;
                IIngeoShape mainshape = null;
                List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                foreach (tSpatialElement spelem in parcelSpatial.EntitySpatial.SpatialElements)
                {
                    IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                    IIngeoContourPart contour = shape.Contour.Insert(-1);
                    foreach (KPZU05_spatial.tSpelementUnitZUOut spelementUnit in spelem.SpelementUnits)
                    {
                        Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                        contour.Closed = true;
                    }
                    if (i == 0)
                        mainshape = shape;
                    else
                        holeshapes.Add(shape);
                    mapObject.MapObjects.UpdateChanges();
                    i++;
                }
                shapesToDel = new List<IIngeoShape>();
                foreach (IIngeoShape shape in holeshapes)
                {

                    if (shape != mainshape)
                    {
                        mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                        shapesToDel.Add(shape);
                    }
                }
                //foreach (IIngeoShape shape in mapObject.Shapes)
                //{
                //    foreach (IIngeoShape othershape in mapObject.Shapes)
                //    {
                //        if (shape.Index != othershape.Index)
                //        {
                //            //System.Windows.Forms.MessageBox.Show(shape.Contour.Square + " " +
                //            //    shape.Contour.TestRelation(othershape.Contour).ToString());
                //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "3")
                //            {
                //                othershape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                //                shapesToDel.Add(shape);
                //            }
                //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "5")
                //            {
                //                shape.Contour.Combine(TIngeoContourCombineOperation.inccSub, othershape.Contour);
                //                shapesToDel.Add(othershape);
                //            }
                //        }
                //    }
                //}
                foreach (IIngeoShape s in shapesToDel)
                {
                    s.Delete();
                }
                mapObject.MapObjects.UpdateChanges();

            }
            #endregion
            else
                #region if (parcelSpatial.Contours != null)
                if (parcelSpatial.Contours != null)
                {
                    string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
                    if (!objId.Equals(string.Empty))
                    {
                        mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                        //System.Windows.Forms.MessageBox.Show(mapObject.Shapes.Count.ToString());
                        foreach (IIngeoShape sh in mapObject.Shapes)
                            shapesToDel.Add(sh);
                        isModify = true;
                    }
                    else
                        mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
                    foreach (IIngeoShape s in shapesToDel)
                    {
                        s.Delete();
                    }
                    foreach (KPZU05_spatial.tContour contour in parcelSpatial.Contours)
                    {
                        if (contour.EntitySpatial != null)
                        {
                            // Считаем, что первый shape - основной контур, остальные вырезаются из него
                            int i = 0;
                            IIngeoShape mainshape = null;
                            List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                            foreach (tSpatialElement spelem in contour.EntitySpatial.SpatialElements)
                            {
                                IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                                IIngeoContourPart cont = shape.Contour.Insert(-1);
                                foreach (KPZU05_spatial.tSpelementUnitZUOut spelementUnit in spelem.SpelementUnits)
                                {
                                    Transforms.Start(cont, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);

                                }
                                cont.Closed = true;
                                if (i == 0)
                                    mainshape = shape;
                                else
                                    holeshapes.Add(shape);
                                mapObject.MapObjects.UpdateChanges();
                                i++;
                            }
                            shapesToDel = new List<IIngeoShape>();
                            foreach (IIngeoShape shape in holeshapes)
                            {

                                if (shape != mainshape)
                                {
                                    mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                                    shapesToDel.Add(shape);
                                }
                            }
                            //foreach (IIngeoShape shape in mapObject.Shapes)
                            //{
                            //    foreach (IIngeoShape othershape in mapObject.Shapes)
                            //    {
                            //        if (shape.Index != othershape.Index)
                            //        {
                            //            //System.Windows.Forms.MessageBox.Show(shape.Contour.Square + " " +
                            //            //    shape.Contour.TestRelation(othershape.Contour).ToString());
                            //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "3")
                            //            {
                            //                othershape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                            //                shapesToDel.Add(shape);
                            //            }
                            //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "5")
                            //            {
                            //                shape.Contour.Combine(TIngeoContourCombineOperation.inccSub, othershape.Contour);
                            //                shapesToDel.Add(othershape);
                            //            }
                            //        }
                            //    }
                            //}
                            foreach (IIngeoShape s in shapesToDel)
                            {
                                s.Delete();
                            }
                        }
                    }
                }
                #endregion
                else
                    #region if (parcelSpatial.CompositionEZ != null)
                    if (parcelSpatial.CompositionEZ != null)
                    {
                        if (parcelSpatial.CompositionEZ.EntryParcel != null)
                        {
                            KPZU05_spatial.tEntryParcel entryParcel = parcelSpatial.CompositionEZ.EntryParcel;
                            if (entryParcel.EntitySpatial != null)
                            {
                                string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
                                if (!objId.Equals(string.Empty))
                                {
                                    mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                                    //System.Windows.Forms.MessageBox.Show(mapObject.Shapes.Count.ToString());
                                    foreach (IIngeoShape sh in mapObject.Shapes)
                                        shapesToDel.Add(sh);
                                    isModify = true;
                                }
                                else
                                    mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
                                foreach (IIngeoShape s in shapesToDel)
                                {
                                    s.Delete();
                                }
                                // Считаем, что первый shape - основной контур, остальные вырезаются из него
                                //int i = 0;
                                //IIngeoShape mainshape = null;
                                //List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                                foreach (tSpatialElement spelem in entryParcel.EntitySpatial.SpatialElements)
                                {
                                    IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                                    IIngeoContourPart cont = shape.Contour.Insert(-1);
                                    foreach (KPZU05_spatial.tSpelementUnitZUOut spelementUnit in spelem.SpelementUnits)
                                    {
                                        Transforms.Start(cont, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);

                                    }
                                    cont.Closed = true;
                                    //if (i == 0)
                                    //    mainshape = shape;
                                    //else
                                    //    holeshapes.Add(shape);
                                    mapObject.MapObjects.UpdateChanges();
                                    //i++;
                                }
                                shapesToDel = new List<IIngeoShape>();
                                //foreach (IIngeoShape shape in holeshapes)
                                //{

                                //    if (shape != mainshape)
                                //    {
                                //        mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                                //        shapesToDel.Add(shape);
                                //    }
                                //}
                                foreach (IIngeoShape shape in mapObject.Shapes)
                                {
                                    foreach (IIngeoShape othershape in mapObject.Shapes)
                                    {
                                        if (shape.Index != othershape.Index)
                                        {
                                            //System.Windows.Forms.MessageBox.Show(shape.Contour.Square + " " +
                                            //    shape.Contour.TestRelation(othershape.Contour).ToString());
                                            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "3")
                                            {
                                                othershape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                                                shapesToDel.Add(shape);
                                            }
                                            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "5")
                                            {
                                                shape.Contour.Combine(TIngeoContourCombineOperation.inccSub, othershape.Contour);
                                                shapesToDel.Add(othershape);
                                            }
                                        }
                                    }
                                }
                                foreach (IIngeoShape s in shapesToDel)
                                {
                                    s.Delete();
                                }
                            }
                        }
                    }
                    #endregion
            if (mapObject != null)
            {
                try
                {
                    mapObject.SemData.SetValue("LotInfo", "CadNo", cadNo, 0);
                }
                catch { }
                try
                {
                    mapObject.SemData.SetValue("LotInfo", "ADDR", address, 0);
                }
                catch { }
                mapObject.MapObjects.UpdateChanges();
            }
            return mapObject;
        }

        /// <summary>
        /// Импортируем геометрию части участка
        /// </summary>
        /// <param name="parcelSpatial">описание координатной части участка</param>
        /// <param name="cadNo">кадастровый номер участка</param>
        /// <param name="address">адрес участка</param>
        /// <returns>Пространственный объект земельного участка</returns>
        private IIngeoMapObject ImportGeometryLotPart(KPZU05_spatial.tSubParcel parcelSpatial, string cadNo, string address)
        {
            IIngeoMapObject mapObject = null;
            if (parcelSpatial.EntitySpatial != null)
            {
                string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
                if (!objId.Equals(string.Empty))
                {
                    mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                    foreach (IIngeoShape sh in mapObject.Shapes)
                        sh.Delete();
                    isModify = true;
                }
                else
                    mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
                // Считаем, что первый shape - основной контур, остальные вырезаются из него
                int i = 0;
                IIngeoShape mainshape = null;
                List<IIngeoShape> holeshapes = new List<IIngeoShape>();
                foreach (tSpatialElement spelem in parcelSpatial.EntitySpatial.SpatialElements)
                {
                    IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                    IIngeoContourPart contour = shape.Contour.Insert(-1);
                    foreach (KPZU05_spatial.tSpelementUnitZUOut spelementUnit in spelem.SpelementUnits)
                    {
                        Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                        contour.Closed = true;
                    }
                    if (i == 0)
                        mainshape = shape;
                    else
                        holeshapes.Add(shape);
                    mapObject.MapObjects.UpdateChanges();
                    i++;
                }
                List<IIngeoShape> shapesToDel = new List<IIngeoShape>();
                foreach (IIngeoShape shape in holeshapes)
                {

                    if (shape != mainshape)
                    {
                        mainshape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                        shapesToDel.Add(shape);
                    }
                }
                //foreach (IIngeoShape shape in mapObject.Shapes)
                //{
                //    foreach (IIngeoShape othershape in mapObject.Shapes)
                //    {
                //        if (shape.Index != othershape.Index)
                //        {
                //            //System.Windows.Forms.MessageBox.Show(shape.Contour.Square + " " +
                //            //    shape.Contour.TestRelation(othershape.Contour).ToString());
                //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "3")
                //            {
                //                othershape.Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                //                shapesToDel.Add(shape);
                //            }
                //            if (shape.Contour.TestRelation(othershape.Contour).ToString() == "5")
                //            {
                //                shape.Contour.Combine(TIngeoContourCombineOperation.inccSub, othershape.Contour);
                //                shapesToDel.Add(othershape);
                //            }
                //        }
                //    }
                //}
                foreach (IIngeoShape s in shapesToDel)
                {
                    s.Delete();
                }
                mapObject.MapObjects.UpdateChanges();

            }

            if (mapObject != null)
            {
                try
                {
                    mapObject.SemData.SetValue("LotInfo", "CadNo", cadNo, 0);
                }
                catch { }
                try
                {
                    mapObject.SemData.SetValue("LotInfo", "ADDR", address, 0);
                }
                catch { }
                mapObject.MapObjects.UpdateChanges();
            }
            return mapObject;
        }

        
        private void ImportSemantics(tParcel parcel, IIngeoMapObject mapObject)
        {
            //import(parcel.XElement, mapObject);
            //nextNode(parcel.XElement, mapObject);

            mapObject.MapObjects.UpdateChanges();
        }

        void nextNode(XElement elem, IIngeoMapObject mapObject)
        {
            foreach (XElement el in elem.Elements())
            {
                import(el, mapObject);
                nextNode(el, mapObject);
            }
        }

        void import(XElement el, IIngeoMapObject mapObject)
        {
            foreach (XAttribute attr in el.Attributes())
            {
                //System.Windows.MessageBox.Show(XmlTemplate.GetPath(el));
                XmlTemplateItem item = XmlTemplate.Template[XmlTemplate.GetPath(el), attr.Name.ToString()];
                if (item != null)
                    mapObject.SemData.SetValue(item.TableName, item.FieldName, attr.Value, 0);
            }
            if (!el.HasElements)
                if (el.Value.Length > 0)
                {
                    XmlTemplateItem item = XmlTemplate.Template[XmlTemplate.GetPath(el), "Значение узла"];
                    if (item != null)
                        mapObject.SemData.SetValue(item.TableName, item.FieldName, el.Value, 0);
                }
        }

        private void CreateLink(IIngeoMapObject mapObject, DataObject regObject)
        {
            mapObject.SemData.SetValue("InMetaLink", "RegisterNo", regObject.Id.ToString(), 0);
            mapObject.SemData.SetValue("InMetaLink", "Classifier", "Land/Lot", 0);
            mapObject.MapObjects.UpdateChanges();
        }

        private void SetValue(DataObject obj, string property, object value)
        {
            if (value == null || value == "" || value == " ")
                return;
            switch (value.GetType().Name)
            {
                case "Int32": obj.SetInteger(property, (int)value); break;
                case "String": obj.SetString(property, (string)value); break;
                case "Double": obj.SetDouble(property, (double)value); break;
                case "Float": obj.SetDouble(property, (double)value); break;
                case "Boolean": obj.SetBoolean(property, (bool)value); break;
                case "DateTime":
                    if ((DateTime)value!= DateTime.MinValue)
                        obj.SetDateTime(property, (DateTime)value); break;
            }
        }
    }
}
