﻿using Integro.InMeta.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace KPZU06_spatial
{

    public class Parser
    {
        private XmlDocument doc = new XmlDocument();

        public Parser(string fileName)
        {
            doc.Load(fileName);
        }

        /// <summary>
        /// Заполняем поля объекта данными из XML элемента
        /// </summary>
        /// <param name="obj">Объект - приемник</param>
        /// <param name="element">XML элемент - источник</param>
        private void ParseElement(object obj, XmlElement element)
        {
            if (element == null)
                return;
            foreach (XmlAttribute attr in element.Attributes)
            {
                FieldInfo field = obj.GetType().GetField(attr.Name);
                if (field == null)
                    continue;
                switch (field.FieldType.Name)
                {
                    case "String":
                        field.SetValue(obj, attr.Value);
                        break;
                    case "Int32":
                        field.SetValue(obj, Int32.Parse(attr.Value));
                        break;
                    case "Double":
                        field.SetValue(obj, Double.Parse(attr.Value.Replace('.', ',')));
                        break;
                    case "Nullable`1":
                        if (field.FieldType.FullName.Contains("DateTime"))
                            field.SetValue(obj, DateTime.Parse(attr.Value));
                        break;
                }

            }
            foreach (XmlElement el in element.ChildNodes)
            {
                FieldInfo field = obj.GetType().GetField(el.Name);
                if (field == null)
                    continue;
                switch (field.FieldType.Name)
                {
                    case "String":
                        field.SetValue(obj, el.InnerText);
                        break;
                    case "Int32":
                        field.SetValue(obj, Int32.Parse(el.InnerText));
                        break;
                    case "Double":
                        field.SetValue(obj, Double.Parse(el.InnerText.Replace('.', ',')));
                        break;
                    case "Nullable`1":
                        if (field.FieldType.FullName.Contains("DateTime"))
                            field.SetValue(obj, DateTime.Parse(el.InnerText));
                        break;
                }
            }
        }

        /// <summary>
        /// Получение полного кадастрового номера
        /// </summary>
        /// <param name="xParcel">Узел Parcel</param>
        /// <returns>Полный кадастровый номер</returns>
        private string GetCadastralNumber(XmlElement xParcel)
        {
            if (!Regex.IsMatch(xParcel.Attributes["CadastralNumber"].Value, "[0-9]+:[0-9]+:[0-9]+:[0-9]+"))
            {
                return xParcel.ParentNode.ParentNode.Attributes["CadastralNumber"].Value +
                    xParcel.Attributes["CadastralNumber"].Value;
            }
            else
            {
                return xParcel.Attributes["CadastralNumber"].Value;
            }
        }


        public tParcelSpatial ParseDocument()
        {
            tKPZU cv = new tKPZU();
            XmlElement root = doc["KPZU"];
            ParseElement(cv, root);
            //cv.Package = GetPackage(root["Package"]);

            cv.Parcel = GetParcelSpatial(root["Parcel"]);

            return cv.Parcel;
        }

        

        #region Parcel
        public tParcelSpatial GetParcelSpatial(XmlElement xParcel)
        {
            tParcelSpatial parcel = new tParcelSpatial();
            
            ParseElement(parcel, xParcel);
            parcel.XElement = System.Xml.Linq.XElement.Load(xParcel.CreateNavigator().ReadSubtree());
            if (xParcel.Attributes["CadastralNumber"] != null)
                parcel.CadastralNumber = GetCadastralNumber(xParcel);
            parcel.EntitySpatial = GetEntitySpatial(xParcel["EntitySpatial"]);
            parcel.Contours = GetContours(xParcel["Contours"]);
            parcel.CompositionEZ = GetCompositionEZ(xParcel["CompositionEZ"]);
            parcel.SubParcels = GetSubParcels(xParcel["SubParcels"]);
            
            return parcel;
        }

        private List<tContour> GetContours(XmlElement xContours)
        {
            if (xContours == null)
                return null;
            List<tContour> contours = new List<tContour>();
            foreach (XmlElement xContour in xContours.GetElementsByTagName("Contour"))
            {
                tContour contour = new tContour();
                ParseElement(contour, xContour);
                if (xContour["EntitySpatial"] != null)
                    contour.EntitySpatial = GetEntitySpatial(xContour["EntitySpatial"]);
                contours.Add(contour);
            }
            return contours;
        }

        private List<tSubParcel> GetSubParcels(XmlElement xSubParcels)
        {
            if (xSubParcels == null)
                return null;
            List<tSubParcel> subParcels = new List<tSubParcel>();
            foreach (XmlElement xSubParcel in xSubParcels.GetElementsByTagName("SubParcel"))
            {
                tSubParcel subParcel = new tSubParcel();
                ParseElement(subParcel, xSubParcel);
                //if (xSubParcel["Object_Entry"] != null)
                //{
                //    subParcel.ObjectEntry = new tObjectEntry();
                //    ParseElement(subParcel.ObjectEntry, xSubParcel["Object_Entry"]);
                //}
                if (xSubParcel["EntitySpatial"] != null)
                {
                    subParcel.EntitySpatial = GetEntitySpatial(xSubParcel["EntitySpatial"]);
                    //subParcel.EntitySpatial = new tEntitySpatial();
                    //ParseElement(subParcel.EntitySpatial, xSubParcel["EntitySpatial"]);
                }
                subParcels.Add(subParcel);
            }
            return subParcels;
        }

        #region Entity_Spatial
        private tEntitySpatial GetEntitySpatial(XmlElement xEntitySpatial)
        {
            tEntitySpatial entitySpatial = new tEntitySpatial();
            if (xEntitySpatial == null)
                return null;
            ParseElement(entitySpatial, xEntitySpatial);
            entitySpatial.SpatialElements = GetSpatialElements(xEntitySpatial.GetElementsByTagName("ns7:SpatialElement"));
            //entitySpatial.Borders = ;
            return entitySpatial;
        }

        private List<tSpatialElement> GetSpatialElements(XmlNodeList xSpatialElements)
        {
            List<tSpatialElement> spatialElements = new List<tSpatialElement>();
            foreach (XmlElement xSpatialElement in xSpatialElements)
            {
                tSpatialElement spatialElement = new tSpatialElement();
                spatialElement.SpelementUnits = GetSpelementUnits(xSpatialElement.GetElementsByTagName("ns7:SpelementUnit"));
                spatialElements.Add(spatialElement);
            }
            return spatialElements;
        }

        private List<tSpelementUnitZUOut> GetSpelementUnits(XmlNodeList xSpelementUnits)
        {
            List<tSpelementUnitZUOut> spelementUnits = new List<tSpelementUnitZUOut>();
            foreach (XmlElement xSpelementUnit in xSpelementUnits)
            {
                tSpelementUnitZUOut spelementUnit = new tSpelementUnitZUOut();
                ParseElement(spelementUnit, xSpelementUnit);
                spelementUnit.Ordinate = GetOrdinate(xSpelementUnit["ns7:Ordinate"]);
                spelementUnits.Add(spelementUnit);
            }
            return spelementUnits;
        }

        private Ordinate GetOrdinate(XmlElement xOrdinate)
        {
            Ordinate ordinate = new Ordinate();
            ParseElement(ordinate, xOrdinate);
            //ordinate.tOrdinateOut = GetOrdinateOut(xOrdinate["Ordinate"]);
            return ordinate;
        }
        //private tOrdinateOut GetOrdinateOut(XmlElement xOrdinateOut)
        //{
        //    tOrdinateOut OrdinateOut = new tOrdinateOut();
        //    ParseElement(OrdinateOut, xOrdinateOut);
        //    return OrdinateOut;
        //}
        #endregion

        private tCompositionEZ GetCompositionEZ(XmlElement xCompositionEZ)
        {
            tCompositionEZ compositionEZ = new tCompositionEZ();
            if (xCompositionEZ == null)
                return null;
            ParseElement(compositionEZ, xCompositionEZ);
            compositionEZ.EntryParcel = GetEntryParcel(xCompositionEZ["EntryParcel"]);
            return compositionEZ;
        }

        private tEntryParcel GetEntryParcel(XmlElement xEntryParcel)
        {
            tEntryParcel entryParcel = new tEntryParcel();
            if (xEntryParcel != null)
            {
                ParseElement(entryParcel, xEntryParcel);
                if (xEntryParcel["EntitySpatial"] != null)
                {
                    entryParcel.EntitySpatial = GetEntitySpatial(xEntryParcel["EntitySpatial"]);
                }
            }
            return entryParcel;
        }

       
        #endregion
    }
}
