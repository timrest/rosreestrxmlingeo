﻿using Helpers;
using Ingeo;
using InMeta;
using InMeta.Land;
using InMeta.Rosreestr;
using Integro.InMeta.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KVZU
{
    public class Importer
    {
        private tKVZU CV;
        private IngeoApplication ingeo;
        private DataApplication inmeta;
        private DataSession session;
        //private InMeta.Session inmetaSession;
        public string TableName;
        public string LayerId;
        public string StyleId;
        public string FileType { get; set; }
        bool isModify = false;

        public Importer(IngeoHelper Ingeo, InmetaHelper Inmeta)
        {
            ingeo = Ingeo.Application;
            inmeta = Inmeta.Application;
            session = Inmeta.Session;
            //inmetaSession = (Session)session;
        }

        public void Import(tKVZU cv)
        {
            CV = cv;
            // Импортируем описание документа
            DataObject eDocument = ImportAdditionalRegistryData();
            ProgressBarLogger.MaxValue = 1;
            //foreach (tParcel parcel in cv.Package.Parcels)
            //{
                ProgressBarLogger.ProgressInc();
                /*if (session["Land/Lot"].Query("", "CadNo=?", parcel.CadastralNumber).Count > 0)
                    continue; */
                tParcel parcel = cv.Parcels.Parcel;
                // Импортируем данные текущего участка
                DataObject regParcel = ImportRegistryData(parcel);
                // Связываем участок с документом
                LinkLotAndDoc(regParcel, eDocument);
                // Импортируем пространственные данные
                IIngeoMapObject mapObject = null;
                if (parcel.EntitySpatial != null)
                    mapObject = ImportGeometry(parcel.EntitySpatial, parcel.CadastralNumber);
                else if (parcel.Contours != null && parcel.Contours.Count > 0)
                    mapObject = ImportGeometry(parcel.Contours, parcel.CadastralNumber);

                if (mapObject == null)
                {
                    try
                    {
                        if (isModify)
                            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " обновлен (точки не найдены)");
                        else
                            ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " добавлен (точки не найдены)");
                    }
                    catch { }
                    //continue;
                }

                // Импортируем семантику
                ImportSemantics(parcel, mapObject);
                // Связываем пространственные данные с реестровыми
                CreateLink(mapObject, regParcel);
                try
                {
                    if (isModify)
                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " обновлен");
                    else
                        ProgressBarLogger.Writer.WriteLine(this.FileType + ": " + parcel.CadastralNumber + " добавлен");
                }
                catch { }
                if (Settings.IsZoom)
                {
                    if (Settings.IsAllObjectsZoom)
                        Settings.ZoomRect = new Rect(mapObject, Settings.ZoomRect);
                    else
                        Settings.ZoomRect = new Rect(mapObject);
                }
            //}
        }

        private DataObject ImportAdditionalRegistryData()
        {
            #region Описание документа
            DataObject eDoc = session["Rosreestr/eDocument"].AddNew();
            SetValue(eDoc, "CodeType", 114);
            SetValue(eDoc, "Version", 06);
            SetValue(eDoc, "DateUpload", DateTime.Now);
            SetValue(eDoc, "Organization", CV.CertificationDoc.Organization);
            SetValue(eDoc, "Date", CV.CertificationDoc.Date);
            SetValue(eDoc, "Number", CV.CertificationDoc.Number);
            SetValue(eDoc, "Appointment", CV.CertificationDoc.Appointment);
            SetValue(eDoc, "FIO", CV.CertificationDoc.FIO);
            //gFIO fio = CV.CertificationDoc.FIO;
            //SetValue(eDoc, "FamilyName", fio.FamilyName);
            //SetValue(eDoc, "FirstName", fio.FirstName);
            //SetValue(eDoc, "Patronymic", fio.Patronymic);
            #endregion

            #region Координатные системы
            foreach (KeyValuePair<string, tCoordSystem> cs in CV.CoordSystems)
            {
                if (session["General/CoordSys"].Query("", "Code=?", cs.Key).Count > 0)
                    continue;
                DataObject coordSys = session["General/CoordSys"].AddNew();
                coordSys.SetString("Code", cs.Value.Cs_Id);
                coordSys.SetString("Name", cs.Value.Name);
            }
            #endregion

            #region Реестр исполнителей работ
            foreach (tContractor contractor in CV.Contractors)
            {
                //try
                //{
                //tCadastralEngineer engineer = (tCadastralEngineer)contractor.Value;
                //Contractor inmetaContractor = inmetaSession.Rosreestr_Contractor.AddNew();
                DataObject contr = session["Rosreestr/Contractor"].AddNew();
                //Contractor inmetaContractor;
                //inmetaContractor= contr as Contractor;
                contr.SetLink("eDocument", eDoc);
                SetValue(contr, "Date", contractor.Date);
                //inmetaContractor.eDocument = eDoc as eDocument;
                DataObject cadEngineer;
                //inmetaContractor.Date = (DateTime)contractor.Date;
                if (session["Rosreestr/Cadastral_Engineer"].Query("", "NCertificate=?", contractor.tEngineerOut.NCertificate).Count == 0)
                {
                    cadEngineer = session["Rosreestr/Cadastral_Engineer"].AddNew();
                    SetValue(cadEngineer, "NCertificate", contractor.tEngineerOut.NCertificate);
                    SetValue(cadEngineer, "LastName", contractor.tEngineerOut.FIO.FamilyName);
                    SetValue(cadEngineer, "FirstName", contractor.tEngineerOut.FIO.FirstName);
                    SetValue(cadEngineer, "Patronymic", contractor.tEngineerOut.FIO.Patronymic);
                    SetValue(cadEngineer, "OrganizationName", contractor.tEngineerOut.Organization);
                }
                else
                {
                    cadEngineer = session["Rosreestr/Cadastral_Engineer"].Query("", "NCertificate=?", contractor.tEngineerOut.NCertificate)[0];
                }
                contr.SetLink("WorkContactor", cadEngineer);
                //inmetaContractor.WorkContractor = cadEngineer;
                //}
                //catch { }
            }
            #endregion

            session.Commit();
            return eDoc;
        }
        private void LinkLotAndDoc(DataObject lot, DataObject doc)
        {
            DataObject rosreestrLots = session["Rosreestr/Lots"].AddNew();
            rosreestrLots.SetLink("eDocument", doc);
            
            eDocument eDoc = (eDocument)doc;
            foreach (Contractor inmetaContractor in eDoc.Rosreestr_Contractor)
            {
                inmetaContractor.Lot = (Lot)lot;
            }
            //lot.DeleteAllAgregates(rosreestrLots);
            rosreestrLots.SetLink("Lot", lot);
            session.Commit();
        }
        private DataObject ImportRegistryData(tParcel parcel)
        {
            DataObject regObject = null;
            DataObjectList listL = session["Land/Lot"].Query("", "CadNo=?", parcel.CadastralNumber);
            if (listL.Count == 0)
                regObject = session["Land/Lot"].AddNew();
            else
                regObject = listL[0];

            DataObject coordDescr = null;

            if (parcel.EntitySpatial != null)
                foreach (tSpatialElement spEl in parcel.EntitySpatial.SpatialElements)
                {
                    /// Удаление старых точек
                    DataObjectList refList = session["Land/CoordDescription"].Query(
                        "", "Lot=?", regObject.Id.ToString());
                    foreach (DataObject refObj in refList)
                    {
                        DataObjectList pointList = session["Land/Coord"].Query("",
                            "CoordDescription=?", refObj.Id.ToString());
                        foreach (DataObject pointObj in pointList)
                            pointObj.Delete();
                        refObj.Delete();
                    }
                    /// Добавление новых точек
                    coordDescr = session["Land/CoordDescription"].AddNew();
                    coordDescr.SetLink("Lot", regObject);
                    foreach (tSpelementUnitZUOut spUnit in spEl.SpelementUnits)
                    {
                        DataObject coord = session["Land/Coord"].AddNew();
                        coord.SetDouble("CoordX", spUnit.Ordinate.X);
                        coord.SetDouble("CoordY", spUnit.Ordinate.Y);
                        coord.SetInteger("CoordsNo", spUnit.Su_Nmb);
                        coord.SetLink("CoordDescription", coordDescr);
                    }
                }

            SetValue(regObject, "CadNo", parcel.CadastralNumber);
            if (parcel.DateExpiry != null)
                SetValue(regObject, "DateExpiry", parcel.DateExpiry);
            if (parcel.DateCreated != null)
                SetValue(regObject, "CadDate", parcel.DateCreated);
            if (parcel.DateRemoved != null)
                SetValue(regObject, "DateRemoved", parcel.DateRemoved);
            if (parcel.DateCreatedDoc != null)
                SetValue(regObject, "DateCreatedDoc", parcel.DateCreatedDoc);
            SetValue(regObject, "RegisterNo", parcel.CadastralNumber);
            SetValue(regObject, "CadastralBlock", parcel.CadastralBlock);
            string oldNumbString = "";
            foreach (tOldNumber oldNumber in parcel.OldCadastralNumbers)
            {
                if(oldNumbString == "")
                    oldNumbString = oldNumber.Type + " " + oldNumber.Number;
                else
                    oldNumbString = oldNumbString + ", " + oldNumber.Type + " " + oldNumber.Number;
            }
            SetValue(regObject, "ConditionalNo", oldNumbString);

            string prevCadastralNumString = "";
            foreach (string prevCadastralNum in parcel.PrevCadastralNumbers)
            {
                if (prevCadastralNumString == "")
                    prevCadastralNumString = prevCadastralNum;
                else
                    prevCadastralNumString = prevCadastralNumString + ", " + prevCadastralNum;
            }
            SetValue(regObject, "PrevCadastralNumbers", prevCadastralNumString);

            string InnerCadastralNumString = "";
            foreach (string InnerCadastralNum in parcel.InnerCadastralNumbers)
            {
                if (InnerCadastralNumString == "")
                    InnerCadastralNumString = InnerCadastralNum;
                else
                    InnerCadastralNumString = oldNumbString + ", " + InnerCadastralNum;
            }
            SetValue(regObject, "InnerCadastralNumbers", InnerCadastralNumString);

            SetValue(regObject, "SpecialNote", parcel.SpecialNote);
            regObject.SetLink("LotState", session["Land/LotState"].Query("", "Code=?", parcel.State)[0]);
            regObject.SetLink("LotName", session["Land/LotName"].Query("", "Code=?", parcel.Name)[0]);

            #region Исполнители работ
            /*
            if (parcel.Contactor != null)
            {
                DataObject contactor = session["Rosreestr/Contractor"].AddNew();
                SetValue(contactor, "Date", parcel.Contactor.Date);
                if (parcel.Contactor.GUID_FL.Count > 0)
                    contactor.SetLink("WorkContractor", session["Rosreestr/Cadastral_Engineer"].
                        Query("", "GU_FL=?", parcel.Contactor.GUID_FL[0])[0]);
                else if (parcel.Contactor.GUID_UL.Count > 0)
                    contactor.SetLink("WorkContractor", session["Rosreestr/Cadastral_Organization"].
                        Query("", "GU_UL=?", parcel.Contactor.GUID_UL[0])[0]);
                contactor.SetLink("Lot", regObject);
            }
            */
            #endregion

            #region Координатная система
            if (parcel.EntitySpatial != null)
            {
                if (session["General/CoordSys"].Query("", "Code=?", parcel.EntitySpatial.Ent_Sys).Count > 0)
                {
                    regObject.SetLink("CoordSys", session["General/CoordSys"].
                        Query("", "Code=?", parcel.EntitySpatial.Ent_Sys)[0]);
                    coordDescr.SetLink("CoordSys", session["General/CoordSys"].
                        Query("", "Code=?", parcel.EntitySpatial.Ent_Sys)[0]);
                }
            }
            #endregion

            #region Площади участка
            if (parcel.Area != null)
            {
                DataObject lotSquare = session["Land/LotSquares"].AddNew();
                lotSquare.SetLink("AreaCode", session["General/AreaCode"].Query("", "Code=?", "002")[0]);
                lotSquare.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", parcel.Area.Unit)[0]);
                SetValue(lotSquare, "Area", parcel.Area.Area.ToString());
                SetValue(lotSquare, "Innccuracy", parcel.Area.Innccuracy.ToString());
                regObject.DeleteAllAgregates(lotSquare);
                lotSquare.SetLink("Object", regObject);
            }
            #endregion

            #region Адрес участка
            DataObject address = session["General/LotPostAddr"].AddNew();
            SetValue(address, "Codе_OKATO", parcel.Location.Address.OKATO);
            SetValue(address, "Codе_KLADR", parcel.Location.Address.KLADR);
            SetValue(address, "PostalCode", parcel.Location.Address.PostalCode);
            try
            {
                address.SetLink("Region", session["General/Province"].Query("", "Code=?", parcel.Location.Address.Region)[0]);
            }
            catch { }
            SetValue(address, "District", parcel.Location.Address.District.Type + " " + parcel.Location.Address.District.Name);
            SetValue(address, "City", parcel.Location.Address.City.Type + " " + parcel.Location.Address.City.Name);
            SetValue(address, "Urban_District", parcel.Location.Address.UrbanDistrict.Type + " " + parcel.Location.Address.UrbanDistrict.Name);
            SetValue(address, "Soviet_Village", parcel.Location.Address.SovietVillage.Type + " " + parcel.Location.Address.SovietVillage.Name);
            SetValue(address, "Locality", parcel.Location.Address.Locality.Type + " " + parcel.Location.Address.Locality.Name);
            SetValue(address, "Street", parcel.Location.Address.Street.Type + " " + parcel.Location.Address.Street.Name);
            SetValue(address, "House", parcel.Location.Address.Level1.Type + " " + parcel.Location.Address.Level1.Value);
            SetValue(address, "Corpus", parcel.Location.Address.Level2.Type + " " + parcel.Location.Address.Level2.Value);
            SetValue(address, "Building", parcel.Location.Address.Level3.Type + " " + parcel.Location.Address.Level3.Value);
            SetValue(address, "Flat", parcel.Location.Address.Apartment.Type + " " + parcel.Location.Address.Apartment.Name);
            SetValue(address, "Other", parcel.Location.Address.Other);
            SetValue(address, "Description", parcel.Location.Address.Note);
            regObject.DeleteAllAgregates(address);
            address.SetLink("Lot", regObject);
            #endregion

            #region Уточненное местоположение
            if (parcel.Location.Elaboration != null)
            {
                DataObject elaboration = session["General/Location"].AddNew();
                SetValue(elaboration, "Orientir", parcel.Location.Elaboration.ReferenceMark);
                SetValue(elaboration, "Distance", parcel.Location.Elaboration.Distance);
                SetValue(elaboration, "Direction", parcel.Location.Elaboration.Direction);
                regObject.DeleteAllAgregates(elaboration);
                elaboration.SetLink("Object", regObject);
            }
            #endregion

            #region Номера земельных участков, из которых образован данный участок
            //foreach (string cadastralNubmer in parcel.PrevCadastralNumbers)
            //{
            //    DataObject prevCadNo = session["Land/HistCadNo"].AddNew();
            //    SetValue(prevCadNo, "HistCadNo", cadastralNubmer);
            //    prevCadNo.SetLink("HistObject", regObject);
            //}
            #endregion

            #region Категория земель
            if (parcel.Category != null)
            {
                DataObject category = session["Land/LotGroundCategory"].AddNew();
                category.SetLink("GroundCategory", session["Land/GroundCategory"].Query("", "Code=?", parcel.Category)[0]);
                regObject.DeleteAllAgregates(category);
                category.SetLink("Object", regObject);
            }
            #endregion

            #region Использование участка
            if (parcel.Utilization != null)
            {
                DataObject util = session["Land/FactUsingWay"].AddNew();
                SetValue(util, "UsingWayDoc", parcel.Utilization.ByDoc);
                util.SetLink("UsingWay", session["Land/UsingWay"].Query("", "Code=?", parcel.Utilization)[0]);
                regObject.DeleteAllAgregates(util);
                util.SetLink("Object", regObject);
                SetValue(regObject, "UsingWayDoc", parcel.Utilization.ByDoc);
                SetValue(regObject, "UsingWay", 
                    session["Land/UsingWay"].Query("", "Code=?", parcel.Utilization)[0]["Name"].ToString());
            }
            #endregion

            #region Природные объекты
            if (parcel.NaturalObjects != null)
            {
                foreach (tNaturalObject obj in parcel.NaturalObjects)
                {
                    DataObject nObj = session["Land/NaturalObject"].AddNew();
                    SetValue(nObj, "TypeProtectiveForest", obj.ProtectiveForest);
                    SetValue(nObj, "TypeWaterObject", obj.WaterObject);
                    SetValue(nObj, "NameObject", obj.NameOther);
                    SetValue(nObj, "OtherChar", obj.CharOther);
                    nObj.SetLink("Name", session["NaturalObjectKind"].
                        Query("", "Code=?", obj.Kind)[0]);
                    nObj.SetLink("ForestUse", session["General/ForestUseKind"].
                        Query("", "Code=?", obj.ForestUse)[0]);
                    regObject.DeleteAllAgregates(nObj);
                    nObj.SetLink("Lot", regObject);
                }
            }
            #endregion

            #region Права
            if (parcel.Rights != null)
            {
                foreach (tRight right in parcel.Rights)
                {
                    DataObject lotRight = session["General/Law"].AddNew();
                    SetValue(lotRight, "Name", right.Name);
                    SetValue(lotRight, "Desc", right.Desc);
                    lotRight.SetLink("LawKind", session["General/LawKind"].Query("", "Code=?", right.Type)[0]);
                    SetValue(lotRight, "ShareText", right.ShareText);
                    SetValue(lotRight, "Share", right.Share.Numerator + "/" + right.Share.Denominator);
                    SetValue(lotRight, "RegistrationNo", right.Registration.RegNumber);
                    SetValue(lotRight, "RegistrationDate", (DateTime)right.Registration.RegDate);
                    #region Owners
                    if (right.Owners != null)
                    {
                        foreach (tOwner rightOwner in right.Owners)
                        {
                            DataObject rightSubject = session["General/LawSubject"].AddNew();
                            SetValue(rightSubject, "Surname", rightOwner.Person.FIO.FamilyName);
                            SetValue(rightSubject, "FirstName", rightOwner.Person.FIO.FirstName);
                            SetValue(rightSubject, "Patronymic", rightOwner.Person.FIO.Patronymic);
                            SetValue(rightSubject, "OrganizationName", rightOwner.Organization.Name);
                            SetValue(rightSubject, "GovernanceName", rightOwner.Governance.Name);
                            rightSubject.SetLink("LawID", lotRight);
                        }
                    }
                    #endregion
                    #region Documents
                    if (right.Documents != null)
                    {
                        foreach (tDocumentWithoutAppliedFile doc in right.Documents)
                        {
                            DataObject document = session["Rosreestr/Document"].AddNew();
                            SetValue(document, "Cod_Document", doc.CodeDocument);
                            SetValue(document, "Name", doc.Name);
                            SetValue(document, "Series", doc.Series);
                            SetValue(document, "Number", doc.Number);
                            SetValue(document, "Date", doc.Date);
                            SetValue(document, "IssueOrgan", doc.IssueOrgan);
                            SetValue(document, "Desc", doc.Desc);
                            document.SetLink("LawID", lotRight);
                        }
                    }
                    #endregion
                    regObject.DeleteAllAgregates(lotRight);
                    lotRight.SetLink("Object", regObject);
                }
            }
            #endregion

            #region Части участка
            if (parcel.SubParcels != null)
            {
                foreach (tSubParcel subParcel in parcel.SubParcels)
                {
                    DataObject subParc = session["Land/LotPart"].AddNew();
                    SetValue(subParc, "Nom", subParcel.NumberRecord);
                    SetValue(subParc, "Full", subParcel.Full);
                    subParc.SetLink("State", session["Lot/LotState"].Query("", "Code=?", subParcel.State)[0]);
                    if (subParcel.DateExpiry != null)
                        SetValue(subParc, "DateExpiry", subParcel.DateExpiry);
                    
                    #region Площадь
                    if (subParcel.Area != null)
                    {
                        DataObject lotSquare = session["Land/LotSquares"].AddNew();
                        lotSquare.SetLink("AreaCode", session["General/AreaCode"].Query("", "Code=?", "002")[0]);
                        lotSquare.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", subParcel.Area.Unit)[0]);
                        SetValue(lotSquare, "Area", subParcel.Area.Area.ToString());
                        //SetValue(lotSquare, "Innccuracy", subParcel.Area.Innccuracy.ToString());
                        subParc.DeleteAllAgregates(lotSquare);
                        lotSquare.SetLink("Object", subParc);
                    }
                    #endregion
                    #region Ограничения прав
                    if (subParcel.Encumbrance != null)
                    {
                        DataObject enc = session["Land/Encumbrance"].AddNew();
                        SetValue(enc, "Name", subParcel.Encumbrance.Name);
                        SetValue(enc, "CadastralNumberRestriction", subParcel.Encumbrance.CadastralNumberRestriction);
                        enc.SetLink("LawKind", session["Land/EncumbranceKind"].Query("", "Code=?", subParcel.Encumbrance.Type)[0]);
                        SetValue(enc, "AccountNumber", subParcel.Encumbrance.AccountNumber);
                        if (subParcel.Encumbrance.Duration.Started != null)
                            SetValue(enc, "Started", subParcel.Encumbrance.Duration.Started);
                        if (subParcel.Encumbrance.Duration.Stopped != null)
                            SetValue(enc, "Stopped", subParcel.Encumbrance.Duration.Stopped);
                        SetValue(enc, "AccountNumber", subParcel.Encumbrance.AccountNumber);
                        if (subParcel.Encumbrance.Registration.RegDate != null)
                            SetValue(enc, "RegDate", subParcel.Encumbrance.Registration.RegDate);
                        SetValue(enc, "RegNumber", subParcel.Encumbrance.Registration.RegNumber);
                        if (subParcel.Encumbrance.OwnerRestrictionInFavorem.Count > 0)
                        {
                            foreach (tOwner owner in subParcel.Encumbrance.OwnerRestrictionInFavorem)
                            {
                                DataObject ownerR = session["General/Owner_Restriction_InFavorem"].AddNew();
                                SetValue(ownerR, "Surname", owner.Person.FIO.FamilyName);
                                SetValue(ownerR, "FirstName", owner.Person.FIO.FirstName);
                                SetValue(ownerR, "Patronymic", owner.Person.FIO.Patronymic);
                                SetValue(ownerR, "OrganizationName", owner.Organization.Name);
                                SetValue(ownerR, "GovernanceName", owner.Governance.Name);
                                ownerR.SetLink("Encumbrance", enc);
                            }
                        }
                        #region Document
                        if (subParcel.Encumbrance.Document != null)
                        {
                            DataObject document = session["Rosreestr/Document"].AddNew();
                            SetValue(document, "Cod_Document", subParcel.Encumbrance.Document.CodeDocument);
                            SetValue(document, "Name", subParcel.Encumbrance.Document.Name);
                            SetValue(document, "Series", subParcel.Encumbrance.Document.Series);
                            SetValue(document, "Number", subParcel.Encumbrance.Document.Number);
                            SetValue(document, "Date", subParcel.Encumbrance.Document.Date);
                            SetValue(document, "IssueOrgan", subParcel.Encumbrance.Document.IssueOrgan);
                            SetValue(document, "Desc", subParcel.Encumbrance.Document.Desc);
                            document.SetLink("id", enc);
                        }
                        #endregion
                        subParc.DeleteAllAgregates(enc);
                        enc.SetLink("Object", subParc);
                    }
                    #endregion
                    
                    regObject.DeleteAllAgregates(subParc);
                    subParc.SetLink("Object", regObject);
                }
            }
            #endregion

            #region Контуры
            if (parcel.Contours != null)
            {
                foreach (tContour contour in parcel.Contours)
                {
                    DataObject cont = session["Land/LandContour"].AddNew();
                    SetValue(cont, "Number_PP", contour.NumberRecord);
                    if (contour.Area != null)
                    {
                        DataObject lotSquare = session["Land/LotSquares"].AddNew();
                        lotSquare.SetLink("AreaCode", session["General/AreaCode"].Query("", "Code=?", "002")[0]);
                        lotSquare.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", contour.Area.Unit)[0]);
                        SetValue(lotSquare, "Area", contour.Area.Area.ToString());
                        //SetValue(lotSquare, "Innccuracy", contour.Area.Innccuracy.ToString());
                        cont.DeleteAllAgregates(lotSquare);
                        lotSquare.SetLink("Object", cont);
                    }
                    regObject.DeleteAllAgregates(cont);
                    cont.SetLink("Lot", regObject);
                }
            }
            #endregion

            #region Объект входящий в состав единого землепользования
            if (parcel.CompositionEZ != null)
            {
                DataObject entry = session["Land/Object_Entry"].AddNew();
                SetValue(entry, "CadastralNumber", parcel.CompositionEZ.CadastralNumber);
                if (parcel.CompositionEZ.DateRemoved != null)
                    SetValue(entry, "DateRemoved", parcel.CompositionEZ.DateRemoved);
                entry.SetLink("LotState", session["Lot/LotState"].Query("", "Code=?", parcel.CompositionEZ.State)[0]);
                regObject.DeleteAllAgregates(entry);
                entry.SetLink("Object", regObject);
            }
            #endregion

            #region Платежи
            if (parcel.CadastralCost != null)
            {
                DataObject payment = session["Land/GroundPayment"].AddNew();
                payment.SetLink("PaymentKind", session["General/PaymentKind"].Query("", "Code=?", "016010000000")[0]);
                payment.SetLink("Unit", session["General/UnitKind"].Query("", "Code=?", parcel.CadastralCost.Unit)[0]);
                SetValue(payment, "Value", parcel.CadastralCost.Value);
                regObject.DeleteAllAgregates(payment);
                payment.SetLink("Lot", regObject);
            }
            #endregion

            #region Сведения об ограничениях прав
            if (parcel.Encumbrances != null)
            {
                foreach (tEncumbrance encumbrance in parcel.Encumbrances)
                {
                    DataObject enc = session["Land/Encumbrance"].AddNew();
                    SetValue(enc, "Name", encumbrance.Name);
                    SetValue(enc, "CadastralNumberRestriction", encumbrance.CadastralNumberRestriction);
                    enc.SetLink("LawKind", session["Land/EncumbranceKind"].Query("", "Code=?", encumbrance.Type)[0]);
                    SetValue(enc, "AccountNumber", encumbrance.AccountNumber);
                    if (encumbrance.Duration.Started != null)
                        SetValue(enc, "Started", encumbrance.Duration.Started);
                    if (encumbrance.Duration.Stopped != null)
                        SetValue(enc, "Stopped", encumbrance.Duration.Stopped);
                    SetValue(enc, "AccountNumber", encumbrance.AccountNumber);
                    if (encumbrance.Registration.RegDate != null)
                        SetValue(enc, "RegDate", encumbrance.Registration.RegDate);
                    SetValue(enc, "RegNumber", encumbrance.Registration.RegNumber);
                    if (encumbrance.OwnerRestrictionInFavorem.Count > 0)
                    {
                        foreach (tOwner owner in encumbrance.OwnerRestrictionInFavorem)
                        {
                            DataObject ownerR = session["General/Owner_Restriction_InFavorem"].AddNew();
                            SetValue(ownerR, "Surname", owner.Person.FIO.FamilyName);
                            SetValue(ownerR, "FirstName", owner.Person.FIO.FirstName);
                            SetValue(ownerR, "Patronymic", owner.Person.FIO.Patronymic);
                            SetValue(ownerR, "OrganizationName", owner.Organization.Name);
                            SetValue(ownerR, "GovernanceName", owner.Governance.Name);
                            ownerR.SetLink("Encumbrance", enc);
                        }
                    }
                    #region Document
                    if (encumbrance.Document != null)
                    {
                        DataObject document = session["Rosreestr/Document"].AddNew();
                        SetValue(document, "Cod_Document", encumbrance.Document.CodeDocument);
                        SetValue(document, "Name", encumbrance.Document.Name);
                        SetValue(document, "Series", encumbrance.Document.Series);
                        SetValue(document, "Number", encumbrance.Document.Number);
                        SetValue(document, "Date", encumbrance.Document.Date);
                        SetValue(document, "IssueOrgan", encumbrance.Document.IssueOrgan);
                        SetValue(document, "Desc", encumbrance.Document.Desc);
                        document.SetLink("id", enc);
                    }
                    #endregion
                    regObject.DeleteAllAgregates(enc);
                    enc.SetLink("Object", regObject);
                }
            }
            #endregion

            #region Правообладатели смежных участков
            if (parcel.ParcelNeighbours != null)
            {
                foreach (tParcelNeighbour ownerNeighbour in parcel.ParcelNeighbours)
                {
                    DataObject on = session["Land/Adjacency"].AddNew();
                    SetValue(on, "CadNo", ownerNeighbour.CadastralNumber);
                    regObject.DeleteAllAgregates(on);
                    on.SetLink("Lot", regObject);
                }
            }
            #endregion

            

            #region Специальные зоны
            #endregion

            session.Commit();
            return regObject;
        }

        

        private IIngeoMapObject ImportGeometry(List<tContour> xmlContours, string cadNo)
        {
            IIngeoMapObject mapObject = null;
            string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
            if (!objId.Equals(string.Empty))
            {
                mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                foreach (IIngeoShape sh in mapObject.Shapes)
                    sh.Delete();
                isModify = true;
            }
            else
                mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
            foreach (tContour xmlContour in xmlContours)
            {
                tEntitySpatial entitySpatial = xmlContour.EntitySpatial;
                foreach (tSpatialElement spatialElement in entitySpatial.SpatialElements)
                {
                    IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                    IIngeoContourPart contour = shape.Contour.Insert(-1);
                    foreach (tSpelementUnitZUOut spelementUnit in spatialElement.SpelementUnits)
                        Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                    contour.Closed = true;
                }
            }
            mapObject.MapObjects.UpdateChanges();
            return mapObject;
        }

        private IIngeoMapObject ImportGeometry(tEntitySpatial entitySpatial, string cadNo)
        {
            IIngeoMapObject mapObject = null;
            string objId = SqlHelper.IsCadNumberExist(cadNo, ingeo);
            if (!objId.Equals(string.Empty))
            {
                mapObject = ingeo.ActiveDb.MapObjects.GetObject(objId);
                foreach (IIngeoShape sh in mapObject.Shapes)
                    sh.Delete();
                isModify = true;
            }
            else
                mapObject = ingeo.ActiveDb.MapObjects.AddObject(LayerId);
            foreach (tSpatialElement spatialElement in entitySpatial.SpatialElements)
            {
                
                IIngeoShape shape = mapObject.Shapes.Insert(-1, StyleId);
                IIngeoContourPart contour = shape.Contour.Insert(-1);
                foreach (tSpelementUnitZUOut spelementUnit in spatialElement.SpelementUnits)
                    Transforms.Start(contour, spelementUnit.Ordinate.X, spelementUnit.Ordinate.Y);
                contour.Closed = true;

            }
            mapObject.MapObjects.UpdateChanges();
            return mapObject;
        }

        private void ImportSemantics(tParcel parcel, IIngeoMapObject mapObject)
        {
            import(parcel.XElement, mapObject);
            nextNode(parcel.XElement, mapObject);

            mapObject.MapObjects.UpdateChanges();
        }

        void nextNode(XElement elem, IIngeoMapObject mapObject)
        {
            foreach (XElement el in elem.Elements())
            {
                import(el, mapObject);
                nextNode(el, mapObject);
            }
        }

        void import(XElement el, IIngeoMapObject mapObject)
        {
            foreach (XAttribute attr in el.Attributes())
            {
                //System.Windows.MessageBox.Show(XmlTemplate.GetPath(el));
                XmlTemplateItem item = XmlTemplate.Template[XmlTemplate.GetPath(el), attr.Name.ToString()];
                if (item != null)
                    mapObject.SemData.SetValue(item.TableName, item.FieldName, attr.Value, 0);
            }
            if (!el.HasElements)
                if (el.Value.Length > 0)
                {
                    XmlTemplateItem item = XmlTemplate.Template[XmlTemplate.GetPath(el), "Значение узла"];
                    if (item != null)
                        mapObject.SemData.SetValue(item.TableName, item.FieldName, el.Value, 0);
                }
        }

        private void CreateLink(IIngeoMapObject mapObject, DataObject regObject)
        {
            mapObject.SemData.SetValue("InMetaLink", "RegisterNo", regObject.Id.ToString(), 0);
            mapObject.SemData.SetValue("InMetaLink", "Classifier", "Land/Lot", 0);
            mapObject.MapObjects.UpdateChanges();
        }

        private void SetValue(DataObject obj, string property, object value)
        {
            if (value == null)
                return;
            switch (value.GetType().Name)
            {
                case "Int32": obj.SetInteger(property, (int)value); break;
                case "String": obj.SetString(property, (string)value); break;
                case "Double": obj.SetDouble(property, (double)value); break;
                case "Float": obj.SetDouble(property, (double)value); break;
                case "Boolean": obj.SetBoolean(property, (bool)value); break;
                case "DateTime": obj.SetDateTime(property, (DateTime)value); break;
            }
        }
    }
}
