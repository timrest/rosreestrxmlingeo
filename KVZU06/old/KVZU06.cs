﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KVZU
{
    /// <summary>
    /// Описание структуры xml схемы кадастровой выписки земельных участков (KVZU версия 06)
    /// </summary>


    /// <summary>
    /// Кадастровая выписка о земельном участке (Корневой элемент)
    /// </summary>
    public class tKVZU
    {
        public tParcels Parcels;
        public tCertificationDoc CertificationDoc;
        public List<tContractor> Contractors;
        public Dictionary<string, tCoordSystem> CoordSystems;
    }

    public class tParcels
    {
        public tParcel Parcel;
        public tOffspringParcel OffspringParcel;
    }

    /// <summary>
    /// Сведения об удостоверении документа
    /// </summary>
    public class tCertificationDoc
    {
        public string CodeType;
        public string Version;
        public string Organization;
        public Nullable<DateTime> Date;
        public string Number;
        public string Appointment;
        public gFIO FIO = new gFIO();
        //public string FamilyName;
        //public string FirstName;
        //public string Patronymic;
    }

    /// <summary>
    /// Сведения о кадастровых инженерах
    /// </summary>
    public class tContractor
    {
        public Nullable<DateTime> Date;
        public tEngineerOut tEngineerOut= new tEngineerOut();
    }

    /// <summary>
    /// Кадастровый инженер
    /// </summary>
    
    public class tEngineerOut
    {
        //public string FamilyName;
        //public string FirstName;
        //public string Patronymic;
        public gFIO FIO= new gFIO();
        public string NCertificate;
        public string Organization;
    }

    /// <summary>
    /// Система координат
    /// </summary>
    public class tCoordSystem
    {
        public string Name;
        public string Cs_Id;
    }


    #region Сведения о земельном участке, на который выдается выписка
    /// <summary>
    /// Сведения о земельном участке, на который выдается выписка
    /// </summary>
    public class tParcel
    {
        public XElement XElement;
        public string CadastralNumber;

        public string State;
        public Nullable<DateTime> DateExpiry;
        public Nullable<DateTime> DateCreated;
        public Nullable<DateTime> DateRemoved;
        public Nullable<DateTime> DateCreatedDoc;

        public string CadastralBlock;
        public string Name;
        //public tContactor Contactor;
        public List<tOldNumber> OldCadastralNumbers;
        public List<string> PrevCadastralNumbers;
        public List<string> InnerCadastralNumbers;

        public tAreaOut Area;
        public tLocation Location;
        public string Category;
        public tUtilization Utilization;
        public List<tNaturalObject> NaturalObjects;
        public List<tRight> Rights;
        public List<tSubParcel> SubParcels;
        public tEntitySpatial EntitySpatial;
        public List<tContour> Contours;

        public tEntryParcel CompositionEZ;
        public List<tEncumbrance> Encumbrances;
        public tCadastralCost CadastralCost;

        
        //public List<string> SpecZones;
        public string SpecialNote;
        public List<tParcelNeighbour> ParcelNeighbours;
        public List<string> AllOffspringParcel;
    }

    /// <summary>
    /// Ранее присвоенные государственные учетные номера
    /// </summary>
    public class tOldNumber
    {
        public string Type;
        public string Number;
    }

    /// <summary>
    /// Площадь с округлением до 1 кв. м и погрешность определения площади
    /// </summary>
    public class tAreaOut
    {
        public double Area;
        public string Unit;
        public double Innccuracy;
    }

    /// <summary>
    /// Значение площади (с округлением до 0,01 кв. м) без погрешности определения
    /// </summary>
    public class tAreaWithoutInaccuracyOut 
    {
        public double Area;
        public string Unit;
    }
    /// <summary>
    /// Уточнение местоположения и адрес (описание местоположения) земельного участка)
    /// </summary>
    public class tLocation
    {
        public string inBounds;
        public string Placed;
        public tElaboration Elaboration;
        public tAddress Address;
    }

    /// <summary>
    /// Уточнение местоположения
    /// </summary>
    public class tElaboration
    {
        public string ReferenceMark;
        public string Distance;
        public string Direction;
    }

    /// <summary>
    /// Адрес (описание местоположения)
    /// </summary>
    public class tAddress
    {
        public string OKATO;
        public string KLADR;
        public string OKTMO;
        public string PostalCode;
        public string Region;
        public tNameType District = new tNameType();
        public tNameType City = new tNameType();
        public tNameType UrbanDistrict = new tNameType();
        public tNameType SovietVillage = new tNameType();
        public tNameType Locality = new tNameType();
        public tNameType Street = new tNameType();
        public tTypeValue Level1 = new tTypeValue();
        public tTypeValue Level2 = new tTypeValue();
        public tTypeValue Level3 = new tTypeValue();
        public tNameType Apartment = new tNameType();
        public string Other;
        public string Note;
    }

    public class tNameType
    {
        public string Name;
        public string Type;
    }

    public class tTypeValue
    {
        public string Type;
        public string Value;
    }

    //public class tCategory
    //{
    //    public string Category;
    //}

    /// <summary>
    /// Разрешенное использование
    /// </summary>
    public class tUtilization
    {
        public string Utilization;
        public string ByDoc;
    }

    /// <summary>
    /// Сведения о природном объекте
    /// </summary>
    public class tNaturalObject
    {
        public string Kind;
        public string ForestUse;
        public string ProtectiveForest;
        public string WaterObject;
        public string NameOther;
        public string CharOther;
    }

    /// <summary>
    /// Право
    /// </summary>

    public class tRight
    {
        public string Name;
        public string Type;
        public List<tOwner> Owners;
        public tShare Share;
        public string ShareText;
        public string Desc;
        public tRegistration Registration;
        public List<tDocumentWithoutAppliedFile> Documents;
    }

    /// <summary>
    /// Номер и дата государственной регистрации (согласно записи ЕГРП)
    /// </summary>
    public class tRegistration
    {
        public string RegNumber;
        public Nullable<DateTime> RegDate;
    }

    /// <summary>
    /// Обладатели прав, ограничений (обременений) прав)
    /// </summary>
    public class tOwner
    {
        //public tPerson Person = new tPerson();
        public Person Person;
        public tNameOwner Organization;
        public tNameOwner Governance;
        public tContactOwner ContactOwner;
    }

    /// <summary>
    /// Связь с правообладателем
    /// </summary>
    public class tContactOwner
    {
        public string ContactAddress;
        public string Email;
    }
    /// <summary>
    /// Физическое лицо
    /// </summary>
    public class Person
    {
        public gFIO FIO = new gFIO();
    }

    public class gFIO
    {
        public string FamilyName;
        public string FirstName;
        public string Patronymic;
    }

    /// <summary>
    /// Наименование правообладателя (юридического лица, субъекта публичного права)
    /// </summary>
    public class tNameOwner
    {
        public string Name;
    }

    /// <summary>
    /// Доля в праве
    /// </summary>
    public class tShare
    {
        public int Numerator;
        public int Denominator;
    }

    /// <summary>
    /// Сведения о частях участка
    /// </summary>
    public class tSubParcel
    {
        public int? NumberRecord;
        public bool Full;
        public string State;
        public Nullable<DateTime> DateExpiry;

        public tAreaWithoutInaccuracyOut Area = new tAreaWithoutInaccuracyOut();
        public tEncumbrance Encumbrance;
        public tEntitySpatial EntitySpatial;
    }



    /// <summary>
    /// Контуры многоконтурного участка
    /// </summary>
    public class tContour
    {
        public string NumberRecord;

        public tAreaWithoutInaccuracyOut Area = new tAreaWithoutInaccuracyOut();
        public tEntitySpatial EntitySpatial;
    }

    /// <summary>
    /// Ограничение (обременение) права
    /// </summary>
    public class tEncumbrance
    {
        public string Name;
        public string Type;
        public string AccountNumber;
        public string CadastralNumberRestriction;
        public List<tOwner> OwnerRestrictionInFavorem;
        public tDuration Duration;
        public tRegistration Registration;
        public tDocumentWithoutAppliedFile Document;
    }

    /// <summary>
    /// Срок действия (продолжительность)
    /// </summary>
    public class tDuration
    {
        public Nullable<DateTime> Started;
        public Nullable<DateTime> Stopped;
        public string Term;
    }

    /// <summary>
    /// Реквизиты документа
    /// </summary>
    public class tDocumentWithoutAppliedFile
    {
        public string CodeDocument;
        public string Name;
        public string Series;
        public string Number;
        public Nullable<DateTime> Date;
        public string IssueOrgan;
        public string Desc;
    }

    /// <summary>
    /// Сведения о величине кадастровой стоимости
    /// </summary>
    public class tCadastralCost
    {
        public double Value;
        public string Unit;
    }

    /// <summary>
    /// Описание местоположения границ
    /// </summary>
    #region public class tEntitySpatial
    public class tEntitySpatial
    {
        public string Ent_Sys;
        public List<tSpatialElement> SpatialElements;
        public List<tBorder> Borders;
    }

    /// <summary>
    /// Элемент контура
    /// </summary>
    public class tSpatialElement
    {
        public List<tSpelementUnitZUOut> SpelementUnits;
    }
    /// <summary>
    /// Часть элемента (Точка)
    /// </summary>
    public class tSpelementUnitZUOut
    {
        public string Type_Unit;
        public int Su_Nmb;
        public Ordinate Ordinate;
    }
    /// <summary>
    /// Координата
    /// </summary>
    public class Ordinate
    {
        //public tOrdinateOut tOrdinateOut;
        public string GeopointZacrep;
        public double X;
        public double Y;
        public int Ord_Nmb;
        public int Num_Geopoint;
        public double Delta_Geopoint;
    }
    ///// <summary>
    ///// (Координата)
    ///// </summary>
    //public class tOrdinateOut
    //{
    //    public double X;
    //    public double Y;
    //    public int Ord_Nmb;
    //    public int Num_Geopoint;
    //    public double Delta_Geopoint;
    //}
    /// <summary>
    /// Описание частей границ
    /// </summary>
    public class tBorder
    {
        public double Spatial;
        public double Point1;
        public double Point2;
    }
    /// <summary>
    /// Ребро
    /// </summary>
    public class Edge
    {
        public tLength tLength;
        public tDirectionAngle DirectionAngle;
        public tCadastralNumbers Neighbours; 
    }
    /// <summary>
    /// Горизонтальное проложение в метрах
    /// </summary>
    public class tLength
    {
        public double Length;
        public string Definition;
    }
    /// <summary>
    ///Дирекционный угол
    /// </summary>
    public class tDirectionAngle
    {
        public int Degree;
        public int Minute;
    }

    public class tCadastralNumbers
    {
        public string CadastralNumber;
    }

    #endregion

    /// <summary>
    /// Сведения о смежном земельном участке
    /// </summary>
    #region public class tParcelNeighbour
    public class tParcelNeighbour
    {
        public string CadastralNumber;
        public OwnerNeighbour OwnerNeighbour;
    }

    /// <summary>
    /// Связь с правообладателем
    /// </summary>
    public class OwnerNeighbour
    {
        public string ContactAddress;
        public string Email;
    }
    #endregion

    /// <summary>
    /// Обособленный или условный участок, входящий в состав единого землепользования
    /// </summary>
    public class tEntryParcel
    {
        public string CadastralNumber;
        public string State;
        public Nullable<DateTime> DateRemoved;
        public tEntitySpatial EntitySpatial;
        public tAreaWithoutInaccuracyOut Area;
    }

    #endregion

    /// <summary>
    /// Участки-потомки ЗУ
    /// </summary>
    public class tOffspringParcel
    {
        public string CadastralNumber;
        public tEntitySpatial EntitySpatial;
    }
}
