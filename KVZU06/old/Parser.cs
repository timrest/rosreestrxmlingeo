﻿using Integro.InMeta.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace KVZU
{

    public class Parser
    {
        private XmlDocument doc = new XmlDocument();

        public Parser(string fileName)
        {
            doc.Load(fileName);
        }

        /// <summary>
        /// Заполняем поля объекта данными из XML элемента
        /// </summary>
        /// <param name="obj">Объект - приемник</param>
        /// <param name="element">XML элемент - источник</param>
        private void ParseElement(object obj, XmlElement element)
        {
            if (element == null)
                return;
            foreach (XmlAttribute attr in element.Attributes)
            {
                FieldInfo field = obj.GetType().GetField(attr.Name);
                if (field == null)
                    continue;
                switch (field.FieldType.Name)
                {
                    case "String":
                        field.SetValue(obj, attr.Value);
                        break;
                    case "Int32":
                        field.SetValue(obj, Int32.Parse(attr.Value));
                        break;
                    case "Double":
                        field.SetValue(obj, Double.Parse(attr.Value.Replace('.', ',')));
                        break;
                    case "Nullable`1":
                        if (field.FieldType.FullName.Contains("DateTime"))
                            field.SetValue(obj, DateTime.Parse(attr.Value));
                        break;
                }

            }
            foreach (XmlElement el in element.ChildNodes)
            {
                FieldInfo field = obj.GetType().GetField(el.Name);
                if (field == null)
                    continue;
                switch (field.FieldType.Name)
                {
                    case "String":
                        field.SetValue(obj, el.InnerText);
                        break;
                    case "Int32":
                        field.SetValue(obj, Int32.Parse(el.InnerText));
                        break;
                    case "Double":
                        field.SetValue(obj, Double.Parse(el.InnerText.Replace('.', ',')));
                        break;
                    case "Nullable`1":
                        if (field.FieldType.FullName.Contains("DateTime"))
                            field.SetValue(obj, DateTime.Parse(el.InnerText));
                        break;
                }
            }
        }

        /// <summary>
        /// Получение полного кадастрового номера
        /// </summary>
        /// <param name="xParcel">Узел Parcel</param>
        /// <returns>Полный кадастровый номер</returns>
        private string GetCadastralNumber(XmlElement xParcel)
        {
            if (!Regex.IsMatch(xParcel.Attributes["CadastralNumber"].Value, "[0-9]+:[0-9]+:[0-9]+:[0-9]+"))
            {
                return xParcel.ParentNode.ParentNode.Attributes["CadastralNumber"].Value +
                    xParcel.Attributes["CadastralNumber"].Value;
            }
            else
            {
                return xParcel.Attributes["CadastralNumber"].Value;
            }
        }


        public tKVZU ParseDocument()
        {
            tKVZU cv = new tKVZU();
            XmlElement root = doc["KVZU"];
            ParseElement(cv, root);
            //cv.Package = GetPackage(root["Package"]);
            
            cv.Parcels = GettParcels(root["Parcels"]);
            cv.CertificationDoc = GetCertificationDoc(root["CertificationDoc"]);
            cv.Contractors = GetContractors(root["Contractors"]);
            cv.CoordSystems = GetCoordSystems(root["CoordSystems"]);
            return cv;
        }

        private tParcels GettParcels(XmlElement xParcels)
        {
            tParcels parcels = new tParcels();
            parcels.Parcel = GetParcel(xParcels["Parcel"]);
            try
            {
                parcels.OffspringParcel = GetOffspringParcel(xParcels["OffspringParcel"]);
            }
            catch { }
            return parcels;
        }

        private List<tContractor> GetContractors(XmlElement xContractors)
        {
            if (xContractors == null)
                return null;
            List<tContractor> contractors = new List<tContractor>();
            foreach (XmlElement xContractor in xContractors.GetElementsByTagName("Contractor"))
            {
                tContractor contractor = new tContractor();
                ParseElement(contractor, xContractor);
                
                contractors.Add(contractor);
            }
            return contractors;
        }
        #region Parcel
        private tParcel GetParcel(XmlElement xParcel)
        {
            //List<tParcel> parcels = new List<tParcel>();
            //foreach (XmlElement xParcel in xParcels.GetElementsByTagName("Parcel"))
            //{
            tParcel parcel = new tParcel();
            parcel.XElement = System.Xml.Linq.XElement.Load(xParcel.CreateNavigator().ReadSubtree());
            ParseElement(parcel, xParcel);
            if (xParcel.Attributes["CadastralNumber"] != null)
                parcel.CadastralNumber = GetCadastralNumber(xParcel);
            parcel.OldCadastralNumbers = GetOldCadastralNumbers(xParcel["OldNumbers"]); ;
            parcel.PrevCadastralNumbers = GetPrevCadastralNumbers(xParcel["PrevCadastralNumbers"]);
            parcel.InnerCadastralNumbers = GetInnerCadastralNumbers(xParcel["InnerCadastralNumbers"]);
            parcel.Area = GetAreaOut(xParcel["Area"]);
            parcel.Location = GetLocation(xParcel["Location"]);
            //parcel.Category = GetCategory(xParcel["Category"]);
            parcel.Utilization = GetUtilization(xParcel["Utilization"]);
            parcel.NaturalObjects = GetNaturalObjects(xParcel["NaturalObjects"]);
            parcel.Rights = GetRights(xParcel["Rights"]);
            parcel.SubParcels = GetSubParcels(xParcel["SubParcels"]);
            parcel.EntitySpatial = GetEntitySpatial(xParcel["EntitySpatial"]);
            parcel.Contours = GetContours(xParcel["Contours"]);
            parcel.CompositionEZ = GetEntryParcel(xParcel["CompositionEZ"]);
            parcel.Encumbrances = GetEncumbrances(xParcel["Encumbrances"]);
            parcel.CadastralCost = GetCadastralCost(xParcel["CadastralCost"]);
            parcel.ParcelNeighbours = GetParcelNeighbours(xParcel["OwnerNeighbours"]);
            parcel.AllOffspringParcel = GetAllOffspringParcel(xParcel["AllOffspringParcel"]);
            //parcels.Add(parcel);
            //}
            return parcel;
        }

        private List<tOldNumber> GetOldCadastralNumbers(XmlElement xOldNumbers)
        {
            if (xOldNumbers == null)
                return null;
            List<tOldNumber> oldNumbers = new List<tOldNumber>();
            foreach (XmlElement xOldNumber in xOldNumbers.GetElementsByTagName("OldNumber"))
            {
                tOldNumber oldNumber = new tOldNumber();
                ParseElement(oldNumber, xOldNumber);
                oldNumbers.Add(oldNumber);
            }
            return oldNumbers;
        }


        private List<string> GetPrevCadastralNumbers(XmlElement xPrevCadastrNumbers)
        {
            List<string> prevCadastralNumbers = new List<string>();
            if (xPrevCadastrNumbers == null)
                return prevCadastralNumbers;
            foreach (XmlElement xPrevCadastralNumber in xPrevCadastrNumbers.GetElementsByTagName("CadastralNumber"))
            {
                prevCadastralNumbers.Add(xPrevCadastralNumber.InnerText);
            }
            return prevCadastralNumbers;
        }

        private List<string> GetInnerCadastralNumbers(XmlElement xInnerCadastrNumbers)
        {
            List<string> innerCadastralNumbers = new List<string>();
            if (xInnerCadastrNumbers == null)
                return innerCadastralNumbers;
            foreach (XmlElement xInnerCadastrNumber in xInnerCadastrNumbers.GetElementsByTagName("CadastralNumber"))
            {
                innerCadastralNumbers.Add(xInnerCadastrNumber.InnerText);
            }
            return innerCadastralNumbers;
        }

        private List<string> GetAllOffspringParcel(XmlElement xAllOffspringParcels)
        {
            List<string> allOffspringParcel = new List<string>();
            if (xAllOffspringParcels == null)
                return allOffspringParcel;
            foreach (XmlElement xAllOffspringParcel in xAllOffspringParcels.GetElementsByTagName("CadastralNumber"))
            {
                allOffspringParcel.Add(xAllOffspringParcel.InnerText);
            }
            return allOffspringParcel;
        }

        private tAreaOut GetAreaOut(XmlElement xAreaOut)
        {
            tAreaOut area = new tAreaOut();
            ParseElement(area, xAreaOut);
            return area;
        }

        private tAreaWithoutInaccuracyOut GetAreaWithoutInaccuracyOut(XmlElement xAreaWithoutInaccuracyOut)
        {
            tAreaWithoutInaccuracyOut area = new tAreaWithoutInaccuracyOut();
            ParseElement(area, xAreaWithoutInaccuracyOut);
            return area;
        }

        #region Location
        private tLocation GetLocation(XmlElement xLocation)
        {
            tLocation location = new tLocation();
            ParseElement(location, xLocation);
            location.Elaboration = GetEloboration(xLocation["Elaboration"]);
            location.Address = GetAddress(xLocation["Address"]);
            return location;
        }

        private tElaboration GetEloboration(XmlElement xElaboration)
        {
            if (xElaboration == null)
                return null;
            tElaboration elaboration = new tElaboration();
            ParseElement(elaboration, xElaboration);
            return elaboration;
        }

        private tAddress GetAddress(XmlElement xAddress)
        {
            if (xAddress == null)
                return null;
            tAddress address = new tAddress();
            ParseElement(address, xAddress);
            ParseElement(address.District, xAddress["District"]);
            ParseElement(address.City, xAddress["City"]);
            ParseElement(address.UrbanDistrict, xAddress["Urban_District"]);
            ParseElement(address.SovietVillage, xAddress["Soviet_Vilage"]);
            ParseElement(address.Locality, xAddress["Locality"]);
            ParseElement(address.Street, xAddress["Street"]);
            ParseElement(address.Level1, xAddress["Level1"]);
            ParseElement(address.Level2, xAddress["Level2"]);
            ParseElement(address.Level3, xAddress["Level3"]);
            ParseElement(address.Apartment, xAddress["Apartment"]);
            return address;
        }
        #endregion

        //private tCategory GetCategory(XmlElement xCategory)
        //{
        //    if (xCategory == null)
        //        return null;
        //    tCategory category = new tCategory();
        //    ParseElement(category, xCategory);
        //    return category;
        //}

        private tUtilization GetUtilization(XmlElement xUtilization)
        {
            tUtilization utilization = new tUtilization();
            ParseElement(utilization, xUtilization);
            return utilization;
        }

        private List<tNaturalObject> GetNaturalObjects(XmlElement xNaturalObjects)
        {
            if (xNaturalObjects == null)
                return null;
            List<tNaturalObject> naturalObjects = new List<tNaturalObject>();
            foreach (XmlElement xNaturalObject in xNaturalObjects.GetElementsByTagName("NaturalObject"))
            {
                tNaturalObject naturalObject = new tNaturalObject();
                ParseElement(naturalObject, xNaturalObject);
                naturalObjects.Add(naturalObject);
            }
            return naturalObjects;
        }

        #region Rights
        private List<tRight> GetRights(XmlElement xRights)
        {
            if (xRights == null)
                return null;
            List<tRight> rights = new List<tRight>();
            foreach (XmlElement xRight in xRights.GetElementsByTagName("Right"))
            {
                tRight right = new tRight();
                ParseElement(right, xRight);
                right.Share = GetRightShare(xRight["Share"]);
                right.Owners = GetOwners(xRight["Owners"]);
                right.Registration = GetRegistration(xRight["Registration"]);
                right.Documents = GetDocuments(xRight["Documents"]);
                rights.Add(right);
            }
            return rights;
        }

        private tShare GetRightShare(XmlElement xShare)
        {
            if (xShare == null)
                return null;
            tShare share = new tShare();
            ParseElement(share, xShare);
            return share;
        }

        private List<tOwner> GetOwners(XmlElement xOwners)
        {
            List<tOwner> owners = new List<tOwner>();
            foreach (XmlElement xOwner in xOwners.GetElementsByTagName("Owner"))
            {
                tOwner owner = new tOwner();
                if (xOwner["Person"] != null && xOwner["Person"]["FIO"] != null)
                {
                    ParseElement(owner.Person.FIO.FamilyName, xOwner["Person"]["gFIO"]["FamilyName"]);
                    ParseElement(owner.Person.FIO.FirstName, xOwner["Person"]["gFIO"]["FirstName"]);
                    ParseElement(owner.Person.FIO.Patronymic, xOwner["Person"]["gFIO"]["Patronymic"]);
                }
                if (xOwner["Organization"] != null)
                    ParseElement(owner.Organization, xOwner["Organization"]);
                if (xOwner["Governance"] != null)
                    ParseElement(owner.Governance, xOwner["Governance"]);

                //tContactOwner contactOwner = new tContactOwner();
                ParseElement(owner.ContactOwner, xOwner["ContactOwner"]);
                owners.Add(owner);
            }
            return owners;
        }
        private tRegistration GetRegistration(XmlElement xRegistration)
        {
            if (xRegistration == null)
                return null;
            tRegistration registration = new tRegistration();
            ParseElement(registration, xRegistration);
            return registration;
        }
        private List<tDocumentWithoutAppliedFile> GetDocuments(XmlElement xDocuments)
        {
            if (xDocuments == null)
                return null;
            List<tDocumentWithoutAppliedFile> documents = new List<tDocumentWithoutAppliedFile>();
            foreach (XmlElement xDocument in xDocuments.GetElementsByTagName("Document"))
            {
                tDocumentWithoutAppliedFile document = new tDocumentWithoutAppliedFile();
                ParseElement(document, xDocument);
                //ParseElement(document.Duration, xDocument["Duration"]);
                //document.Images = GetImages(xDocument["Images"]);
                documents.Add(document);
            }
            return documents;
        }

        //private List<tImage> GetImages(XmlElement xImages)
        //{
        //    List<tImage> images = new List<tImage>();
        //    foreach (XmlElement xImage in xImages.GetElementsByTagName("Image"))
        //    {
        //        tImage image = new tImage();
        //        ParseElement(image, xImage);
        //        images.Add(image);
        //    }
        //    return images;
        //}
        #endregion

        private List<tSubParcel> GetSubParcels(XmlElement xSubParcels)
        {
            if (xSubParcels == null)
                return null;
            List<tSubParcel> subParcels = new List<tSubParcel>();
            foreach (XmlElement xSubParcel in xSubParcels.GetElementsByTagName("SubParcel"))
            {
                tSubParcel subParcel = new tSubParcel();
                ParseElement(subParcel, xSubParcel);
                subParcel.Area = GetAreaWithoutInaccuracyOut(xSubParcel["Area"]);
                if (xSubParcel["Encumbrance"] != null)
                {
                    subParcel.Encumbrance = new tEncumbrance();
                    subParcel.Encumbrance = GetEncumbrance(xSubParcel["Encumbrance"]);
                }
                //if (xSubParcel["Object_Entry"] != null)
                //{
                //    subParcel.ObjectEntry = new tObjectEntry();
                //    ParseElement(subParcel.ObjectEntry, xSubParcel["Object_Entry"]);
                //}
                if (xSubParcel["EntitySpatial"] != null)
                {
                    subParcel.EntitySpatial = new tEntitySpatial();
                    ParseElement(subParcel.EntitySpatial, xSubParcel["EntitySpatial"]);
                }
                subParcels.Add(subParcel);
            }
            return subParcels;
        }

        private List<tContour> GetContours(XmlElement xContours)
        {
            if (xContours == null)
                return null;
            List<tContour> contours = new List<tContour>();
            foreach (XmlElement xContour in xContours.GetElementsByTagName("Contour"))
            {
                tContour contour = new tContour();
                ParseElement(contour, xContour);
                contour.Area = GetAreaWithoutInaccuracyOut(xContour["Area"]);
                if (xContour["EntitySpatial"] != null)
                    contour.EntitySpatial = GetEntitySpatial(xContour["EntitySpatial"]);
                contours.Add(contour);
            }
            return contours;
        }

        #region Encumbrances
        private List<tEncumbrance> GetEncumbrances(XmlElement xEncumbrances)
        {
            if (xEncumbrances == null)
                return null;
            List<tEncumbrance> encumbrances = new List<tEncumbrance>();
            foreach (XmlElement xEncumbrance in xEncumbrances.GetElementsByTagName("Encumbrance"))
            {
                tEncumbrance encumbrance = GetEncumbrance(xEncumbrance);
                encumbrances.Add(encumbrance);
            }
            return encumbrances;
        }

        private tEncumbrance GetEncumbrance(XmlElement xEncumbrance)
        {
            if (xEncumbrance == null)
                return null;
            tEncumbrance encumbrance = new tEncumbrance();
            ParseElement(encumbrance, xEncumbrance);
            encumbrance.Registration = GetRegistration(xEncumbrance["Registration"]);
            encumbrance.Duration = GetDuration(xEncumbrance["Duration"]);
            encumbrance.Document = GetDocument(xEncumbrance["Document"]);
            encumbrance.OwnerRestrictionInFavorem = GetOwnerRestrictions(xEncumbrance);
            return encumbrance;
        }

        private tDocumentWithoutAppliedFile GetDocument(XmlElement xDocument)
        {
            if (xDocument == null)
                return null;

            tDocumentWithoutAppliedFile document = new tDocumentWithoutAppliedFile();
            ParseElement(document, xDocument);

            return document;
        }

        private tDuration GetDuration(XmlElement xDuration)
        {
            if (xDuration == null)
                return null;
            tDuration duration = new tDuration();
            ParseElement(duration, xDuration);
            return duration;
        }

        private List<tOwner> GetOwnerRestrictions(XmlElement xEncumbrance)
        {
            List<tOwner> ownerRestrictions = new List<tOwner>();
            foreach (XmlElement xOwnerRestriction in xEncumbrance.GetElementsByTagName("OwnerRestrictionInFavorem"))
            {
                tOwner owner = new tOwner();
                if (xOwnerRestriction["Person"] != null && xOwnerRestriction["Person"]["FIO"] != null)
                {
                    ParseElement(owner.Person.FIO.FamilyName, xOwnerRestriction["Person"]["gFIO"]["FamilyName"]);
                    ParseElement(owner.Person.FIO.FirstName, xOwnerRestriction["Person"]["gFIO"]["FirstName"]);
                    ParseElement(owner.Person.FIO.Patronymic, xOwnerRestriction["Person"]["gFIO"]["Patronymic"]);
                }
                if (xOwnerRestriction["Organization"] != null)
                    ParseElement(owner.Organization, xOwnerRestriction["Organization"]);
                if (xOwnerRestriction["Governance"] != null)
                    ParseElement(owner.Governance, xOwnerRestriction["Governance"]);

                //tContactOwner contactOwner = new tContactOwner();
                ParseElement(owner.ContactOwner, xOwnerRestriction["ContactOwner"]);
                ownerRestrictions.Add(owner);
            }
            return ownerRestrictions;
        }
        #endregion

        private tCadastralCost GetCadastralCost(XmlElement xCadastralCost)
        {
            tCadastralCost cost = new tCadastralCost();
            ParseElement(cost, xCadastralCost);
            return cost;
        }

        #region Entity_Spatial
        private tEntitySpatial GetEntitySpatial(XmlElement xEntitySpatial)
        {
            tEntitySpatial entitySpatial = new tEntitySpatial();
            if (xEntitySpatial == null)
                return null;
            ParseElement(entitySpatial, xEntitySpatial);
            entitySpatial.SpatialElements = GetSpatialElements(xEntitySpatial.GetElementsByTagName("ns3:SpatialElement"));
            //entitySpatial.Borders = ;
            return entitySpatial;
        }

        private List<tSpatialElement> GetSpatialElements(XmlNodeList xSpatialElements)
        {
            List<tSpatialElement> spatialElements = new List<tSpatialElement>();
            foreach (XmlElement xSpatialElement in xSpatialElements)
            {
                tSpatialElement spatialElement = new tSpatialElement();
                spatialElement.SpelementUnits = GetSpelementUnits(xSpatialElement.GetElementsByTagName("ns3:SpelementUnit"));
                spatialElements.Add(spatialElement);
            }
            return spatialElements;
        }

        private List<tSpelementUnitZUOut> GetSpelementUnits(XmlNodeList xSpelementUnits)
        {
            List<tSpelementUnitZUOut> spelementUnits = new List<tSpelementUnitZUOut>();
            foreach (XmlElement xSpelementUnit in xSpelementUnits)
            {
                tSpelementUnitZUOut spelementUnit = new tSpelementUnitZUOut();
                ParseElement(spelementUnit, xSpelementUnit);
                spelementUnit.Ordinate = GetOrdinate(xSpelementUnit["ns3:Ordinate"]);
                spelementUnits.Add(spelementUnit);
            }
            return spelementUnits;
        }

        private Ordinate GetOrdinate(XmlElement xOrdinate)
        {
            Ordinate ordinate = new Ordinate();
            ParseElement(ordinate, xOrdinate);
            //ordinate.tOrdinateOut = GetOrdinateOut(xOrdinate["Ordinate"]);
            return ordinate;
        }
        //private tOrdinateOut GetOrdinateOut(XmlElement xOrdinateOut)
        //{
        //    tOrdinateOut OrdinateOut = new tOrdinateOut();
        //    ParseElement(OrdinateOut, xOrdinateOut);
        //    return OrdinateOut;
        //}
        #endregion

        private tEntryParcel GetEntryParcel(XmlElement xEntryParcel)
        {
            tEntryParcel entryParcel = new tEntryParcel();
            if (xEntryParcel != null)
            {
                ParseElement(entryParcel, xEntryParcel);
                entryParcel.Area = GetAreaWithoutInaccuracyOut(xEntryParcel["Area"]);
                if (xEntryParcel["EntitySpatial"] != null)
                {
                    entryParcel.EntitySpatial = new tEntitySpatial();
                    ParseElement(entryParcel.EntitySpatial, xEntryParcel["EntitySpatial"]);
                }
            }
            return entryParcel;
        }

        #region ParcelNeighbours

        private List<tParcelNeighbour> GetParcelNeighbours(XmlElement xParcelNeighbours)
        {
            List<tParcelNeighbour> parcelNeighbours = new List<tParcelNeighbour>();
            if (xParcelNeighbours != null)
            {
                foreach (XmlElement xParcelNeighbour in xParcelNeighbours.GetElementsByTagName("ParcelNeighbour"))
                {
                    tParcelNeighbour parcelneighbour = new tParcelNeighbour();
                    ParseElement(parcelneighbour, xParcelNeighbour);
                    parcelneighbour.OwnerNeighbour = GetOwnerNeighbour(xParcelNeighbour["OwnerNeighbour"]);
                    parcelNeighbours.Add(parcelneighbour);
                }
            }
            return parcelNeighbours;
        }

        private OwnerNeighbour GetOwnerNeighbour(XmlElement xOwnerNeighbour)
        {
            OwnerNeighbour ownerNeighbour = new OwnerNeighbour();
            ParseElement(ownerNeighbour, xOwnerNeighbour);
            return ownerNeighbour;
        }

        //private List<tOwnerNeighbour> GetOwnerNeighbours(XmlElement xOwnerNeighbours)
        //{
        //    if (xOwnerNeighbours == null)
        //        return null;
        //    List<tOwnerNeighbour> ownerNeighbours = new List<tOwnerNeighbour>();
        //    foreach (XmlElement xOwnerNeighbour in xOwnerNeighbours.GetElementsByTagName("OwnerNeighbour"))
        //    {
        //        tOwnerNeighbour ownerNeighbour = new tOwnerNeighbour();
        //        ParseElement(ownerNeighbours, xOwnerNeighbour);
        //        //ownerNeighbour.Neighbours = GetNeighbours(xOwnerNeighbour);
        //        ownerNeighbours.Add(ownerNeighbour);
        //    }
        //    return ownerNeighbours;
        //}

        
        #endregion

        //private List<string> GetSpecZones(XmlElement xSpecZones)
        //{
        //    if (xSpecZones == null)
        //        return null;
        //    List<string> specZones = new List<string>();
        //    foreach (XmlElement xSpecZone in xSpecZones.GetElementsByTagName("SpecZone"))
        //    {
        //        specZones.Add(xSpecZone.InnerText);
        //    }
        //    return specZones;
        //}
        #endregion

        //private tEngineerOut GetEngineerOut()
        //{

        //}

        private tOffspringParcel GetOffspringParcel(XmlElement xOffspringParcel)
        {
            //List<tOffspringParcel> offspringParcel = new List<tOffspringParcel>();
            //foreach (XmlElement xOffspringParcel in xParcels.GetElementsByTagName("OffspringParcel"))
            //{
            
                tOffspringParcel offspringParcel = new tOffspringParcel();
                ParseElement(offspringParcel, xOffspringParcel);
                offspringParcel.EntitySpatial = GetEntitySpatial(xOffspringParcel["EntitySpatial"]);
            
                //offspringParcels.Add(offspringParcel);
            //}
            return offspringParcel;
        }

        private tCertificationDoc GetCertificationDoc(XmlElement xCertDoc)
        {
            tCertificationDoc certDoc = new tCertificationDoc();
            ParseElement(certDoc, xCertDoc);
            return certDoc;
        }

        private Dictionary<string, tCoordSystem> GetCoordSystems(XmlElement xCoordSystems)
        {
            Dictionary<string, tCoordSystem> coordSystems = new Dictionary<string, tCoordSystem>();
            foreach (XmlElement xCoordSystem in xCoordSystems.GetElementsByTagName("CoordSystem"))
            {
                tCoordSystem coordSystem = new tCoordSystem();
                ParseElement(coordSystem, xCoordSystem);
                coordSystems.Add(coordSystem.Cs_Id, coordSystem);
            }
            return coordSystems;
        }
    }
}
