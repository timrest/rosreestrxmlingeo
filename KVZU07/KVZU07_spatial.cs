﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KVZU07_spatial
{
    /// <summary>
    /// Описание структуры xml схемы кадастровой выписки земельных участков (KVZU версия 06) касаемо графики
    /// </summary>


     //<summary>
     //Кадастровая выписка о земельном участке (Корневой элемент)
     //</summary>
    public class tKVZU
    {
        public tParcels Parcels;
    }

    public class tParcels
    {
        public tParcelSpatial Parcel;
    }

    


    #region Сведения о земельном участке, на который выдается выписка
    /// <summary>
    /// Сведения о земельном участке, на который выдается выписка
    /// </summary>
    public partial class tParcelSpatial
    {
        public XElement XElement;
        public string CadastralNumber;
        public tEntitySpatial EntitySpatial;
        public List<tContour> Contours;
        public tCompositionEZ CompositionEZ;
        public List<tSubParcel> SubParcels;
    }


    /// <summary>
    /// Контуры многоконтурного участка
    /// </summary>
    public class tContour
    {
        public tEntitySpatial EntitySpatial;
    }
    /// <summary>
    /// Обособленный или условный участок, входящий в состав единого землепользования
    /// </summary>
    public class tCompositionEZ
    {
        public tEntryParcel EntryParcel;
    }

    public class tEntryParcel
    {
        public string CadastralNumber;
        public tEntitySpatial EntitySpatial;
    }

    /// <summary>
    /// Сведения о частях участка
    /// </summary>
    public class tSubParcel
    {
        public string NumberRecord;
        public tEntitySpatial EntitySpatial;
    }
    /// <summary>
    /// Описание местоположения границ
    /// </summary>
    #region public class tEntitySpatial
    public class tEntitySpatial
    {
        public string Ent_Sys;
        public List<tSpatialElement> SpatialElements;
        //public List<tBorder> Borders;
    }

    /// <summary>
    /// Элемент контура
    /// </summary>
    public class tSpatialElement
    {
        public List<tSpelementUnitZUOut> SpelementUnits;
    }
    /// <summary>
    /// Часть элемента (Точка)
    /// </summary>
    public class tSpelementUnitZUOut
    {
        public string Type_Unit;
        public int Su_Nmb;
        public Ordinate Ordinate;
    }
    /// <summary>
    /// Координата
    /// </summary>
    public class Ordinate
    {
        //public tOrdinateOut tOrdinateOut;
        public string GeopointZacrep;
        public double X;
        public double Y;
        public int Ord_Nmb;
        public int Num_Geopoint;
        public double Delta_Geopoint;
    }
    
    ///// <summary>
    ///// Описание частей границ
    ///// </summary>
    //public class tBorder
    //{
    //    public double Spatial;
    //    public double Point1;
    //    public double Point2;
    //}
    ///// <summary>
    ///// Ребро
    ///// </summary>
    //public class Edge
    //{
    //    public tLength tLength;
    //    public tDirectionAngle DirectionAngle;
    //    public tCadastralNumbers Neighbours; 
    //}
    ///// <summary>
    ///// Горизонтальное проложение в метрах
    ///// </summary>
    //public class tLength
    //{
    //    public double Length;
    //    public string Definition;
    //}
    ///// <summary>
    /////Дирекционный угол
    ///// </summary>
    //public class tDirectionAngle
    //{
    //    public int Degree;
    //    public int Minute;
    //}

    #endregion




    #endregion
}
