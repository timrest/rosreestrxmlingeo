﻿namespace RosreestrXMLIngeo
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.lotSettingstabPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lotUsingByDocFieldCB = new System.Windows.Forms.ComboBox();
            this.lotUsingFieldCB = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lotLandCatFieldCB = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lotUsingByDocTableCB = new System.Windows.Forms.ComboBox();
            this.lotAreaFieldCB = new System.Windows.Forms.ComboBox();
            this.lotUsingTableCB = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lotLandCatTableCB = new System.Windows.Forms.ComboBox();
            this.lotAddressFieldCB = new System.Windows.Forms.ComboBox();
            this.lotAreaTableCB = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lotAddressTableCB = new System.Windows.Forms.ComboBox();
            this.lotCadNoFieldCB = new System.Windows.Forms.ComboBox();
            this.lotCadNoTableCB = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lotStyleSingCB = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lotStyleCB = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lotLayerCB = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lotMapCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cadBlockSettingstabPage = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cadBlockAreaFieldCB = new System.Windows.Forms.ComboBox();
            this.cadBlockAreaTableCB = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cadBlockCadNoFieldCB = new System.Windows.Forms.ComboBox();
            this.cadBlockCadNoTableCB = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cadBlockCaptionStyleCB = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cadBlockStyleCB = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cadBlockLayerCB = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cadBlockMapCB = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.oksSettingstabPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.oksAssignFieldCB = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.oksTypeFieldCB = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.oksAreaFieldCB = new System.Windows.Forms.ComboBox();
            this.oksAssignTableCB = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.oksTypeTableCB = new System.Windows.Forms.ComboBox();
            this.oksAddressFieldCB = new System.Windows.Forms.ComboBox();
            this.oksAreaTableCB = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.oksAddressTableCB = new System.Windows.Forms.ComboBox();
            this.oksCadNoFieldCB = new System.Windows.Forms.ComboBox();
            this.oksCadNoTableCB = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.oksCaptionStyleCB = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.oksUncompletedStyleCB = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.oksConstrStyleCB = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.oksBuildStyleCB = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.oksLayerCB = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.oksMapCB = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.zonesSettingstabPage = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.zoneRestrictFieldCB = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.zoneNameFieldCB = new System.Windows.Forms.ComboBox();
            this.zoneRestrictTableCB = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.zoneNameTableCB = new System.Windows.Forms.ComboBox();
            this.zoneAccountNoFieldCB = new System.Windows.Forms.ComboBox();
            this.zoneAccountNoTableCB = new System.Windows.Forms.ComboBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.zoneCaptionStyleCB = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.zoneSZZStyleCB = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.zoneTerrStyleCB = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.zoneLayerCB = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.zoneMapCB = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.SKConvertSettingstabPage = new System.Windows.Forms.TabPage();
            this.XtoYcheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.isTransformCheckBox = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sk2MathRB = new System.Windows.Forms.RadioButton();
            this.sk2GeoRB = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.sk1MathRB = new System.Windows.Forms.RadioButton();
            this.sk1GeoRB = new System.Windows.Forms.RadioButton();
            this.sk2LB = new System.Windows.Forms.ListBox();
            this.sk1LB = new System.Windows.Forms.ListBox();
            this.sk2CB = new System.Windows.Forms.ComboBox();
            this.sk1CB = new System.Windows.Forms.ComboBox();
            this.ingeoConnectBtn = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.saveSettingdBtn = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.isPozitionCheckBox = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.isAllZoomRB = new System.Windows.Forms.RadioButton();
            this.isOneZoomRB = new System.Windows.Forms.RadioButton();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.filesListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.importButton = new System.Windows.Forms.Button();
            this.delFileButton = new System.Windows.Forms.Button();
            this.addFileButton = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.openXMLFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1.SuspendLayout();
            this.lotSettingstabPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.cadBlockSettingstabPage.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.oksSettingstabPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.zonesSettingstabPage.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SKConvertSettingstabPage.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.lotSettingstabPage);
            this.tabControl1.Controls.Add(this.cadBlockSettingstabPage);
            this.tabControl1.Controls.Add(this.oksSettingstabPage);
            this.tabControl1.Controls.Add(this.zonesSettingstabPage);
            this.tabControl1.Controls.Add(this.SKConvertSettingstabPage);
            this.tabControl1.Location = new System.Drawing.Point(6, 6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(761, 394);
            this.tabControl1.TabIndex = 0;
            // 
            // lotSettingstabPage
            // 
            this.lotSettingstabPage.Controls.Add(this.groupBox2);
            this.lotSettingstabPage.Controls.Add(this.groupBox1);
            this.lotSettingstabPage.Location = new System.Drawing.Point(4, 22);
            this.lotSettingstabPage.Name = "lotSettingstabPage";
            this.lotSettingstabPage.Padding = new System.Windows.Forms.Padding(3);
            this.lotSettingstabPage.Size = new System.Drawing.Size(753, 368);
            this.lotSettingstabPage.TabIndex = 1;
            this.lotSettingstabPage.Text = "Земельные участки";
            this.lotSettingstabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lotUsingByDocFieldCB);
            this.groupBox2.Controls.Add(this.lotUsingFieldCB);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.lotLandCatFieldCB);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lotUsingByDocTableCB);
            this.groupBox2.Controls.Add(this.lotAreaFieldCB);
            this.groupBox2.Controls.Add(this.lotUsingTableCB);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lotLandCatTableCB);
            this.groupBox2.Controls.Add(this.lotAddressFieldCB);
            this.groupBox2.Controls.Add(this.lotAreaTableCB);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lotAddressTableCB);
            this.groupBox2.Controls.Add(this.lotCadNoFieldCB);
            this.groupBox2.Controls.Add(this.lotCadNoTableCB);
            this.groupBox2.Location = new System.Drawing.Point(240, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(501, 352);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Настройки семантики";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 294);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(178, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Вид использования по документу";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 252);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Вид использования";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Категория земель";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Площадь";
            // 
            // lotUsingByDocFieldCB
            // 
            this.lotUsingByDocFieldCB.FormattingEnabled = true;
            this.lotUsingByDocFieldCB.Location = new System.Drawing.Point(317, 313);
            this.lotUsingByDocFieldCB.Name = "lotUsingByDocFieldCB";
            this.lotUsingByDocFieldCB.Size = new System.Drawing.Size(166, 21);
            this.lotUsingByDocFieldCB.TabIndex = 1;
            // 
            // lotUsingFieldCB
            // 
            this.lotUsingFieldCB.FormattingEnabled = true;
            this.lotUsingFieldCB.Location = new System.Drawing.Point(317, 249);
            this.lotUsingFieldCB.Name = "lotUsingFieldCB";
            this.lotUsingFieldCB.Size = new System.Drawing.Size(166, 21);
            this.lotUsingFieldCB.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Адрес";
            // 
            // lotLandCatFieldCB
            // 
            this.lotLandCatFieldCB.FormattingEnabled = true;
            this.lotLandCatFieldCB.Location = new System.Drawing.Point(317, 199);
            this.lotLandCatFieldCB.Name = "lotLandCatFieldCB";
            this.lotLandCatFieldCB.Size = new System.Drawing.Size(166, 21);
            this.lotLandCatFieldCB.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Кадастровый номер";
            // 
            // lotUsingByDocTableCB
            // 
            this.lotUsingByDocTableCB.FormattingEnabled = true;
            this.lotUsingByDocTableCB.Location = new System.Drawing.Point(133, 313);
            this.lotUsingByDocTableCB.Name = "lotUsingByDocTableCB";
            this.lotUsingByDocTableCB.Size = new System.Drawing.Size(166, 21);
            this.lotUsingByDocTableCB.TabIndex = 1;
            this.lotUsingByDocTableCB.SelectedIndexChanged += new System.EventHandler(this.lotUsingByDocTableCB_SelectedIndexChanged);
            // 
            // lotAreaFieldCB
            // 
            this.lotAreaFieldCB.FormattingEnabled = true;
            this.lotAreaFieldCB.Location = new System.Drawing.Point(317, 148);
            this.lotAreaFieldCB.Name = "lotAreaFieldCB";
            this.lotAreaFieldCB.Size = new System.Drawing.Size(166, 21);
            this.lotAreaFieldCB.TabIndex = 1;
            // 
            // lotUsingTableCB
            // 
            this.lotUsingTableCB.FormattingEnabled = true;
            this.lotUsingTableCB.Location = new System.Drawing.Point(133, 249);
            this.lotUsingTableCB.Name = "lotUsingTableCB";
            this.lotUsingTableCB.Size = new System.Drawing.Size(166, 21);
            this.lotUsingTableCB.TabIndex = 1;
            this.lotUsingTableCB.SelectedIndexChanged += new System.EventHandler(this.lotUsingTableCB_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(372, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Поле";
            // 
            // lotLandCatTableCB
            // 
            this.lotLandCatTableCB.FormattingEnabled = true;
            this.lotLandCatTableCB.Location = new System.Drawing.Point(133, 199);
            this.lotLandCatTableCB.Name = "lotLandCatTableCB";
            this.lotLandCatTableCB.Size = new System.Drawing.Size(166, 21);
            this.lotLandCatTableCB.TabIndex = 1;
            this.lotLandCatTableCB.SelectedIndexChanged += new System.EventHandler(this.lotLandCatTableCB_SelectedIndexChanged);
            // 
            // lotAddressFieldCB
            // 
            this.lotAddressFieldCB.FormattingEnabled = true;
            this.lotAddressFieldCB.Location = new System.Drawing.Point(317, 96);
            this.lotAddressFieldCB.Name = "lotAddressFieldCB";
            this.lotAddressFieldCB.Size = new System.Drawing.Size(166, 21);
            this.lotAddressFieldCB.TabIndex = 1;
            // 
            // lotAreaTableCB
            // 
            this.lotAreaTableCB.FormattingEnabled = true;
            this.lotAreaTableCB.Location = new System.Drawing.Point(133, 148);
            this.lotAreaTableCB.Name = "lotAreaTableCB";
            this.lotAreaTableCB.Size = new System.Drawing.Size(166, 21);
            this.lotAreaTableCB.TabIndex = 1;
            this.lotAreaTableCB.SelectedIndexChanged += new System.EventHandler(this.lotAreaTableCB_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(183, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Таблица";
            // 
            // lotAddressTableCB
            // 
            this.lotAddressTableCB.FormattingEnabled = true;
            this.lotAddressTableCB.Location = new System.Drawing.Point(133, 96);
            this.lotAddressTableCB.Name = "lotAddressTableCB";
            this.lotAddressTableCB.Size = new System.Drawing.Size(166, 21);
            this.lotAddressTableCB.TabIndex = 1;
            this.lotAddressTableCB.SelectedIndexChanged += new System.EventHandler(this.lotAddressTableCB_SelectedIndexChanged);
            // 
            // lotCadNoFieldCB
            // 
            this.lotCadNoFieldCB.FormattingEnabled = true;
            this.lotCadNoFieldCB.Location = new System.Drawing.Point(317, 48);
            this.lotCadNoFieldCB.Name = "lotCadNoFieldCB";
            this.lotCadNoFieldCB.Size = new System.Drawing.Size(166, 21);
            this.lotCadNoFieldCB.TabIndex = 1;
            // 
            // lotCadNoTableCB
            // 
            this.lotCadNoTableCB.FormattingEnabled = true;
            this.lotCadNoTableCB.Location = new System.Drawing.Point(133, 48);
            this.lotCadNoTableCB.Name = "lotCadNoTableCB";
            this.lotCadNoTableCB.Size = new System.Drawing.Size(166, 21);
            this.lotCadNoTableCB.TabIndex = 1;
            this.lotCadNoTableCB.SelectedIndexChanged += new System.EventHandler(this.lotCadNoTableCB_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lotStyleSingCB);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lotStyleCB);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lotLayerCB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lotMapCB);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 238);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Пространственные настройки";
            // 
            // lotStyleSingCB
            // 
            this.lotStyleSingCB.FormattingEnabled = true;
            this.lotStyleSingCB.Location = new System.Drawing.Point(18, 192);
            this.lotStyleSingCB.Name = "lotStyleSingCB";
            this.lotStyleSingCB.Size = new System.Drawing.Size(193, 21);
            this.lotStyleSingCB.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Стиль подписи";
            // 
            // lotStyleCB
            // 
            this.lotStyleCB.FormattingEnabled = true;
            this.lotStyleCB.Location = new System.Drawing.Point(18, 141);
            this.lotStyleCB.Name = "lotStyleCB";
            this.lotStyleCB.Size = new System.Drawing.Size(193, 21);
            this.lotStyleCB.TabIndex = 1;
            this.lotStyleCB.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Стиль";
            this.label3.Visible = false;
            // 
            // lotLayerCB
            // 
            this.lotLayerCB.FormattingEnabled = true;
            this.lotLayerCB.Location = new System.Drawing.Point(18, 89);
            this.lotLayerCB.Name = "lotLayerCB";
            this.lotLayerCB.Size = new System.Drawing.Size(193, 21);
            this.lotLayerCB.TabIndex = 1;
            this.lotLayerCB.SelectedIndexChanged += new System.EventHandler(this.lotLayerCB_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Слой";
            // 
            // lotMapCB
            // 
            this.lotMapCB.FormattingEnabled = true;
            this.lotMapCB.Location = new System.Drawing.Point(18, 38);
            this.lotMapCB.Name = "lotMapCB";
            this.lotMapCB.Size = new System.Drawing.Size(193, 21);
            this.lotMapCB.TabIndex = 1;
            this.lotMapCB.SelectedIndexChanged += new System.EventHandler(this.lotMapCB_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Карта";
            // 
            // cadBlockSettingstabPage
            // 
            this.cadBlockSettingstabPage.Controls.Add(this.groupBox4);
            this.cadBlockSettingstabPage.Controls.Add(this.groupBox3);
            this.cadBlockSettingstabPage.Location = new System.Drawing.Point(4, 22);
            this.cadBlockSettingstabPage.Name = "cadBlockSettingstabPage";
            this.cadBlockSettingstabPage.Padding = new System.Windows.Forms.Padding(3);
            this.cadBlockSettingstabPage.Size = new System.Drawing.Size(753, 368);
            this.cadBlockSettingstabPage.TabIndex = 2;
            this.cadBlockSettingstabPage.Text = "Кад.кварталы";
            this.cadBlockSettingstabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.cadBlockAreaFieldCB);
            this.groupBox4.Controls.Add(this.cadBlockAreaTableCB);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.cadBlockCadNoFieldCB);
            this.groupBox4.Controls.Add(this.cadBlockCadNoTableCB);
            this.groupBox4.Location = new System.Drawing.Point(246, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(493, 237);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Настройки семантики";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 88);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "Площадь";
            // 
            // cadBlockAreaFieldCB
            // 
            this.cadBlockAreaFieldCB.FormattingEnabled = true;
            this.cadBlockAreaFieldCB.Location = new System.Drawing.Point(311, 85);
            this.cadBlockAreaFieldCB.Name = "cadBlockAreaFieldCB";
            this.cadBlockAreaFieldCB.Size = new System.Drawing.Size(166, 21);
            this.cadBlockAreaFieldCB.TabIndex = 8;
            // 
            // cadBlockAreaTableCB
            // 
            this.cadBlockAreaTableCB.FormattingEnabled = true;
            this.cadBlockAreaTableCB.Location = new System.Drawing.Point(127, 85);
            this.cadBlockAreaTableCB.Name = "cadBlockAreaTableCB";
            this.cadBlockAreaTableCB.Size = new System.Drawing.Size(166, 21);
            this.cadBlockAreaTableCB.TabIndex = 9;
            this.cadBlockAreaTableCB.SelectedIndexChanged += new System.EventHandler(this.cadBlockAreaTableCB_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Кадастровый номер";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(366, 10);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Поле";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(177, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Таблица";
            // 
            // cadBlockCadNoFieldCB
            // 
            this.cadBlockCadNoFieldCB.FormattingEnabled = true;
            this.cadBlockCadNoFieldCB.Location = new System.Drawing.Point(311, 34);
            this.cadBlockCadNoFieldCB.Name = "cadBlockCadNoFieldCB";
            this.cadBlockCadNoFieldCB.Size = new System.Drawing.Size(166, 21);
            this.cadBlockCadNoFieldCB.TabIndex = 5;
            // 
            // cadBlockCadNoTableCB
            // 
            this.cadBlockCadNoTableCB.FormattingEnabled = true;
            this.cadBlockCadNoTableCB.Location = new System.Drawing.Point(127, 34);
            this.cadBlockCadNoTableCB.Name = "cadBlockCadNoTableCB";
            this.cadBlockCadNoTableCB.Size = new System.Drawing.Size(166, 21);
            this.cadBlockCadNoTableCB.TabIndex = 6;
            this.cadBlockCadNoTableCB.SelectedIndexChanged += new System.EventHandler(this.cadBlockCadNoTableCB_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cadBlockCaptionStyleCB);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.cadBlockStyleCB);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.cadBlockLayerCB);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.cadBlockMapCB);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(234, 237);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Пространственные настройки";
            // 
            // cadBlockCaptionStyleCB
            // 
            this.cadBlockCaptionStyleCB.FormattingEnabled = true;
            this.cadBlockCaptionStyleCB.Location = new System.Drawing.Point(19, 191);
            this.cadBlockCaptionStyleCB.Name = "cadBlockCaptionStyleCB";
            this.cadBlockCaptionStyleCB.Size = new System.Drawing.Size(193, 21);
            this.cadBlockCaptionStyleCB.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 175);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Стиль подписи";
            // 
            // cadBlockStyleCB
            // 
            this.cadBlockStyleCB.FormattingEnabled = true;
            this.cadBlockStyleCB.Location = new System.Drawing.Point(19, 140);
            this.cadBlockStyleCB.Name = "cadBlockStyleCB";
            this.cadBlockStyleCB.Size = new System.Drawing.Size(193, 21);
            this.cadBlockStyleCB.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Стиль";
            // 
            // cadBlockLayerCB
            // 
            this.cadBlockLayerCB.FormattingEnabled = true;
            this.cadBlockLayerCB.Location = new System.Drawing.Point(19, 88);
            this.cadBlockLayerCB.Name = "cadBlockLayerCB";
            this.cadBlockLayerCB.Size = new System.Drawing.Size(193, 21);
            this.cadBlockLayerCB.TabIndex = 8;
            this.cadBlockLayerCB.SelectedIndexChanged += new System.EventHandler(this.cadBlockLayerCB_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Слой";
            // 
            // cadBlockMapCB
            // 
            this.cadBlockMapCB.FormattingEnabled = true;
            this.cadBlockMapCB.Location = new System.Drawing.Point(19, 37);
            this.cadBlockMapCB.Name = "cadBlockMapCB";
            this.cadBlockMapCB.Size = new System.Drawing.Size(193, 21);
            this.cadBlockMapCB.TabIndex = 9;
            this.cadBlockMapCB.SelectedIndexChanged += new System.EventHandler(this.cadBlockMapCB_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Карта";
            // 
            // oksSettingstabPage
            // 
            this.oksSettingstabPage.Controls.Add(this.groupBox5);
            this.oksSettingstabPage.Controls.Add(this.groupBox6);
            this.oksSettingstabPage.Location = new System.Drawing.Point(4, 22);
            this.oksSettingstabPage.Name = "oksSettingstabPage";
            this.oksSettingstabPage.Padding = new System.Windows.Forms.Padding(3);
            this.oksSettingstabPage.Size = new System.Drawing.Size(753, 368);
            this.oksSettingstabPage.TabIndex = 0;
            this.oksSettingstabPage.Text = "ОКС";
            this.oksSettingstabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.oksAssignFieldCB);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.oksTypeFieldCB);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.oksAreaFieldCB);
            this.groupBox5.Controls.Add(this.oksAssignTableCB);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.oksTypeTableCB);
            this.groupBox5.Controls.Add(this.oksAddressFieldCB);
            this.groupBox5.Controls.Add(this.oksAreaTableCB);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.oksAddressTableCB);
            this.groupBox5.Controls.Add(this.oksCadNoFieldCB);
            this.groupBox5.Controls.Add(this.oksCadNoTableCB);
            this.groupBox5.Location = new System.Drawing.Point(240, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(501, 283);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Настройки семантики";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(17, 252);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Назначение";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(17, 202);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(26, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Тип";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(17, 151);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Площадь";
            // 
            // oksAssignFieldCB
            // 
            this.oksAssignFieldCB.FormattingEnabled = true;
            this.oksAssignFieldCB.Location = new System.Drawing.Point(317, 249);
            this.oksAssignFieldCB.Name = "oksAssignFieldCB";
            this.oksAssignFieldCB.Size = new System.Drawing.Size(166, 21);
            this.oksAssignFieldCB.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(17, 99);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Адрес";
            // 
            // oksTypeFieldCB
            // 
            this.oksTypeFieldCB.FormattingEnabled = true;
            this.oksTypeFieldCB.Location = new System.Drawing.Point(317, 199);
            this.oksTypeFieldCB.Name = "oksTypeFieldCB";
            this.oksTypeFieldCB.Size = new System.Drawing.Size(166, 21);
            this.oksTypeFieldCB.TabIndex = 1;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(17, 51);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Кадастровый номер";
            // 
            // oksAreaFieldCB
            // 
            this.oksAreaFieldCB.FormattingEnabled = true;
            this.oksAreaFieldCB.Location = new System.Drawing.Point(317, 148);
            this.oksAreaFieldCB.Name = "oksAreaFieldCB";
            this.oksAreaFieldCB.Size = new System.Drawing.Size(166, 21);
            this.oksAreaFieldCB.TabIndex = 1;
            // 
            // oksAssignTableCB
            // 
            this.oksAssignTableCB.FormattingEnabled = true;
            this.oksAssignTableCB.Location = new System.Drawing.Point(133, 249);
            this.oksAssignTableCB.Name = "oksAssignTableCB";
            this.oksAssignTableCB.Size = new System.Drawing.Size(166, 21);
            this.oksAssignTableCB.TabIndex = 1;
            this.oksAssignTableCB.SelectedIndexChanged += new System.EventHandler(this.oksAssignTableCB_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(372, 24);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Поле";
            // 
            // oksTypeTableCB
            // 
            this.oksTypeTableCB.FormattingEnabled = true;
            this.oksTypeTableCB.Location = new System.Drawing.Point(133, 199);
            this.oksTypeTableCB.Name = "oksTypeTableCB";
            this.oksTypeTableCB.Size = new System.Drawing.Size(166, 21);
            this.oksTypeTableCB.TabIndex = 1;
            this.oksTypeTableCB.SelectedIndexChanged += new System.EventHandler(this.oksTypeTableCB_SelectedIndexChanged);
            // 
            // oksAddressFieldCB
            // 
            this.oksAddressFieldCB.FormattingEnabled = true;
            this.oksAddressFieldCB.Location = new System.Drawing.Point(317, 96);
            this.oksAddressFieldCB.Name = "oksAddressFieldCB";
            this.oksAddressFieldCB.Size = new System.Drawing.Size(166, 21);
            this.oksAddressFieldCB.TabIndex = 1;
            // 
            // oksAreaTableCB
            // 
            this.oksAreaTableCB.FormattingEnabled = true;
            this.oksAreaTableCB.Location = new System.Drawing.Point(133, 148);
            this.oksAreaTableCB.Name = "oksAreaTableCB";
            this.oksAreaTableCB.Size = new System.Drawing.Size(166, 21);
            this.oksAreaTableCB.TabIndex = 1;
            this.oksAreaTableCB.SelectedIndexChanged += new System.EventHandler(this.oksAreaTableCB_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(183, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(50, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Таблица";
            // 
            // oksAddressTableCB
            // 
            this.oksAddressTableCB.FormattingEnabled = true;
            this.oksAddressTableCB.Location = new System.Drawing.Point(133, 96);
            this.oksAddressTableCB.Name = "oksAddressTableCB";
            this.oksAddressTableCB.Size = new System.Drawing.Size(166, 21);
            this.oksAddressTableCB.TabIndex = 1;
            this.oksAddressTableCB.SelectedIndexChanged += new System.EventHandler(this.oksAddressTableCB_SelectedIndexChanged);
            // 
            // oksCadNoFieldCB
            // 
            this.oksCadNoFieldCB.FormattingEnabled = true;
            this.oksCadNoFieldCB.Location = new System.Drawing.Point(317, 48);
            this.oksCadNoFieldCB.Name = "oksCadNoFieldCB";
            this.oksCadNoFieldCB.Size = new System.Drawing.Size(166, 21);
            this.oksCadNoFieldCB.TabIndex = 1;
            // 
            // oksCadNoTableCB
            // 
            this.oksCadNoTableCB.FormattingEnabled = true;
            this.oksCadNoTableCB.Location = new System.Drawing.Point(133, 48);
            this.oksCadNoTableCB.Name = "oksCadNoTableCB";
            this.oksCadNoTableCB.Size = new System.Drawing.Size(166, 21);
            this.oksCadNoTableCB.TabIndex = 1;
            this.oksCadNoTableCB.SelectedIndexChanged += new System.EventHandler(this.oksCadNoTableCB_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.oksCaptionStyleCB);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.oksUncompletedStyleCB);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.oksConstrStyleCB);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.oksBuildStyleCB);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.oksLayerCB);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.oksMapCB);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(228, 352);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Пространственные настройки";
            // 
            // oksCaptionStyleCB
            // 
            this.oksCaptionStyleCB.FormattingEnabled = true;
            this.oksCaptionStyleCB.Location = new System.Drawing.Point(16, 286);
            this.oksCaptionStyleCB.Name = "oksCaptionStyleCB";
            this.oksCaptionStyleCB.Size = new System.Drawing.Size(193, 21);
            this.oksCaptionStyleCB.TabIndex = 1;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(13, 270);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(82, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Стиль подписи";
            // 
            // oksUncompletedStyleCB
            // 
            this.oksUncompletedStyleCB.FormattingEnabled = true;
            this.oksUncompletedStyleCB.Location = new System.Drawing.Point(16, 235);
            this.oksUncompletedStyleCB.Name = "oksUncompletedStyleCB";
            this.oksUncompletedStyleCB.Size = new System.Drawing.Size(193, 21);
            this.oksUncompletedStyleCB.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(13, 219);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(212, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "Стиль о.незавершенного строительства";
            // 
            // oksConstrStyleCB
            // 
            this.oksConstrStyleCB.FormattingEnabled = true;
            this.oksConstrStyleCB.Location = new System.Drawing.Point(16, 182);
            this.oksConstrStyleCB.Name = "oksConstrStyleCB";
            this.oksConstrStyleCB.Size = new System.Drawing.Size(193, 21);
            this.oksConstrStyleCB.TabIndex = 1;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(13, 166);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(87, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "Стиль строения";
            // 
            // oksBuildStyleCB
            // 
            this.oksBuildStyleCB.FormattingEnabled = true;
            this.oksBuildStyleCB.Location = new System.Drawing.Point(16, 138);
            this.oksBuildStyleCB.Name = "oksBuildStyleCB";
            this.oksBuildStyleCB.Size = new System.Drawing.Size(193, 21);
            this.oksBuildStyleCB.TabIndex = 1;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(13, 122);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(76, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Стиль здания";
            // 
            // oksLayerCB
            // 
            this.oksLayerCB.FormattingEnabled = true;
            this.oksLayerCB.Location = new System.Drawing.Point(16, 86);
            this.oksLayerCB.Name = "oksLayerCB";
            this.oksLayerCB.Size = new System.Drawing.Size(193, 21);
            this.oksLayerCB.TabIndex = 1;
            this.oksLayerCB.SelectedIndexChanged += new System.EventHandler(this.oksLayerCB_SelectedIndexChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(13, 70);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(32, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "Слой";
            // 
            // oksMapCB
            // 
            this.oksMapCB.FormattingEnabled = true;
            this.oksMapCB.Location = new System.Drawing.Point(16, 35);
            this.oksMapCB.Name = "oksMapCB";
            this.oksMapCB.Size = new System.Drawing.Size(193, 21);
            this.oksMapCB.TabIndex = 1;
            this.oksMapCB.SelectedIndexChanged += new System.EventHandler(this.oksMapCB_SelectedIndexChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(13, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(37, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Карта";
            // 
            // zonesSettingstabPage
            // 
            this.zonesSettingstabPage.Controls.Add(this.groupBox7);
            this.zonesSettingstabPage.Controls.Add(this.groupBox8);
            this.zonesSettingstabPage.Location = new System.Drawing.Point(4, 22);
            this.zonesSettingstabPage.Name = "zonesSettingstabPage";
            this.zonesSettingstabPage.Size = new System.Drawing.Size(753, 368);
            this.zonesSettingstabPage.TabIndex = 3;
            this.zonesSettingstabPage.Text = "Зоны";
            this.zonesSettingstabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.label38);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Controls.Add(this.zoneRestrictFieldCB);
            this.groupBox7.Controls.Add(this.label40);
            this.groupBox7.Controls.Add(this.zoneNameFieldCB);
            this.groupBox7.Controls.Add(this.zoneRestrictTableCB);
            this.groupBox7.Controls.Add(this.label41);
            this.groupBox7.Controls.Add(this.zoneNameTableCB);
            this.groupBox7.Controls.Add(this.zoneAccountNoFieldCB);
            this.groupBox7.Controls.Add(this.zoneAccountNoTableCB);
            this.groupBox7.Location = new System.Drawing.Point(243, 8);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(501, 283);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Настройки семантики";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(17, 137);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(127, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Ограничения (для СЗЗ) ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(17, 80);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(220, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Наименование объекта землеустройства";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(17, 51);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(86, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Учётный номер";
            // 
            // zoneRestrictFieldCB
            // 
            this.zoneRestrictFieldCB.FormattingEnabled = true;
            this.zoneRestrictFieldCB.Location = new System.Drawing.Point(317, 156);
            this.zoneRestrictFieldCB.Name = "zoneRestrictFieldCB";
            this.zoneRestrictFieldCB.Size = new System.Drawing.Size(166, 21);
            this.zoneRestrictFieldCB.TabIndex = 1;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(372, 24);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(33, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Поле";
            // 
            // zoneNameFieldCB
            // 
            this.zoneNameFieldCB.FormattingEnabled = true;
            this.zoneNameFieldCB.Location = new System.Drawing.Point(317, 102);
            this.zoneNameFieldCB.Name = "zoneNameFieldCB";
            this.zoneNameFieldCB.Size = new System.Drawing.Size(166, 21);
            this.zoneNameFieldCB.TabIndex = 1;
            // 
            // zoneRestrictTableCB
            // 
            this.zoneRestrictTableCB.FormattingEnabled = true;
            this.zoneRestrictTableCB.Location = new System.Drawing.Point(133, 156);
            this.zoneRestrictTableCB.Name = "zoneRestrictTableCB";
            this.zoneRestrictTableCB.Size = new System.Drawing.Size(166, 21);
            this.zoneRestrictTableCB.TabIndex = 1;
            this.zoneRestrictTableCB.SelectedIndexChanged += new System.EventHandler(this.zoneRestrictTableCB_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(183, 24);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(50, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Таблица";
            // 
            // zoneNameTableCB
            // 
            this.zoneNameTableCB.FormattingEnabled = true;
            this.zoneNameTableCB.Location = new System.Drawing.Point(133, 102);
            this.zoneNameTableCB.Name = "zoneNameTableCB";
            this.zoneNameTableCB.Size = new System.Drawing.Size(166, 21);
            this.zoneNameTableCB.TabIndex = 1;
            this.zoneNameTableCB.SelectedIndexChanged += new System.EventHandler(this.zoneNameTableCB_SelectedIndexChanged);
            // 
            // zoneAccountNoFieldCB
            // 
            this.zoneAccountNoFieldCB.FormattingEnabled = true;
            this.zoneAccountNoFieldCB.Location = new System.Drawing.Point(317, 48);
            this.zoneAccountNoFieldCB.Name = "zoneAccountNoFieldCB";
            this.zoneAccountNoFieldCB.Size = new System.Drawing.Size(166, 21);
            this.zoneAccountNoFieldCB.TabIndex = 1;
            // 
            // zoneAccountNoTableCB
            // 
            this.zoneAccountNoTableCB.FormattingEnabled = true;
            this.zoneAccountNoTableCB.Location = new System.Drawing.Point(133, 48);
            this.zoneAccountNoTableCB.Name = "zoneAccountNoTableCB";
            this.zoneAccountNoTableCB.Size = new System.Drawing.Size(166, 21);
            this.zoneAccountNoTableCB.TabIndex = 1;
            this.zoneAccountNoTableCB.SelectedIndexChanged += new System.EventHandler(this.zoneAccountNoTableCB_SelectedIndexChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.zoneCaptionStyleCB);
            this.groupBox8.Controls.Add(this.label42);
            this.groupBox8.Controls.Add(this.zoneSZZStyleCB);
            this.groupBox8.Controls.Add(this.label44);
            this.groupBox8.Controls.Add(this.zoneTerrStyleCB);
            this.groupBox8.Controls.Add(this.label45);
            this.groupBox8.Controls.Add(this.zoneLayerCB);
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Controls.Add(this.zoneMapCB);
            this.groupBox8.Controls.Add(this.label47);
            this.groupBox8.Location = new System.Drawing.Point(9, 8);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(228, 283);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Пространственные настройки";
            // 
            // zoneCaptionStyleCB
            // 
            this.zoneCaptionStyleCB.FormattingEnabled = true;
            this.zoneCaptionStyleCB.Location = new System.Drawing.Point(15, 233);
            this.zoneCaptionStyleCB.Name = "zoneCaptionStyleCB";
            this.zoneCaptionStyleCB.Size = new System.Drawing.Size(193, 21);
            this.zoneCaptionStyleCB.TabIndex = 1;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(12, 217);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(82, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Стиль подписи";
            // 
            // zoneSZZStyleCB
            // 
            this.zoneSZZStyleCB.FormattingEnabled = true;
            this.zoneSZZStyleCB.Location = new System.Drawing.Point(16, 182);
            this.zoneSZZStyleCB.Name = "zoneSZZStyleCB";
            this.zoneSZZStyleCB.Size = new System.Drawing.Size(193, 21);
            this.zoneSZZStyleCB.TabIndex = 1;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(13, 166);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Стиль сзз";
            // 
            // zoneTerrStyleCB
            // 
            this.zoneTerrStyleCB.FormattingEnabled = true;
            this.zoneTerrStyleCB.Location = new System.Drawing.Point(16, 138);
            this.zoneTerrStyleCB.Name = "zoneTerrStyleCB";
            this.zoneTerrStyleCB.Size = new System.Drawing.Size(193, 21);
            this.zoneTerrStyleCB.TabIndex = 1;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(13, 122);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(81, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "Стиль террзон";
            // 
            // zoneLayerCB
            // 
            this.zoneLayerCB.FormattingEnabled = true;
            this.zoneLayerCB.Location = new System.Drawing.Point(16, 86);
            this.zoneLayerCB.Name = "zoneLayerCB";
            this.zoneLayerCB.Size = new System.Drawing.Size(193, 21);
            this.zoneLayerCB.TabIndex = 1;
            this.zoneLayerCB.SelectedIndexChanged += new System.EventHandler(this.zoneLayerCB_SelectedIndexChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(13, 70);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(32, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Слой";
            // 
            // zoneMapCB
            // 
            this.zoneMapCB.FormattingEnabled = true;
            this.zoneMapCB.Location = new System.Drawing.Point(16, 35);
            this.zoneMapCB.Name = "zoneMapCB";
            this.zoneMapCB.Size = new System.Drawing.Size(193, 21);
            this.zoneMapCB.TabIndex = 1;
            this.zoneMapCB.SelectedIndexChanged += new System.EventHandler(this.zoneMapCB_SelectedIndexChanged);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(13, 19);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(37, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "Карта";
            // 
            // SKConvertSettingstabPage
            // 
            this.SKConvertSettingstabPage.Controls.Add(this.XtoYcheckBox);
            this.SKConvertSettingstabPage.Controls.Add(this.groupBox9);
            this.SKConvertSettingstabPage.Location = new System.Drawing.Point(4, 22);
            this.SKConvertSettingstabPage.Name = "SKConvertSettingstabPage";
            this.SKConvertSettingstabPage.Padding = new System.Windows.Forms.Padding(3);
            this.SKConvertSettingstabPage.Size = new System.Drawing.Size(753, 368);
            this.SKConvertSettingstabPage.TabIndex = 4;
            this.SKConvertSettingstabPage.Text = "Пересчет СК";
            this.SKConvertSettingstabPage.UseVisualStyleBackColor = true;
            // 
            // XtoYcheckBox
            // 
            this.XtoYcheckBox.AutoSize = true;
            this.XtoYcheckBox.Location = new System.Drawing.Point(488, 17);
            this.XtoYcheckBox.Name = "XtoYcheckBox";
            this.XtoYcheckBox.Size = new System.Drawing.Size(154, 17);
            this.XtoYcheckBox.TabIndex = 5;
            this.XtoYcheckBox.Text = "Поменять X и Y местами";
            this.XtoYcheckBox.UseVisualStyleBackColor = true;
            this.XtoYcheckBox.CheckedChanged += new System.EventHandler(this.XtoYcheckBox_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.isTransformCheckBox);
            this.groupBox9.Controls.Add(this.panel2);
            this.groupBox9.Controls.Add(this.panel1);
            this.groupBox9.Controls.Add(this.sk2LB);
            this.groupBox9.Controls.Add(this.sk1LB);
            this.groupBox9.Controls.Add(this.sk2CB);
            this.groupBox9.Controls.Add(this.sk1CB);
            this.groupBox9.Location = new System.Drawing.Point(6, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(417, 257);
            this.groupBox9.TabIndex = 4;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Системы координат:";
            // 
            // isTransformCheckBox
            // 
            this.isTransformCheckBox.AutoSize = true;
            this.isTransformCheckBox.Location = new System.Drawing.Point(308, 11);
            this.isTransformCheckBox.Name = "isTransformCheckBox";
            this.isTransformCheckBox.Size = new System.Drawing.Size(100, 17);
            this.isTransformCheckBox.TabIndex = 9;
            this.isTransformCheckBox.Text = "Без пересчета";
            this.isTransformCheckBox.UseVisualStyleBackColor = true;
            this.isTransformCheckBox.CheckedChanged += new System.EventHandler(this.isTransformCheckBox_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sk2MathRB);
            this.panel2.Controls.Add(this.sk2GeoRB);
            this.panel2.Location = new System.Drawing.Point(211, 211);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(159, 45);
            this.panel2.TabIndex = 8;
            // 
            // sk2MathRB
            // 
            this.sk2MathRB.AutoSize = true;
            this.sk2MathRB.Checked = true;
            this.sk2MathRB.Location = new System.Drawing.Point(6, 4);
            this.sk2MathRB.Name = "sk2MathRB";
            this.sk2MathRB.Size = new System.Drawing.Size(111, 17);
            this.sk2MathRB.TabIndex = 4;
            this.sk2MathRB.TabStop = true;
            this.sk2MathRB.Text = "Математическая";
            this.sk2MathRB.UseVisualStyleBackColor = true;
            this.sk2MathRB.CheckedChanged += new System.EventHandler(this.sk2MathRB_CheckedChanged);
            // 
            // sk2GeoRB
            // 
            this.sk2GeoRB.AutoSize = true;
            this.sk2GeoRB.Location = new System.Drawing.Point(6, 22);
            this.sk2GeoRB.Name = "sk2GeoRB";
            this.sk2GeoRB.Size = new System.Drawing.Size(102, 17);
            this.sk2GeoRB.TabIndex = 5;
            this.sk2GeoRB.Text = "Геодезическая";
            this.sk2GeoRB.UseVisualStyleBackColor = true;
            this.sk2GeoRB.CheckedChanged += new System.EventHandler(this.sk2GeoRB_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.sk1MathRB);
            this.panel1.Controls.Add(this.sk1GeoRB);
            this.panel1.Location = new System.Drawing.Point(9, 211);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(159, 45);
            this.panel1.TabIndex = 7;
            // 
            // sk1MathRB
            // 
            this.sk1MathRB.AutoSize = true;
            this.sk1MathRB.Checked = true;
            this.sk1MathRB.Location = new System.Drawing.Point(6, 4);
            this.sk1MathRB.Name = "sk1MathRB";
            this.sk1MathRB.Size = new System.Drawing.Size(111, 17);
            this.sk1MathRB.TabIndex = 4;
            this.sk1MathRB.TabStop = true;
            this.sk1MathRB.Text = "Математическая";
            this.sk1MathRB.UseVisualStyleBackColor = true;
            this.sk1MathRB.CheckedChanged += new System.EventHandler(this.sk1MathRB_CheckedChanged);
            // 
            // sk1GeoRB
            // 
            this.sk1GeoRB.AutoSize = true;
            this.sk1GeoRB.Location = new System.Drawing.Point(6, 22);
            this.sk1GeoRB.Name = "sk1GeoRB";
            this.sk1GeoRB.Size = new System.Drawing.Size(102, 17);
            this.sk1GeoRB.TabIndex = 5;
            this.sk1GeoRB.Text = "Геодезическая";
            this.sk1GeoRB.UseVisualStyleBackColor = true;
            this.sk1GeoRB.CheckedChanged += new System.EventHandler(this.sk1GeoRB_CheckedChanged);
            // 
            // sk2LB
            // 
            this.sk2LB.DisplayMember = "Value";
            this.sk2LB.FormattingEnabled = true;
            this.sk2LB.Location = new System.Drawing.Point(211, 59);
            this.sk2LB.Name = "sk2LB";
            this.sk2LB.Size = new System.Drawing.Size(196, 147);
            this.sk2LB.TabIndex = 3;
            // 
            // sk1LB
            // 
            this.sk1LB.DisplayMember = "Value";
            this.sk1LB.FormattingEnabled = true;
            this.sk1LB.Location = new System.Drawing.Point(9, 58);
            this.sk1LB.Name = "sk1LB";
            this.sk1LB.Size = new System.Drawing.Size(196, 147);
            this.sk1LB.TabIndex = 2;
            this.sk1LB.SelectedValueChanged += new System.EventHandler(this.sk1LB_SelectedValueChanged);
            // 
            // sk2CB
            // 
            this.sk2CB.DisplayMember = "Value";
            this.sk2CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sk2CB.FormattingEnabled = true;
            this.sk2CB.Location = new System.Drawing.Point(211, 31);
            this.sk2CB.Name = "sk2CB";
            this.sk2CB.Size = new System.Drawing.Size(196, 21);
            this.sk2CB.TabIndex = 1;
            // 
            // sk1CB
            // 
            this.sk1CB.DisplayMember = "Value";
            this.sk1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sk1CB.FormattingEnabled = true;
            this.sk1CB.Location = new System.Drawing.Point(9, 31);
            this.sk1CB.Name = "sk1CB";
            this.sk1CB.Size = new System.Drawing.Size(196, 21);
            this.sk1CB.TabIndex = 0;
            this.sk1CB.SelectedIndexChanged += new System.EventHandler(this.sk1CB_SelectedIndexChanged);
            this.sk1CB.SelectedValueChanged += new System.EventHandler(this.sk1CB_SelectedValueChanged);
            // 
            // ingeoConnectBtn
            // 
            this.ingeoConnectBtn.Location = new System.Drawing.Point(203, 416);
            this.ingeoConnectBtn.Name = "ingeoConnectBtn";
            this.ingeoConnectBtn.Size = new System.Drawing.Size(118, 23);
            this.ingeoConnectBtn.TabIndex = 1;
            this.ingeoConnectBtn.Text = "Подключиться";
            this.ingeoConnectBtn.UseVisualStyleBackColor = true;
            this.ingeoConnectBtn.Click += new System.EventHandler(this.ingeoConnectBtn_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(69, 421);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Не подключено к Ингео";
            // 
            // saveSettingdBtn
            // 
            this.saveSettingdBtn.Location = new System.Drawing.Point(436, 416);
            this.saveSettingdBtn.Name = "saveSettingdBtn";
            this.saveSettingdBtn.Size = new System.Drawing.Size(244, 23);
            this.saveSettingdBtn.TabIndex = 3;
            this.saveSettingdBtn.Text = "Сохранить все настройки";
            this.saveSettingdBtn.UseVisualStyleBackColor = true;
            this.saveSettingdBtn.Click += new System.EventHandler(this.saveSettingdBtn_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(4, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(875, 476);
            this.tabControl2.TabIndex = 5;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(867, 450);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Импорт";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.label36);
            this.groupBox10.Controls.Add(this.isPozitionCheckBox);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Controls.Add(this.isAllZoomRB);
            this.groupBox10.Controls.Add(this.isOneZoomRB);
            this.groupBox10.Controls.Add(this.progressBar1);
            this.groupBox10.Controls.Add(this.filesListView);
            this.groupBox10.Controls.Add(this.importButton);
            this.groupBox10.Controls.Add(this.delFileButton);
            this.groupBox10.Controls.Add(this.addFileButton);
            this.groupBox10.Location = new System.Drawing.Point(3, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(858, 438);
            this.groupBox10.TabIndex = 5;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Файлы для импорта:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(518, 46);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(139, 13);
            this.label36.TabIndex = 13;
            this.label36.Text = "Импорт без смены X на Y";
            // 
            // isPozitionCheckBox
            // 
            this.isPozitionCheckBox.AutoSize = true;
            this.isPozitionCheckBox.Location = new System.Drawing.Point(300, 10);
            this.isPozitionCheckBox.Name = "isPozitionCheckBox";
            this.isPozitionCheckBox.Size = new System.Drawing.Size(15, 14);
            this.isPozitionCheckBox.TabIndex = 12;
            this.isPozitionCheckBox.UseVisualStyleBackColor = true;
            this.isPozitionCheckBox.CheckStateChanged += new System.EventHandler(this.isPozitionCheckBox_CheckStateChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(189, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Позиционирование";
            // 
            // isAllZoomRB
            // 
            this.isAllZoomRB.AutoSize = true;
            this.isAllZoomRB.Checked = true;
            this.isAllZoomRB.Enabled = false;
            this.isAllZoomRB.Location = new System.Drawing.Point(168, 26);
            this.isAllZoomRB.Name = "isAllZoomRB";
            this.isAllZoomRB.Size = new System.Drawing.Size(68, 17);
            this.isAllZoomRB.TabIndex = 10;
            this.isAllZoomRB.TabStop = true;
            this.isAllZoomRB.Text = "По всем";
            this.isAllZoomRB.UseVisualStyleBackColor = true;
            // 
            // isOneZoomRB
            // 
            this.isOneZoomRB.AutoSize = true;
            this.isOneZoomRB.Enabled = false;
            this.isOneZoomRB.Location = new System.Drawing.Point(242, 26);
            this.isOneZoomRB.Name = "isOneZoomRB";
            this.isOneZoomRB.Size = new System.Drawing.Size(103, 17);
            this.isOneZoomRB.TabIndex = 9;
            this.isOneZoomRB.Text = "По последнему";
            this.isOneZoomRB.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(464, 20);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(388, 23);
            this.progressBar1.TabIndex = 7;
            // 
            // filesListView
            // 
            this.filesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.filesListView.FullRowSelect = true;
            this.filesListView.GridLines = true;
            this.filesListView.Location = new System.Drawing.Point(6, 65);
            this.filesListView.Name = "filesListView";
            this.filesListView.Size = new System.Drawing.Size(846, 437);
            this.filesListView.TabIndex = 6;
            this.filesListView.UseCompatibleStateImageBehavior = false;
            this.filesListView.View = System.Windows.Forms.View.Details;
            this.filesListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.filesListView_ItemChecked);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Имя файла";
            this.columnHeader1.Width = 556;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Тип файла";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Версия файла";
            this.columnHeader3.Width = 85;
            // 
            // importButton
            // 
            this.importButton.Location = new System.Drawing.Point(351, 20);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(107, 23);
            this.importButton.TabIndex = 5;
            this.importButton.Text = "Импортировать";
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);
            // 
            // delFileButton
            // 
            this.delFileButton.Location = new System.Drawing.Point(87, 20);
            this.delFileButton.Name = "delFileButton";
            this.delFileButton.Size = new System.Drawing.Size(75, 23);
            this.delFileButton.TabIndex = 4;
            this.delFileButton.Text = "Исключить";
            this.delFileButton.UseVisualStyleBackColor = true;
            this.delFileButton.Click += new System.EventHandler(this.delFileButton_Click);
            // 
            // addFileButton
            // 
            this.addFileButton.Location = new System.Drawing.Point(6, 20);
            this.addFileButton.Name = "addFileButton";
            this.addFileButton.Size = new System.Drawing.Size(75, 23);
            this.addFileButton.TabIndex = 3;
            this.addFileButton.Text = "Добавить";
            this.addFileButton.UseVisualStyleBackColor = true;
            this.addFileButton.Click += new System.EventHandler(this.addFileButton_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tabControl1);
            this.tabPage4.Controls.Add(this.ingeoConnectBtn);
            this.tabPage4.Controls.Add(this.saveSettingdBtn);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(867, 450);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Настройки";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // openXMLFileDialog
            // 
            this.openXMLFileDialog.Filter = "XML|*.xml";
            this.openXMLFileDialog.Multiselect = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 485);
            this.Controls.Add(this.tabControl2);
            this.Name = "MainForm";
            this.Text = "RosreesrtXMLIngeo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.lotSettingstabPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.cadBlockSettingstabPage.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.oksSettingstabPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.zonesSettingstabPage.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.SKConvertSettingstabPage.ResumeLayout(false);
            this.SKConvertSettingstabPage.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage oksSettingstabPage;
        private System.Windows.Forms.TabPage lotSettingstabPage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ingeoConnectBtn;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button saveSettingdBtn;
        public System.Windows.Forms.ComboBox lotMapCB;
        public System.Windows.Forms.ComboBox lotStyleCB;
        public System.Windows.Forms.ComboBox lotLayerCB;
        public System.Windows.Forms.ComboBox lotStyleSingCB;
        public System.Windows.Forms.ComboBox lotUsingByDocFieldCB;
        public System.Windows.Forms.ComboBox lotUsingFieldCB;
        public System.Windows.Forms.ComboBox lotLandCatFieldCB;
        public System.Windows.Forms.ComboBox lotUsingByDocTableCB;
        public System.Windows.Forms.ComboBox lotAreaFieldCB;
        public System.Windows.Forms.ComboBox lotUsingTableCB;
        public System.Windows.Forms.ComboBox lotLandCatTableCB;
        public System.Windows.Forms.ComboBox lotAddressFieldCB;
        public System.Windows.Forms.ComboBox lotAreaTableCB;
        public System.Windows.Forms.ComboBox lotAddressTableCB;
        public System.Windows.Forms.ComboBox lotCadNoFieldCB;
        public System.Windows.Forms.ComboBox lotCadNoTableCB;
        private System.Windows.Forms.TabPage cadBlockSettingstabPage;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.ComboBox cadBlockAreaFieldCB;
        public System.Windows.Forms.ComboBox cadBlockAreaTableCB;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.ComboBox cadBlockCadNoFieldCB;
        public System.Windows.Forms.ComboBox cadBlockCadNoTableCB;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.ComboBox cadBlockCaptionStyleCB;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.ComboBox cadBlockStyleCB;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.ComboBox cadBlockLayerCB;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.ComboBox cadBlockMapCB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.ComboBox oksAssignFieldCB;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.ComboBox oksTypeFieldCB;
        private System.Windows.Forms.Label label27;
        public System.Windows.Forms.ComboBox oksAreaFieldCB;
        public System.Windows.Forms.ComboBox oksAssignTableCB;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.ComboBox oksTypeTableCB;
        public System.Windows.Forms.ComboBox oksAddressFieldCB;
        public System.Windows.Forms.ComboBox oksAreaTableCB;
        private System.Windows.Forms.Label label29;
        public System.Windows.Forms.ComboBox oksAddressTableCB;
        public System.Windows.Forms.ComboBox oksCadNoFieldCB;
        public System.Windows.Forms.ComboBox oksCadNoTableCB;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.ComboBox oksCaptionStyleCB;
        private System.Windows.Forms.Label label30;
        public System.Windows.Forms.ComboBox oksBuildStyleCB;
        private System.Windows.Forms.Label label31;
        public System.Windows.Forms.ComboBox oksLayerCB;
        private System.Windows.Forms.Label label32;
        public System.Windows.Forms.ComboBox oksMapCB;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage zonesSettingstabPage;
        public System.Windows.Forms.ComboBox oksUncompletedStyleCB;
        private System.Windows.Forms.Label label35;
        public System.Windows.Forms.ComboBox oksConstrStyleCB;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        public System.Windows.Forms.ComboBox zoneRestrictFieldCB;
        private System.Windows.Forms.Label label40;
        public System.Windows.Forms.ComboBox zoneNameFieldCB;
        public System.Windows.Forms.ComboBox zoneRestrictTableCB;
        private System.Windows.Forms.Label label41;
        public System.Windows.Forms.ComboBox zoneNameTableCB;
        public System.Windows.Forms.ComboBox zoneAccountNoFieldCB;
        public System.Windows.Forms.ComboBox zoneAccountNoTableCB;
        private System.Windows.Forms.GroupBox groupBox8;
        public System.Windows.Forms.ComboBox zoneCaptionStyleCB;
        private System.Windows.Forms.Label label42;
        public System.Windows.Forms.ComboBox zoneSZZStyleCB;
        private System.Windows.Forms.Label label44;
        public System.Windows.Forms.ComboBox zoneTerrStyleCB;
        private System.Windows.Forms.Label label45;
        public System.Windows.Forms.ComboBox zoneLayerCB;
        private System.Windows.Forms.Label label46;
        public System.Windows.Forms.ComboBox zoneMapCB;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage SKConvertSettingstabPage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox isTransformCheckBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton sk2MathRB;
        private System.Windows.Forms.RadioButton sk2GeoRB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton sk1MathRB;
        private System.Windows.Forms.RadioButton sk1GeoRB;
        private System.Windows.Forms.ListBox sk2LB;
        private System.Windows.Forms.ListBox sk1LB;
        private System.Windows.Forms.ComboBox sk2CB;
        private System.Windows.Forms.ComboBox sk1CB;
        private System.Windows.Forms.GroupBox groupBox10;
        public System.Windows.Forms.CheckBox isPozitionCheckBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton isAllZoomRB;
        private System.Windows.Forms.RadioButton isOneZoomRB;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ListView filesListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.Button delFileButton;
        private System.Windows.Forms.Button addFileButton;
        private System.Windows.Forms.OpenFileDialog openXMLFileDialog;
        private System.Windows.Forms.CheckBox XtoYcheckBox;
        private System.Windows.Forms.Label label36;
    }
}

