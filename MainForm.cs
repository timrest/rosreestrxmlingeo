﻿using Helpers;
using Ingeo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace RosreestrXMLIngeo
{
    public partial class MainForm : Form
    {
        IngeoHelper ingeo;
        XmlHelper xml;
        //ClassifiersHelper classifiersHelper;
        List<string> versions = new List<string>()
        {
            //"114:04",
            //"114:05",
            //"114:06",
            //"114:07",
            //"101:07",
            "101:08",
            "101:09",
            "101:10"
        };

        public MainForm()
        {
            InitializeComponent();
            xml = new XmlHelper();
            ProgressBarLogger.Progress = progressBar1;
            Helpers.Settings.IsZoom = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ingeoConnectBtn_Click(sender, e);
        }
        XDocument doc;
        public struct UIElementItem
        {
            public string Value { get; set; }
            public XElement XElement { get; set; }
        }
        void afterFormLoad()
        {

            Cursor = Cursors.WaitCursor;
            try
            {
                doc = XDocument.Load(Application.StartupPath + "/GeoTransform/geotransform.xml");
                sk1CB.SelectedValueChanged += sk1CB_SelectedValueChanged;
                sk1CB.Tag = sk1LB;
                sk2CB.Tag = sk2LB;
                sk2CB.SelectedValueChanged += sk1CB_SelectedValueChanged;
                sk1LB.SelectedValueChanged += sk1LB_SelectedValueChanged;
                sk2LB.SelectedValueChanged += sk1LB_SelectedValueChanged;
                foreach (XElement el in doc.Root.Elements())
                {
                    if (el.Name.ToString().ToLower().Trim().Equals("ellipsoids"))
                    {
                        sk1CB.Items.Add(new UIElementItem() { Value = "Эллипсоиды", XElement = el });
                        sk2CB.Items.Add(new UIElementItem() { Value = "Эллипсоиды", XElement = el });
                    }
                    if (el.Attribute("caption") != null)
                    {
                        sk1CB.Items.Add(new UIElementItem()
                        {
                            Value = el.Attribute("caption").Value,
                            XElement = el
                        });
                        sk2CB.Items.Add(new UIElementItem()
                        {
                            Value = el.Attribute("caption").Value,
                            XElement = el
                        });
                    }
                }
            }
            catch { MessageBox.Show("Не удалось открыть файл geotransform.xml"); }
            try
            {
                sk1CB.SelectedIndex = 3;
                sk2CB.SelectedIndex = 4;
                sk1LB.SelectedIndex = 0;
                sk2LB.SelectedIndex = 0;
            }
            catch { }

            if (Helpers.Transforms.Type)
                sk1MathRB.Checked = true;
            else
                sk1GeoRB.Checked = true;
            if (Helpers.Transforms.TypeOut)
                sk2MathRB.Checked = true;
            else
                sk2GeoRB.Checked = true;
            if (Helpers.Transforms.IsEnabled)
                isTransformCheckBox.Checked = false;
            else
                isTransformCheckBox.Checked = true;
            if (Transforms.XtoYChange)
            {
                label36.Text = "Импорт со сменой X на Y";
                XtoYcheckBox.Checked = true;
            }
            else
            {
                XtoYcheckBox.Checked = false;
                label36.Text = "Импорт без смены X на Y";
            }
            foreach (UIElementItem itm in sk1CB.Items)
                foreach (XElement el in itm.XElement.Elements())
                    if (el.Attribute("name").Value.Equals(Helpers.Settings.CoordSysTransformIn))
                    {
                        sk1LB.Items.Clear();
                        int index = 0, _index = 0;
                        foreach (XElement elem in itm.XElement.Elements())
                        {
                            if (elem.Attribute("name").Value.Equals(Helpers.Settings.CoordSysTransformIn))
                                index = _index;
                            sk1LB.Items.Add(new UIElementItem()
                            {
                                Value = elem.Attribute("caption").Value,
                                XElement = elem
                            });
                            _index++;
                        }
                        sk1CB.SelectedItem = itm;
                        sk1LB.SelectedIndex = index;
                    }
            foreach (UIElementItem itm in sk2CB.Items)
                foreach (XElement el in itm.XElement.Elements())
                    if (el.Attribute("name").Value.Equals(Helpers.Settings.CoordSysTransformOut))
                    {
                        sk2LB.Items.Clear();
                        int index = 0, _index = 0;
                        foreach (XElement elem in itm.XElement.Elements())
                        {
                            if (elem.Attribute("name").Value.Equals(Helpers.Settings.CoordSysTransformOut))
                                index = _index;
                            sk2LB.Items.Add(new UIElementItem()
                            {
                                Value = elem.Attribute("caption").Value,
                                XElement = elem
                            });
                            _index++;
                        }
                        sk2CB.SelectedItem = itm;
                        sk2LB.SelectedIndex = index;
                    }
            Cursor = Cursors.Default;
        }
        private void sk1CB_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            ListBox list = cb.Tag as ListBox;
            UIElementItem cbItem = (UIElementItem)cb.SelectedItem;

            list.Items.Clear();
            foreach (XElement el in cbItem.XElement.Elements())
                if (el.Attribute("caption") != null)
                    list.Items.Add(new UIElementItem()
                    {
                        Value = el.Attribute("caption").Value,
                        XElement = el
                    });
        }
        private void sk1LB_SelectedValueChanged(object sender, EventArgs e)
        {
            if (sk1LB.SelectedItem != null && sk2LB.SelectedItem != null)
                if (sk1LB.SelectedItem.Equals(sk2LB.SelectedItem))
                {
                    MessageBox.Show("Вы задали одинаковые системы координат", Text);
                    (sender as ListBox).SelectedIndex = -1;
                }
                else
                {
                    XAttribute attrFrom = ((UIElementItem)sk1LB.SelectedItem).XElement.Attribute("name");
                    XAttribute attrOut = ((UIElementItem)sk2LB.SelectedItem).XElement.Attribute("name");
                    if (attrFrom != null && attrOut != null)
                    {
                        Transforms.FromName = attrFrom.Value;
                        Transforms.OutName = attrOut.Value;
                    }
                }
        }
        // Обновление подключения к ИнГео
        private void ingeoConnectBtn_Click(object sender, EventArgs e)
        {
            try { ingeo = new IngeoHelper(); }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            try
            {
                SettingsSave.OpenSettings(ingeo, this);
            }
            catch { }
            try
            {
                ingeo = new IngeoHelper();
                label13.Text = "Подключено к ИнГео";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            ingeo.GetLotMaps(ingeo.Application.ActiveProjectView, lotMapCB, Helpers.Settings.LotMap);
            ingeo.GetCadBlockMaps(ingeo.Application.ActiveProjectView, cadBlockMapCB, Helpers.Settings.CadBlockMap);
            ingeo.GetOksMaps(ingeo.Application.ActiveProjectView, oksMapCB, Helpers.Settings.OksMap);
            ingeo.GetZoneMaps(ingeo.Application.ActiveProjectView, zoneMapCB, Helpers.Settings.ZoneMap);
            SettingsSave.SetInGeoHelper(ingeo);
            afterFormLoad();
            this.Cursor = Cursors.Default;
        }
        private void saveSettingdBtn_Click(object sender, EventArgs e)
        {
            try
            {
                SettingsSave.SaveSettings();
                MessageBox.Show("Настройки сохранены");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка сохранения настроек.\n" + ex.Message);
            }
        }

        #region // При изменении полей настроек для ЗУ
        private void lotMapCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotLayers(ingeo.LotMaps[lotMapCB.SelectedIndex], lotLayerCB, Helpers.Settings.LotLayer);
        }

        // Отображение списков стилей и таблиц выбранного слоя
        private void lotLayerCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotStyles(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotStyleCB, Helpers.Settings.LotStyle);
            ingeo.GetLotCaptionStyles(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotStyleSingCB, Helpers.Settings.LotCaptionStyle);

            //try
            //{
                ingeo.GetLotTablesCadNo(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotCadNoTableCB, Helpers.Settings.LotTableCadNo);
                ingeo.GetLotTablesAddress(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotAddressTableCB, Helpers.Settings.LotTableAddress);
                ingeo.GetLotTablesArea(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotAreaTableCB, Helpers.Settings.LotTableArea);
                ingeo.GetLotTablesLandCat(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotLandCatTableCB, Helpers.Settings.LotTableLandCat);
                ingeo.GetLotTablesUsing(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotUsingTableCB, Helpers.Settings.LotTableUsing);
                ingeo.GetLotTablesUsingByDoc(ingeo.LotLayers[lotLayerCB.SelectedIndex], lotUsingByDocTableCB, Helpers.Settings.LotTableUsingByDoc);
            //}
            //catch { }
        }

        private void lotCadNoTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotFieldsCadNo(ingeo.LotTablesCadNo[lotCadNoTableCB.SelectedIndex], lotCadNoFieldCB, Helpers.Settings.LotFieldCadNo);
        }

        private void lotAddressTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotFieldsAddress(ingeo.LotTablesAddress[lotAddressTableCB.SelectedIndex], lotAddressFieldCB, Helpers.Settings.LotFieldAddress);
        }

        private void lotAreaTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotFieldsArea(ingeo.LotTablesArea[lotAreaTableCB.SelectedIndex], lotAreaFieldCB, Helpers.Settings.LotFieldArea);
        }

        private void lotLandCatTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotFieldsLandCat(ingeo.LotTablesLandCat[lotLandCatTableCB.SelectedIndex], lotLandCatFieldCB, Helpers.Settings.LotFieldLandCat);
        }

        private void lotUsingTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotFieldsUsing(ingeo.LotTablesUsing[lotUsingTableCB.SelectedIndex], lotUsingFieldCB, Helpers.Settings.LotFieldUsing);
        }

        private void lotUsingByDocTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetLotFieldsUsingByDoc(ingeo.LotTablesUsingByDoc[lotUsingByDocTableCB.SelectedIndex], lotUsingByDocFieldCB, Helpers.Settings.LotFieldUsingByDoc);
        }
        #endregion

        #region // При изменении полей настроек для кад. квартала
        private void cadBlockMapCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetCadBlockLayers(ingeo.CadBlockMaps[cadBlockMapCB.SelectedIndex], cadBlockLayerCB, Helpers.Settings.CadBlockLayer);
        }

        private void cadBlockLayerCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetCadBlockStyles(ingeo.CadBlockLayers[cadBlockLayerCB.SelectedIndex], cadBlockStyleCB, Helpers.Settings.CadBlockStyle);
            ingeo.GetCadBlockCaptionStyles(ingeo.CadBlockLayers[cadBlockLayerCB.SelectedIndex], cadBlockCaptionStyleCB, Helpers.Settings.CadBlockCaptionStyle);

            ingeo.GetCadBlockTablesCadNo(ingeo.CadBlockLayers[cadBlockLayerCB.SelectedIndex], cadBlockCadNoTableCB, Helpers.Settings.CadBlockTableCadNo);
            ingeo.GetCadBlockTablesArea(ingeo.CadBlockLayers[cadBlockLayerCB.SelectedIndex], cadBlockAreaTableCB, Helpers.Settings.CadBlockTableArea);
        }

        private void cadBlockCadNoTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetCadBlockFieldsCadNo(ingeo.CadBlockTablesCadNo[cadBlockCadNoTableCB.SelectedIndex], cadBlockCadNoFieldCB, Helpers.Settings.CadBlockFieldCadNo);
        }

        private void cadBlockAreaTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetCadBlockFieldsArea(ingeo.CadBlockTablesArea[cadBlockAreaTableCB.SelectedIndex], cadBlockAreaFieldCB, Helpers.Settings.CadBlockFieldArea);
        }
        #endregion

        #region // При изменении полей настроек для ОКС
        private void oksMapCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetOksLayers(ingeo.OksMaps[oksMapCB.SelectedIndex], oksLayerCB, Helpers.Settings.OksLayer);
        }

        private void oksLayerCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetOksBuildStyles(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksBuildStyleCB, Helpers.Settings.OksBuildStyle);
            ingeo.GetOksConstrStyles(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksConstrStyleCB, Helpers.Settings.OksConstrStyle);
            ingeo.GetOksUncompletedStyles(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksUncompletedStyleCB, Helpers.Settings.OksUncompletedStyle);
            ingeo.GetOksCaptionStyles(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksCaptionStyleCB, Helpers.Settings.OksCaptionStyle);

            ingeo.GetOksTablesCadNo(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksCadNoTableCB, Helpers.Settings.OksTableCadNo);
            ingeo.GetOksTablesAddress(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksAddressTableCB, Helpers.Settings.OksTableAddress);
            ingeo.GetOksTablesArea(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksAreaTableCB, Helpers.Settings.OksTableArea);
            ingeo.GetOksTablesType(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksTypeTableCB, Helpers.Settings.OksTableType);
            ingeo.GetOksLotTablesAssign(ingeo.OksLayers[oksLayerCB.SelectedIndex], oksAssignTableCB, Helpers.Settings.OksTableAssign);
        }

        private void oksCadNoTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetOksFieldsCadNo(ingeo.OksTablesCadNo[oksCadNoTableCB.SelectedIndex], oksCadNoFieldCB, Helpers.Settings.OksFieldCadNo);
        }

        private void oksAddressTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetOksFieldsAddress(ingeo.OksTablesAddress[oksAddressTableCB.SelectedIndex], oksAddressFieldCB, Helpers.Settings.OksFieldAddress);
        }

        private void oksAreaTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetOksFieldsArea(ingeo.OksTablesArea[oksAreaTableCB.SelectedIndex], oksAreaFieldCB, Helpers.Settings.OksFieldArea);
        }

        private void oksTypeTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetOksFieldsType(ingeo.OksTablesType[oksTypeTableCB.SelectedIndex], oksTypeFieldCB, Helpers.Settings.OksFieldType);
        }

        private void oksAssignTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetOksFieldsAssign(ingeo.OksTablesAssign[oksAssignTableCB.SelectedIndex], oksAssignFieldCB, Helpers.Settings.OksFieldAssign);
        }
        #endregion

        #region // При изменении полей настроек для зон
        private void zoneMapCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetZoneLayers(ingeo.ZoneMaps[zoneMapCB.SelectedIndex], zoneLayerCB, Helpers.Settings.ZoneLayer);
        }

        private void zoneLayerCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetZoneTerrStyles(ingeo.ZoneLayers[zoneLayerCB.SelectedIndex], zoneTerrStyleCB, Helpers.Settings.ZoneTerrStyle);
            ingeo.GetZoneSZZStyles(ingeo.ZoneLayers[zoneLayerCB.SelectedIndex], zoneSZZStyleCB, Helpers.Settings.ZoneSZZStyle);
            ingeo.GetZoneCaptionStyles(ingeo.ZoneLayers[zoneLayerCB.SelectedIndex], zoneCaptionStyleCB, Helpers.Settings.ZoneCaptionStyle);

            ingeo.GetZoneTablesAccountNo(ingeo.ZoneLayers[zoneLayerCB.SelectedIndex], zoneAccountNoTableCB, Helpers.Settings.ZoneTableAccountNo);
            ingeo.GetZoneTablesName(ingeo.ZoneLayers[zoneLayerCB.SelectedIndex], zoneNameTableCB, Helpers.Settings.ZoneTableName);
            ingeo.GetZoneTablesRestrict(ingeo.ZoneLayers[zoneLayerCB.SelectedIndex], zoneRestrictTableCB, Helpers.Settings.ZoneTableRestrict);
        }

        private void zoneAccountNoTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetZoneFieldsAccountNo(ingeo.ZoneTablesAccountNo[zoneAccountNoTableCB.SelectedIndex], zoneAccountNoFieldCB, Helpers.Settings.ZoneFieldAccountNo);
        }

        private void zoneNameTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetZoneFieldsName(ingeo.ZoneTablesName[zoneNameTableCB.SelectedIndex], zoneNameFieldCB, Helpers.Settings.ZoneFieldName);
        }

        private void zoneRestrictTableCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ingeo.GetZoneFieldsRestrict(ingeo.ZoneTablesRestrict[zoneRestrictTableCB.SelectedIndex], zoneRestrictFieldCB, Helpers.Settings.ZoneFieldRestrict);
        }
        #endregion

        private void sk1CB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // Добавление файла в очередь импорта
        private void addFileButton_Click(object sender, EventArgs e)
        {
            if (ingeo == null)
            {
                MessageBox.Show("Сервер ИнГео не запущен", Text);
                return;
            }
            
            if (openXMLFileDialog.ShowDialog() != DialogResult.OK)
                return;
            Cursor = Cursors.WaitCursor;
            foreach (string fileName in openXMLFileDialog.FileNames)
            {
                string version = xml.GetVersion(fileName);
                if (version.Equals(string.Empty))
                {
                    MessageBox.Show("Такой файл не поддерживается", Text);
                    continue;
                }
                if (!versions.Contains(version))
                {
                    MessageBox.Show("Версия файла(" + version + ") " + fileName + " не поддерживается", Text);
                    continue;
                }

                bool itemExists = false;
                foreach (ListViewItem lvi in filesListView.Items)
                {
                    if (lvi.Text == fileName)
                    {
                        itemExists = true;
                        break;
                    }
                }
                if (!itemExists)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = fileName;
                    XmlDocument doc = new XmlDocument();
                    doc.Load(fileName);
                    if (doc.GetElementsByTagName("Region_Cadastr_Vidimus_KP").Count > 0 ||
                        doc.GetElementsByTagName("KPZU").Count > 0)
                        item.SubItems.Add("Кадастровый паспорт");
                    else if (version.Split(':')[0] == "114")
                        item.SubItems.Add("Кадастровая выписка");
                    if (version.Split(':')[0] == "101")
                        item.SubItems.Add("Кадастровый план территории");
                    item.SubItems.Add(version.Split(':')[1]);
                    filesListView.Items.Add(item);
                }
                else
                    MessageBox.Show("Файл " + fileName + " уже добавлен в список импорта");
            }
            
            Cursor = Cursors.Default;
        }

        // Удаление файла из очереди импорта - по кнопке
        private void delFileButton_Click(object sender, EventArgs e)
        {
            if (filesListView.SelectedItems.Count > 0)
            {
                for (int i = filesListView.SelectedItems.Count; i > 0; i--)
                {
                    filesListView.Items.Remove(filesListView.SelectedItems[i - 1]);
                }
            }
        }

        // Запуск импорта
        private void importButton_Click(object sender, EventArgs e)
        {
            filesListView.CheckBoxes = true;
            if (isAllZoomRB.Checked)
                Helpers.Settings.IsAllObjectsZoom = true;
            else if (isOneZoomRB.Checked)
                Helpers.Settings.IsAllObjectsZoom = false;
            Helpers.Settings.ZoomRect = null;
           
            if (ingeo == null)
            {
                MessageBox.Show("Сервер ИнГео не запущен", Text);
                return;
            }

            


            if (label13.Text == "Не подключено к ИнГео")
            {
                MessageBox.Show("Проверьте подключение к ИнГео");
                return;
            }
            if (filesListView.Items.Count == 0)
            {
                MessageBox.Show("Не загружено ни одного файла для импорта");
                return;
            }
            //if (!mapsCB.Text.Equals(string.Empty) && !layersCB.Text.Equals(string.Empty))
            //{
            //    foreach (IIngeoMapView map in ingeo.Application.ActiveProjectView.MapViews)
            //        if (map.Map.Name.Equals(mapsCB.Text))
            //            foreach (IIngeoLayerView layer in map.LayerViews)
            //                if (layer.Layer.Name.Equals(layersCB.Text))
            //                    if (layer.Layer.SemTables.Count == 0)
            //                    {
            //                        MessageBox.Show("Нет таблиц в выбранном слое");
            //                        return;
            //                    }
            //}
            //if (Helpers.XmlTemplate.Template == null ||
            //    Helpers.XmlTemplate.Template["CadastralNumber"] == null)
            //{
            //    MessageBox.Show("Не задано значение импорта в ИнГео для кадастрового номера", Text);
            //    try
            //    {
            //        tabControl1.SelectedIndex = 2;
            //    }
            //    catch { }
            //    return;
            //}
            fillClassifiers();
            ProgressBarLogger.Writer = new ImportResult();
            foreach (ListViewItem item in filesListView.Items)
                item.Checked = false;
            filesListView.Enabled = false;
            int scrollStep = 0;
            foreach (ListViewItem item in filesListView.Items)
            {
                filesListView.EnsureVisible(scrollStep);
                scrollStep++;
                ProgressBarLogger.Value = 0;
                
                #region KPT
                if (item.SubItems[1].Text == "Кадастровый план территории" && item.SubItems[2].Text == "08")
                {
                    XmlSerializer ser = new XmlSerializer(typeof(KPT08.Region_Cadastr));
                    KPT08.Region_Cadastr kpt;
                    KPT08_spatial.tKPT08_spatial kpt08Spatial;
                    using (XmlReader reader = XmlReader.Create(item.Text))
                    {
                        kpt = (KPT08.Region_Cadastr)ser.Deserialize(reader);

                    }
                    KPT08_spatial.ParserSpatial parser = new KPT08_spatial.ParserSpatial(item.Text);
                    kpt08Spatial = parser.ParseDocument();
                    KPT08.Importer importer = new KPT08.Importer(ingeo);

                    importer.FileType = item.SubItems[1].Text + " \"" + item.SubItems[0].Text.Substring(item.SubItems[0].Text.LastIndexOf("\\") + 1) + "\" кадастровый №";

                    #region Выбранные значения настроек
                    importer.LotLayer = ingeo.LotLayers[lotLayerCB.SelectedIndex].Layer.ID;
                    importer.LotStyles = ingeo.LotStyles;
                    //importer.LotStyle = ingeo.LotStyles[lotStyleCB.SelectedIndex].Style.ID;
                    importer.LotCaptionStyle = ingeo.LotCaptionStyles[lotStyleSingCB.SelectedIndex].Style.ID;
                    importer.LotTableCadNo = ingeo.LotTablesCadNo[lotCadNoTableCB.SelectedIndex].Name;
                    importer.LotFieldCadNo = ingeo.LotFieldsCadNo[lotCadNoFieldCB.SelectedIndex].FieldName;
                    importer.LotTableAddress = ingeo.LotTablesAddress[lotAddressTableCB.SelectedIndex].Name;
                    importer.LotFieldAddress = ingeo.LotFieldsAddress[lotAddressFieldCB.SelectedIndex].FieldName;
                    importer.LotTableArea = ingeo.LotTablesArea[lotAreaTableCB.SelectedIndex].Name;
                    importer.LotFieldArea = ingeo.LotFieldsArea[lotAreaFieldCB.SelectedIndex].FieldName;
                    importer.LotTableLandCat = ingeo.LotTablesLandCat[lotLandCatTableCB.SelectedIndex].Name;
                    importer.LotFieldLandCat = ingeo.LotFieldsLandCat[lotLandCatFieldCB.SelectedIndex].FieldName;
                    importer.LotTableUsing = ingeo.LotTablesUsing[lotUsingTableCB.SelectedIndex].Name;
                    importer.LotFieldUsing = ingeo.LotFieldsUsing[lotUsingFieldCB.SelectedIndex].FieldName;
                    importer.LotTableUsingByDoc = ingeo.LotTablesUsingByDoc[lotUsingByDocTableCB.SelectedIndex].Name;
                    importer.LotFieldUsingByDoc = ingeo.LotFieldsUsingByDoc[lotUsingByDocFieldCB.SelectedIndex].FieldName;



                    importer.CadBlockLayer = ingeo.CadBlockLayers[cadBlockLayerCB.SelectedIndex].Layer.ID;
                    importer.CadBlockStyle = ingeo.CadBlockStyles[cadBlockStyleCB.SelectedIndex].Style.ID;
                    importer.CadBlockCaptionStyle = ingeo.CadBlockCaptionStyles[cadBlockCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.CadBlockTableCadNo = ingeo.CadBlockTablesCadNo[cadBlockCadNoTableCB.SelectedIndex].Name;
                    importer.CadBlockFieldCadNo = ingeo.CadBlockFieldsCadNo[cadBlockCadNoFieldCB.SelectedIndex].FieldName;
                    importer.CadBlockTableArea = ingeo.CadBlockTablesArea[cadBlockAreaTableCB.SelectedIndex].Name;
                    importer.CadBlockFieldArea = ingeo.CadBlockFieldsArea[cadBlockAreaFieldCB.SelectedIndex].FieldName;


                    importer.OksLayer = ingeo.OksLayers[oksLayerCB.SelectedIndex].Layer.ID;
                    importer.OksBuildStyle = ingeo.OksBuildStyles[oksBuildStyleCB.SelectedIndex].Style.ID;
                    importer.OksConstrStyle = ingeo.OksConstrStyles[oksConstrStyleCB.SelectedIndex].Style.ID;
                    importer.OksUncompletedStyle = ingeo.OksUncompletedStyles[oksUncompletedStyleCB.SelectedIndex].Style.ID;
                    importer.OksCaptionStyle = ingeo.OksCaptionStyles[oksCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.OksTableCadNo = ingeo.OksTablesCadNo[oksCadNoTableCB.SelectedIndex].Name;
                    importer.OksFieldCadNo = ingeo.OksFieldsCadNo[oksCadNoFieldCB.SelectedIndex].FieldName;
                    importer.OksTableAddress = ingeo.OksTablesAddress[oksAddressTableCB.SelectedIndex].Name;
                    importer.OksFieldAddress = ingeo.OksFieldsAddress[oksAddressFieldCB.SelectedIndex].FieldName;
                    importer.OksTableArea = ingeo.OksTablesArea[oksAreaTableCB.SelectedIndex].Name;
                    importer.OksFieldArea = ingeo.OksFieldsArea[oksAreaFieldCB.SelectedIndex].FieldName;
                    importer.OksTableType = ingeo.OksTablesType[oksTypeTableCB.SelectedIndex].Name;
                    importer.OksFieldType = ingeo.OksFieldsType[oksTypeFieldCB.SelectedIndex].FieldName;
                    importer.OksTableAssign = ingeo.OksTablesAssign[oksAssignTableCB.SelectedIndex].Name;
                    importer.OksFieldAssign = ingeo.OksFieldsAssign[oksAssignFieldCB.SelectedIndex].FieldName;

                    importer.ZoneLayer = ingeo.ZoneLayers[zoneLayerCB.SelectedIndex].Layer.ID;
                    importer.ZoneTerrStyle = ingeo.ZoneTerrStyles[zoneTerrStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneSZZStyle = ingeo.ZoneSZZStyles[zoneSZZStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneCaptionStyle = ingeo.ZoneCaptionStyles[zoneCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneTableAccountNo = ingeo.ZoneTablesAccountNo[zoneAccountNoTableCB.SelectedIndex].Name;
                    importer.ZoneFieldAccountNo = ingeo.ZoneFieldsAccountNo[zoneAccountNoFieldCB.SelectedIndex].FieldName;
                    importer.ZoneTableName = ingeo.ZoneTablesName[zoneNameTableCB.SelectedIndex].Name;
                    importer.ZoneFieldName = ingeo.ZoneFieldsName[zoneNameFieldCB.SelectedIndex].FieldName;
                    importer.ZoneTableRestrict = ingeo.ZoneTablesRestrict[zoneRestrictTableCB.SelectedIndex].Name;
                    importer.ZoneFieldRestrict = ingeo.ZoneFieldsRestrict[zoneRestrictFieldCB.SelectedIndex].FieldName;
                    #endregion

                    importer.Import(kpt, kpt08Spatial);
                    item.Checked = true;
                    continue;
                }
                else if (item.SubItems[1].Text == "Кадастровый план территории" && item.SubItems[2].Text == "09")
                {
                    XmlSerializer ser = new XmlSerializer(typeof(KPT09.KPT));
                    KPT09.KPT kpt;
                    KPT09_spatial.tKPT09_spatial kpt09Spatial;
                    using (XmlReader reader = XmlReader.Create(item.Text))
                    {
                        kpt = (KPT09.KPT)ser.Deserialize(reader);

                    }
                    KPT09_spatial.ParserSpatial parser = new KPT09_spatial.ParserSpatial(item.Text);
                    kpt09Spatial = parser.ParseDocument();
                    KPT09.Importer importer = new KPT09.Importer(ingeo);

                    importer.FileType = item.SubItems[1].Text + " \"" + item.SubItems[0].Text.Substring(item.SubItems[0].Text.LastIndexOf("\\") + 1) + "\" кадастровый №";

                    #region Выбранные значения настроек
                    importer.LotLayer = ingeo.LotLayers[lotLayerCB.SelectedIndex].Layer.ID;
                    importer.LotStyles = ingeo.LotStyles;
                    //importer.LotStyle = ingeo.LotStyles[lotStyleCB.SelectedIndex].Style.ID;
                    importer.LotCaptionStyle = ingeo.LotCaptionStyles[lotStyleSingCB.SelectedIndex].Style.ID;
                    importer.LotTableCadNo = ingeo.LotTablesCadNo[lotCadNoTableCB.SelectedIndex].Name;
                    importer.LotFieldCadNo = ingeo.LotFieldsCadNo[lotCadNoFieldCB.SelectedIndex].FieldName;
                    importer.LotTableAddress = ingeo.LotTablesAddress[lotAddressTableCB.SelectedIndex].Name;
                    importer.LotFieldAddress = ingeo.LotFieldsAddress[lotAddressFieldCB.SelectedIndex].FieldName;
                    importer.LotTableArea = ingeo.LotTablesArea[lotAreaTableCB.SelectedIndex].Name;
                    importer.LotFieldArea = ingeo.LotFieldsArea[lotAreaFieldCB.SelectedIndex].FieldName;
                    importer.LotTableLandCat = ingeo.LotTablesLandCat[lotLandCatTableCB.SelectedIndex].Name;
                    importer.LotFieldLandCat = ingeo.LotFieldsLandCat[lotLandCatFieldCB.SelectedIndex].FieldName;
                    importer.LotTableUsing = ingeo.LotTablesUsing[lotUsingTableCB.SelectedIndex].Name;
                    importer.LotFieldUsing = ingeo.LotFieldsUsing[lotUsingFieldCB.SelectedIndex].FieldName;
                    importer.LotTableUsingByDoc = ingeo.LotTablesUsingByDoc[lotUsingByDocTableCB.SelectedIndex].Name;
                    importer.LotFieldUsingByDoc = ingeo.LotFieldsUsingByDoc[lotUsingByDocFieldCB.SelectedIndex].FieldName;



                    importer.CadBlockLayer = ingeo.CadBlockLayers[cadBlockLayerCB.SelectedIndex].Layer.ID;
                    importer.CadBlockStyle = ingeo.CadBlockStyles[cadBlockStyleCB.SelectedIndex].Style.ID;
                    importer.CadBlockCaptionStyle = ingeo.CadBlockCaptionStyles[cadBlockCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.CadBlockTableCadNo = ingeo.CadBlockTablesCadNo[cadBlockCadNoTableCB.SelectedIndex].Name;
                    importer.CadBlockFieldCadNo = ingeo.CadBlockFieldsCadNo[cadBlockCadNoFieldCB.SelectedIndex].FieldName;
                    importer.CadBlockTableArea = ingeo.CadBlockTablesArea[cadBlockAreaTableCB.SelectedIndex].Name;
                    importer.CadBlockFieldArea = ingeo.CadBlockFieldsArea[cadBlockAreaFieldCB.SelectedIndex].FieldName;


                    importer.OksLayer = ingeo.OksLayers[oksLayerCB.SelectedIndex].Layer.ID;
                    importer.OksBuildStyle = ingeo.OksBuildStyles[oksBuildStyleCB.SelectedIndex].Style.ID;
                    importer.OksConstrStyle = ingeo.OksConstrStyles[oksConstrStyleCB.SelectedIndex].Style.ID;
                    importer.OksUncompletedStyle = ingeo.OksUncompletedStyles[oksUncompletedStyleCB.SelectedIndex].Style.ID;
                    importer.OksCaptionStyle = ingeo.OksCaptionStyles[oksCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.OksTableCadNo = ingeo.OksTablesCadNo[oksCadNoTableCB.SelectedIndex].Name;
                    importer.OksFieldCadNo = ingeo.OksFieldsCadNo[oksCadNoFieldCB.SelectedIndex].FieldName;
                    importer.OksTableAddress = ingeo.OksTablesAddress[oksAddressTableCB.SelectedIndex].Name;
                    importer.OksFieldAddress = ingeo.OksFieldsAddress[oksAddressFieldCB.SelectedIndex].FieldName;
                    importer.OksTableArea = ingeo.OksTablesArea[oksAreaTableCB.SelectedIndex].Name;
                    importer.OksFieldArea = ingeo.OksFieldsArea[oksAreaFieldCB.SelectedIndex].FieldName;
                    importer.OksTableType = ingeo.OksTablesType[oksTypeTableCB.SelectedIndex].Name;
                    importer.OksFieldType = ingeo.OksFieldsType[oksTypeFieldCB.SelectedIndex].FieldName;
                    importer.OksTableAssign = ingeo.OksTablesAssign[oksAssignTableCB.SelectedIndex].Name;
                    importer.OksFieldAssign = ingeo.OksFieldsAssign[oksAssignFieldCB.SelectedIndex].FieldName;

                    importer.ZoneLayer = ingeo.ZoneLayers[zoneLayerCB.SelectedIndex].Layer.ID;
                    importer.ZoneTerrStyle = ingeo.ZoneTerrStyles[zoneTerrStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneSZZStyle = ingeo.ZoneSZZStyles[zoneSZZStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneCaptionStyle = ingeo.ZoneCaptionStyles[zoneCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneTableAccountNo = ingeo.ZoneTablesAccountNo[zoneAccountNoTableCB.SelectedIndex].Name;
                    importer.ZoneFieldAccountNo = ingeo.ZoneFieldsAccountNo[zoneAccountNoFieldCB.SelectedIndex].FieldName;
                    importer.ZoneTableName = ingeo.ZoneTablesName[zoneNameTableCB.SelectedIndex].Name;
                    importer.ZoneFieldName = ingeo.ZoneFieldsName[zoneNameFieldCB.SelectedIndex].FieldName;
                    importer.ZoneTableRestrict = ingeo.ZoneTablesRestrict[zoneRestrictTableCB.SelectedIndex].Name;
                    importer.ZoneFieldRestrict = ingeo.ZoneFieldsRestrict[zoneRestrictFieldCB.SelectedIndex].FieldName;
                    #endregion

                    importer.Import(kpt, kpt09Spatial);
                    item.Checked = true;
                    continue;
                }
                else if (item.SubItems[1].Text == "Кадастровый план территории" && item.SubItems[2].Text == "10")
                {
                    XmlSerializer ser = new XmlSerializer(typeof(KPT10.KPT));
                    KPT10.KPT kpt;
                    KPT10_spatial.tKPT10_spatial kpt10Spatial;
                    using (XmlReader reader = XmlReader.Create(item.Text))
                    {
                        kpt = (KPT10.KPT)ser.Deserialize(reader);

                    }
                    KPT10_spatial.ParserSpatial parser = new KPT10_spatial.ParserSpatial(item.Text);
                    kpt10Spatial = parser.ParseDocument();
                    KPT10.Importer importer = new KPT10.Importer(ingeo);

                    importer.FileType = item.SubItems[1].Text + " \"" + item.SubItems[0].Text.Substring(item.SubItems[0].Text.LastIndexOf("\\") + 1) + "\" кадастровый №";

                    #region Выбранные значения настроек
                    importer.LotLayer = ingeo.LotLayers[lotLayerCB.SelectedIndex].Layer.ID;
                    importer.LotStyles = ingeo.LotStyles;
                    //importer.LotStyle = ingeo.LotStyles[lotStyleCB.SelectedIndex].Style.ID;
                    importer.LotCaptionStyle = ingeo.LotCaptionStyles[lotStyleSingCB.SelectedIndex].Style.ID;
                    importer.LotTableCadNo = ingeo.LotTablesCadNo[lotCadNoTableCB.SelectedIndex].Name;
                    importer.LotFieldCadNo = ingeo.LotFieldsCadNo[lotCadNoFieldCB.SelectedIndex].FieldName;
                    importer.LotTableAddress = ingeo.LotTablesAddress[lotAddressTableCB.SelectedIndex].Name;
                    importer.LotFieldAddress = ingeo.LotFieldsAddress[lotAddressFieldCB.SelectedIndex].FieldName;
                    importer.LotTableArea = ingeo.LotTablesArea[lotAreaTableCB.SelectedIndex].Name;
                    importer.LotFieldArea = ingeo.LotFieldsArea[lotAreaFieldCB.SelectedIndex].FieldName;
                    importer.LotTableLandCat = ingeo.LotTablesLandCat[lotLandCatTableCB.SelectedIndex].Name;
                    importer.LotFieldLandCat = ingeo.LotFieldsLandCat[lotLandCatFieldCB.SelectedIndex].FieldName;
                    importer.LotTableUsing = ingeo.LotTablesUsing[lotUsingTableCB.SelectedIndex].Name;
                    importer.LotFieldUsing = ingeo.LotFieldsUsing[lotUsingFieldCB.SelectedIndex].FieldName;
                    importer.LotTableUsingByDoc = ingeo.LotTablesUsingByDoc[lotUsingByDocTableCB.SelectedIndex].Name;
                    importer.LotFieldUsingByDoc = ingeo.LotFieldsUsingByDoc[lotUsingByDocFieldCB.SelectedIndex].FieldName;



                    importer.CadBlockLayer = ingeo.CadBlockLayers[cadBlockLayerCB.SelectedIndex].Layer.ID;
                    importer.CadBlockStyle = ingeo.CadBlockStyles[cadBlockStyleCB.SelectedIndex].Style.ID;
                    importer.CadBlockCaptionStyle = ingeo.CadBlockCaptionStyles[cadBlockCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.CadBlockTableCadNo = ingeo.CadBlockTablesCadNo[cadBlockCadNoTableCB.SelectedIndex].Name;
                    importer.CadBlockFieldCadNo = ingeo.CadBlockFieldsCadNo[cadBlockCadNoFieldCB.SelectedIndex].FieldName;
                    importer.CadBlockTableArea = ingeo.CadBlockTablesArea[cadBlockAreaTableCB.SelectedIndex].Name;
                    importer.CadBlockFieldArea = ingeo.CadBlockFieldsArea[cadBlockAreaFieldCB.SelectedIndex].FieldName;


                    importer.OksLayer = ingeo.OksLayers[oksLayerCB.SelectedIndex].Layer.ID;
                    importer.OksBuildStyle = ingeo.OksBuildStyles[oksBuildStyleCB.SelectedIndex].Style.ID;
                    importer.OksConstrStyle = ingeo.OksConstrStyles[oksConstrStyleCB.SelectedIndex].Style.ID;
                    importer.OksUncompletedStyle = ingeo.OksUncompletedStyles[oksUncompletedStyleCB.SelectedIndex].Style.ID;
                    importer.OksCaptionStyle = ingeo.OksCaptionStyles[oksCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.OksTableCadNo = ingeo.OksTablesCadNo[oksCadNoTableCB.SelectedIndex].Name;
                    importer.OksFieldCadNo = ingeo.OksFieldsCadNo[oksCadNoFieldCB.SelectedIndex].FieldName;
                    importer.OksTableAddress = ingeo.OksTablesAddress[oksAddressTableCB.SelectedIndex].Name;
                    importer.OksFieldAddress = ingeo.OksFieldsAddress[oksAddressFieldCB.SelectedIndex].FieldName;
                    importer.OksTableArea = ingeo.OksTablesArea[oksAreaTableCB.SelectedIndex].Name;
                    importer.OksFieldArea = ingeo.OksFieldsArea[oksAreaFieldCB.SelectedIndex].FieldName;
                    importer.OksTableType = ingeo.OksTablesType[oksTypeTableCB.SelectedIndex].Name;
                    importer.OksFieldType = ingeo.OksFieldsType[oksTypeFieldCB.SelectedIndex].FieldName;
                    importer.OksTableAssign = ingeo.OksTablesAssign[oksAssignTableCB.SelectedIndex].Name;
                    importer.OksFieldAssign = ingeo.OksFieldsAssign[oksAssignFieldCB.SelectedIndex].FieldName;

                    importer.ZoneLayer = ingeo.ZoneLayers[zoneLayerCB.SelectedIndex].Layer.ID;
                    importer.ZoneTerrStyle = ingeo.ZoneTerrStyles[zoneTerrStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneSZZStyle = ingeo.ZoneSZZStyles[zoneSZZStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneCaptionStyle = ingeo.ZoneCaptionStyles[zoneCaptionStyleCB.SelectedIndex].Style.ID;
                    importer.ZoneTableAccountNo = ingeo.ZoneTablesAccountNo[zoneAccountNoTableCB.SelectedIndex].Name;
                    importer.ZoneFieldAccountNo = ingeo.ZoneFieldsAccountNo[zoneAccountNoFieldCB.SelectedIndex].FieldName;
                    importer.ZoneTableName = ingeo.ZoneTablesName[zoneNameTableCB.SelectedIndex].Name;
                    importer.ZoneFieldName = ingeo.ZoneFieldsName[zoneNameFieldCB.SelectedIndex].FieldName;
                    importer.ZoneTableRestrict = ingeo.ZoneTablesRestrict[zoneRestrictTableCB.SelectedIndex].Name;
                    importer.ZoneFieldRestrict = ingeo.ZoneFieldsRestrict[zoneRestrictFieldCB.SelectedIndex].FieldName;
                    #endregion

                    importer.Import(kpt, kpt10Spatial);
                    item.Checked = true;
                    continue;
                }
                #endregion
            }
            if (Helpers.Settings.IsZoom)
            {
                try
                {
                    ingeo.Application.MainWindow.MapWindow.Navigator.FitWorldBounds(Helpers.Settings.ZoomRect.X1,
                        Helpers.Settings.ZoomRect.Y1, Helpers.Settings.ZoomRect.X2, Helpers.Settings.ZoomRect.Y2,
                        TIngeoNavigatorFitMode.infitAlwaysScale);
                }
                catch { }
            }
            new RosreestrXML.ResultForm(ProgressBarLogger.Writer, Width, Height).ShowDialog();
            ProgressBarLogger.Value = 0;
            filesListView.Items.Clear();
            filesListView.Enabled = true;
            filesListView.CheckBoxes = true;
        }

        // заполнение классификаторов
        private void fillClassifiers()
        {

            try
            {
                Dictionary<string, string> cv4 = new Dictionary<string, string>()
                    {
                        {"LandCatClassifiers", Application.StartupPath + "/Schemas/KPT_v09/dCategories_v01.xsd"},
                        {"UsingClassifiers", Application.StartupPath + "/Schemas/KPT_v09/dUtilizations_v01.xsd"},
                        {"BuildTypeClassifiers", Application.StartupPath + "/Schemas/KPT_v09/dRealty_v02.xsd"},
                        {"BuildSignClassifiers", Application.StartupPath + "/Schemas/KPT_v09/dAssBuilding_v01.xsd"},
                        {"RegionClassifiers", Application.StartupPath + "/Schemas/KPT_v09/dRegionsRF_v01.xsd"},
                    };
                ClassifiersHelper.FillClassifiers(cv4);
            }
            catch { }
        }

        private void isTransformCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (isTransformCheckBox.Checked)
            {
                Transforms.IsEnabled = false;
                sk1CB.Enabled = false;
                sk2CB.Enabled = false;
                sk1LB.Enabled = false;
                sk2LB.Enabled = false;
                panel1.Enabled = false;
                panel2.Enabled = false;
            }
            else
            {
                Transforms.IsEnabled = true;
                sk1CB.Enabled = true;
                sk2CB.Enabled = true;
                sk1LB.Enabled = true;
                sk2LB.Enabled = true;
                panel1.Enabled = true;
                panel2.Enabled = true;
            }
        }

        private void sk1MathRB_CheckedChanged(object sender, EventArgs e)
        {
            Transforms.Type = true;
        }

        private void sk1GeoRB_CheckedChanged(object sender, EventArgs e)
        {
            Transforms.Type = false;
        }

        private void sk2MathRB_CheckedChanged(object sender, EventArgs e)
        {
            Transforms.TypeOut = true;
        }

        private void sk2GeoRB_CheckedChanged(object sender, EventArgs e)
        {
            Transforms.TypeOut = false;
        }

        private void filesListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (filesListView.Enabled)
                if (e.Item.Checked)
                    e.Item.Checked = false;
        }

        private void isPozitionCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            if ((sender as CheckBox).Checked)
            {
                Helpers.Settings.IsZoom = true;
                isAllZoomRB.Enabled = true;
                isOneZoomRB.Enabled = true;
            }
            else
            {
                Helpers.Settings.IsZoom = false;
                isAllZoomRB.Enabled = false;
                isOneZoomRB.Enabled = false;
            }
        }

        private void XtoYcheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (XtoYcheckBox.Checked)
            {
                Transforms.XtoYChange = true;
                label36.Text = "Импорт со сменой X на Y";
            }
            else
            {
                Transforms.XtoYChange = false;
                label36.Text = "Импорт без смены X на Y";
            }
        }

    }
}
