﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RosreestrXML
{
    public partial class ResultForm : Form
    {
        public ResultForm(Helpers.ImportResult result, int width, int height)
        {
            InitializeComponent();
            Width = width - 40;
            Height = height - 40;
            richTextBox1.Width = Width - 40;
            richTextBox1.Height = Height - 60;
            richTextBox1.Lines = result.Output.ToArray();
            StartPosition = FormStartPosition.CenterParent;
        }
    }
}
